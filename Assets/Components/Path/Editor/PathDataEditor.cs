using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PathData))]
public class PathDataEditor : Editor
{
    private PathData pathData;

    private bool globalParametersFoldout = true;
    private bool pointParametersFoldout = true;

    public static void CallOnSceneGUI(PathData pathData)
    {
        DrawPathData(pathData);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        globalParametersFoldout = EditorGUILayout.Foldout(globalParametersFoldout, "Global parameters");
        if (globalParametersFoldout)
        {
            GUILayout.BeginVertical();
            AddParameters(pathData.GlobalPointsData, false);
            GUILayout.EndVertical();
        }

        GUILayout.Space(10);

        pointParametersFoldout = EditorGUILayout.Foldout(pointParametersFoldout, "Local points parameters");
        if (pointParametersFoldout)
        {
            GUILayout.BeginVertical();
            GUILayout.Space(10);

            for (int i = 0; i < pathData.LocalPointsData.Count; i++)
            {
                GUILayout.BeginVertical();
                GUILayout.Label($"<Point {i + 1}>");

                AddParameters(pathData.LocalPointsData[i], true);

                GUILayout.EndVertical();
            }
            GUILayout.EndVertical();
        }

        serializedObject.ApplyModifiedProperties();
        SceneView.RepaintAll();
    }

    private void OnEnable()
    {
        pathData = (PathData)target;
    }

    private void OnSceneGUI()
    {
        CallOnSceneGUI(pathData);
    }

    private void AddParameters(PathPointData pathPointData, bool addGlobalTrigger)
    {
        foreach (var item in PathPointData.allParamNameList)
        {
            if (!pathPointData.TryGetParameter(item, out PathPointParameter param))
            {
                continue;
            }

            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.Label(item, GUILayout.Width(100));

            if (param.IsFloatValue())
            {
                float newValue = EditorGUILayout.FloatField(param.FloatValue, GUILayout.Width(30));
                if (param.FloatValue != newValue)
                {
                    Undo.RecordObject(target, $"Edited {item}");
                    param.FloatValue = newValue;
                    EditorUtility.SetDirty(target);
                }
            }
            else if (param.IsIntValue())
            {
                int newValue = EditorGUILayout.IntField(param.IntValue, GUILayout.Width(30));
                if (param.IntValue != newValue)
                {
                    Undo.RecordObject(target, $"Edited {item}");
                    param.IntValue = newValue;
                    EditorUtility.SetDirty(target);
                }
            }

            if (addGlobalTrigger)
            {
                GUILayout.Space(10);

                bool newValue = EditorGUILayout.Toggle(param.useGlobalValue, GUILayout.Width(15));
                if (param.useGlobalValue != newValue)
                {
                    Undo.RecordObject(target, $"Edited {nameof(param.useGlobalValue)} of {item}");
                    param.useGlobalValue = newValue;
                    EditorUtility.SetDirty(target);
                }

                GUILayout.Label("Use global");
            }

            GUILayout.EndHorizontal();

        }
    }

    private static void DrawPathData(PathData pathData)
    {
        DrawPathEndsTurnDirections(pathData);
    }

    private static void DrawPathEndsTurnDirections(PathData pathData)
    {
        if (pathData.Path.IsClosed || pathData.Path.PointsCount < 2)
        {
            return;
        }

        Vector3 pointFrom = pathData.Path[0];
        Vector3 pointTo = pathData.Path[1];
        DrawTurnDirection(pointFrom, pointTo, pathData.FirstPointTurnDirection);

        int lastPointIndex = pathData.Path.PointsCount - 1;
        pointFrom = pathData.Path[lastPointIndex];
        pointTo = pathData.Path[lastPointIndex - 1];
        DrawTurnDirection(pointFrom, pointTo, pathData.LastPointTurnDirection);
    }

    private static void DrawTurnDirection(Vector3 pointFrom, Vector3 pointTo,
        PathTurnDirection pathEndsTurnDirection)
    {
        const float arcDist = 1f;
        Handles.color = Color.blue;

        Vector3 faceDirectionInPoint = (pointFrom - pointTo).normalized;

        DrawTurnArc(pointFrom, faceDirectionInPoint, arcDist);

        if (pathEndsTurnDirection == PathTurnDirection.Auto)
        {
            return;
        }

        float turnVectorRotation = pathEndsTurnDirection == PathTurnDirection.Left ? -90 : 90;

        Vector3 turnDirection = 
            VectorFunc.GetRotatedUnitVector(faceDirectionInPoint, turnVectorRotation);
        
        Vector3 arrowPosition = pointFrom + arcDist * turnDirection;
        PathEditor.DrawLine(arrowPosition, arrowPosition - arcDist * faceDirectionInPoint);
    }

    private static void DrawTurnArc(Vector3 arcCenter, Vector3 faceDirectionInPoint, float dist)
    {
        Handles.DrawWireArc(arcCenter, Vector3.up,
            VectorFunc.GetRotatedUnitVector(faceDirectionInPoint, -90), 180, dist, 
            PathEditor.LineWidth);
    }
}
