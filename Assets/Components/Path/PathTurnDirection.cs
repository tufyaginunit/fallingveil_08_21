﻿
/// <summary>
/// Направление поворота при начале движения к очередной точке маршрута
/// </summary>
public enum PathTurnDirection
{
    /// <summary>
    /// Автовыбор
    /// </summary>
    Auto,
    /// <summary>
    /// Влево
    /// </summary>
    Left,
    /// <summary>
    /// Вправо
    /// </summary>
    Right
}
