Shader "Stencil/Lit"
{
    Properties
    {
        _Color("Color", Color) = (0, 0, 0, 0)
        [NoScaleOffset]_BaseMap("BaseMap", 2D) = "white" {}
        [NoScaleOffset]_NormalMap("NormalMap", 2D) = "white" {}
        _NormalScale("NormalScale", Range(0, 1)) = 0
        _Tiling("Tiling", Vector) = (1, 1, 0, 0)
        _Offset("Offset", Vector) = (0, 0, 0, 0)
        [NoScaleOffset]_Occlusion("Occlusion", 2D) = "white" {}
        _Metallic("Metallic", Range(0, 1)) = 0
        _OcclusionStrength("OcclusionStrength", Float) = 0
        _Smoothness("Smoothness", Float) = 0
        _AlphaClipThreshold("AlphaClipThreshold", Range(0, 1)) = 0.5
        [HDR]_EmissionColor("EmissionColor", Color) = (0, 0, 0, 0)
        [HideInInspector][NoScaleOffset]unity_Lightmaps("unity_Lightmaps", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_LightmapsInd("unity_LightmapsInd", 2DArray) = "" {}
        [HideInInspector][NoScaleOffset]unity_ShadowMasks("unity_ShadowMasks", 2DArray) = "" {}
        _StencilRef("StencilRef", Float) = 0
        [Enum(UnityEngine.Rendering.CompareFunction)]_StencilComp("_StencilComp (default = Disable) _____Set to NotEqual if you want to mask by specific _StencilRef value, else set to Disable", Float) = 0 //0 = disable

    }
    SubShader
    {
        Tags
        {
            "RenderPipeline"="UniversalPipeline"
            "RenderType"="Transparent"
            "UniversalMaterialType" = "Lit"
            "Queue"="Transparent"
        }
        Pass
        {
            Name "Universal Forward"
            Tags
            {
                "LightMode" = "UniversalForward"
            }

            // Render State
            Cull Back
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite Off

            Stencil
            {
                Ref [_StencilRef]
                Comp [_StencilComp]
                Pass keep
            } 
            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 4.5
        #pragma exclude_renderers gles gles3 glcore
        #pragma multi_compile_instancing
        #pragma multi_compile_fog
        #pragma multi_compile _ DOTS_INSTANCING_ON
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            #pragma multi_compile _ _SCREEN_SPACE_OCCLUSION
        #pragma multi_compile _ LIGHTMAP_ON
        #pragma multi_compile _ DIRLIGHTMAP_COMBINED
        #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
        #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
        #pragma multi_compile _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS _ADDITIONAL_OFF
        #pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
        #pragma multi_compile _ _SHADOWS_SOFT
        #pragma multi_compile _ LIGHTMAP_SHADOW_MIXING
        #pragma multi_compile _ SHADOWS_SHADOWMASK
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define ATTRIBUTES_NEED_TEXCOORD1
            #define VARYINGS_NEED_POSITION_WS
            #define VARYINGS_NEED_NORMAL_WS
            #define VARYINGS_NEED_TANGENT_WS
            #define VARYINGS_NEED_TEXCOORD0
            #define VARYINGS_NEED_VIEWDIRECTION_WS
            #define VARYINGS_NEED_FOG_AND_VERTEX_LIGHT
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_FORWARD
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            float4 uv1 : TEXCOORD1;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float3 positionWS;
            float3 normalWS;
            float4 tangentWS;
            float4 texCoord0;
            float3 viewDirectionWS;
            #if defined(LIGHTMAP_ON)
            float2 lightmapUV;
            #endif
            #if !defined(LIGHTMAP_ON)
            float3 sh;
            #endif
            float4 fogFactorAndVertexLight;
            float4 shadowCoord;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float3 TangentSpaceNormal;
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float3 interp0 : TEXCOORD0;
            float3 interp1 : TEXCOORD1;
            float4 interp2 : TEXCOORD2;
            float4 interp3 : TEXCOORD3;
            float3 interp4 : TEXCOORD4;
            #if defined(LIGHTMAP_ON)
            float2 interp5 : TEXCOORD5;
            #endif
            #if !defined(LIGHTMAP_ON)
            float3 interp6 : TEXCOORD6;
            #endif
            float4 interp7 : TEXCOORD7;
            float4 interp8 : TEXCOORD8;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyz =  input.positionWS;
            output.interp1.xyz =  input.normalWS;
            output.interp2.xyzw =  input.tangentWS;
            output.interp3.xyzw =  input.texCoord0;
            output.interp4.xyz =  input.viewDirectionWS;
            #if defined(LIGHTMAP_ON)
            output.interp5.xy =  input.lightmapUV;
            #endif
            #if !defined(LIGHTMAP_ON)
            output.interp6.xyz =  input.sh;
            #endif
            output.interp7.xyzw =  input.fogFactorAndVertexLight;
            output.interp8.xyzw =  input.shadowCoord;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.positionWS = input.interp0.xyz;
            output.normalWS = input.interp1.xyz;
            output.tangentWS = input.interp2.xyzw;
            output.texCoord0 = input.interp3.xyzw;
            output.viewDirectionWS = input.interp4.xyz;
            #if defined(LIGHTMAP_ON)
            output.lightmapUV = input.interp5.xy;
            #endif
            #if !defined(LIGHTMAP_ON)
            output.sh = input.interp6.xyz;
            #endif
            output.fogFactorAndVertexLight = input.interp7.xyzw;
            output.shadowCoord = input.interp8.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float3 BaseColor;
            float3 NormalTS;
            float3 Emission;
            float Metallic;
            float Smoothness;
            float Occlusion;
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float4 _Property_4df69b746db5405b8aa881df92700524_Out_0 = IsGammaSpace() ? LinearToSRGB(_EmissionColor) : _EmissionColor;
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.BaseColor = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            surface.NormalTS = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            surface.Emission = (_Property_4df69b746db5405b8aa881df92700524_Out_0.xyz);
            surface.Metallic = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            surface.Smoothness = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            surface.Occlusion = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);



            output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);


            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBRForwardPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "GBuffer"
            Tags
            {
                "LightMode" = "UniversalGBuffer"
            }

            // Render State
            Cull Back
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite Off

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 4.5
        #pragma exclude_renderers gles gles3 glcore
        #pragma multi_compile_instancing
        #pragma multi_compile_fog
        #pragma multi_compile _ DOTS_INSTANCING_ON
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            #pragma multi_compile _ LIGHTMAP_ON
        #pragma multi_compile _ DIRLIGHTMAP_COMBINED
        #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
        #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
        #pragma multi_compile _ _SHADOWS_SOFT
        #pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE
        #pragma multi_compile _ _GBUFFER_NORMALS_OCT
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define ATTRIBUTES_NEED_TEXCOORD1
            #define VARYINGS_NEED_POSITION_WS
            #define VARYINGS_NEED_NORMAL_WS
            #define VARYINGS_NEED_TANGENT_WS
            #define VARYINGS_NEED_TEXCOORD0
            #define VARYINGS_NEED_VIEWDIRECTION_WS
            #define VARYINGS_NEED_FOG_AND_VERTEX_LIGHT
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_GBUFFER
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            float4 uv1 : TEXCOORD1;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float3 positionWS;
            float3 normalWS;
            float4 tangentWS;
            float4 texCoord0;
            float3 viewDirectionWS;
            #if defined(LIGHTMAP_ON)
            float2 lightmapUV;
            #endif
            #if !defined(LIGHTMAP_ON)
            float3 sh;
            #endif
            float4 fogFactorAndVertexLight;
            float4 shadowCoord;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float3 TangentSpaceNormal;
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float3 interp0 : TEXCOORD0;
            float3 interp1 : TEXCOORD1;
            float4 interp2 : TEXCOORD2;
            float4 interp3 : TEXCOORD3;
            float3 interp4 : TEXCOORD4;
            #if defined(LIGHTMAP_ON)
            float2 interp5 : TEXCOORD5;
            #endif
            #if !defined(LIGHTMAP_ON)
            float3 interp6 : TEXCOORD6;
            #endif
            float4 interp7 : TEXCOORD7;
            float4 interp8 : TEXCOORD8;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyz =  input.positionWS;
            output.interp1.xyz =  input.normalWS;
            output.interp2.xyzw =  input.tangentWS;
            output.interp3.xyzw =  input.texCoord0;
            output.interp4.xyz =  input.viewDirectionWS;
            #if defined(LIGHTMAP_ON)
            output.interp5.xy =  input.lightmapUV;
            #endif
            #if !defined(LIGHTMAP_ON)
            output.interp6.xyz =  input.sh;
            #endif
            output.interp7.xyzw =  input.fogFactorAndVertexLight;
            output.interp8.xyzw =  input.shadowCoord;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.positionWS = input.interp0.xyz;
            output.normalWS = input.interp1.xyz;
            output.tangentWS = input.interp2.xyzw;
            output.texCoord0 = input.interp3.xyzw;
            output.viewDirectionWS = input.interp4.xyz;
            #if defined(LIGHTMAP_ON)
            output.lightmapUV = input.interp5.xy;
            #endif
            #if !defined(LIGHTMAP_ON)
            output.sh = input.interp6.xyz;
            #endif
            output.fogFactorAndVertexLight = input.interp7.xyzw;
            output.shadowCoord = input.interp8.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float3 BaseColor;
            float3 NormalTS;
            float3 Emission;
            float Metallic;
            float Smoothness;
            float Occlusion;
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float4 _Property_4df69b746db5405b8aa881df92700524_Out_0 = IsGammaSpace() ? LinearToSRGB(_EmissionColor) : _EmissionColor;
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.BaseColor = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            surface.NormalTS = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            surface.Emission = (_Property_4df69b746db5405b8aa881df92700524_Out_0.xyz);
            surface.Metallic = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            surface.Smoothness = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            surface.Occlusion = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);



            output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);


            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/UnityGBuffer.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBRGBufferPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "ShadowCaster"
            Tags
            {
                "LightMode" = "ShadowCaster"
            }

            // Render State
            Cull Back
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite On
        ColorMask 0

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 4.5
        #pragma exclude_renderers gles gles3 glcore
        #pragma multi_compile_instancing
        #pragma multi_compile _ DOTS_INSTANCING_ON
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define VARYINGS_NEED_TEXCOORD0
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_SHADOWCASTER
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float4 texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float4 interp0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyzw =  input.texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.texCoord0 = input.interp0.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShadowCasterPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "DepthOnly"
            Tags
            {
                "LightMode" = "DepthOnly"
            }

            // Render State
            Cull Back
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite On
        ColorMask 0

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 4.5
        #pragma exclude_renderers gles gles3 glcore
        #pragma multi_compile_instancing
        #pragma multi_compile _ DOTS_INSTANCING_ON
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define VARYINGS_NEED_TEXCOORD0
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_DEPTHONLY
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float4 texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float4 interp0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyzw =  input.texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.texCoord0 = input.interp0.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthOnlyPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "DepthNormals"
            Tags
            {
                "LightMode" = "DepthNormals"
            }

            // Render State
            Cull Back
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite On

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 4.5
        #pragma exclude_renderers gles gles3 glcore
        #pragma multi_compile_instancing
        #pragma multi_compile _ DOTS_INSTANCING_ON
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define ATTRIBUTES_NEED_TEXCOORD1
            #define VARYINGS_NEED_NORMAL_WS
            #define VARYINGS_NEED_TANGENT_WS
            #define VARYINGS_NEED_TEXCOORD0
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_DEPTHNORMALSONLY
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            float4 uv1 : TEXCOORD1;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float3 normalWS;
            float4 tangentWS;
            float4 texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float3 TangentSpaceNormal;
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float3 interp0 : TEXCOORD0;
            float4 interp1 : TEXCOORD1;
            float4 interp2 : TEXCOORD2;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyz =  input.normalWS;
            output.interp1.xyzw =  input.tangentWS;
            output.interp2.xyzw =  input.texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.normalWS = input.interp0.xyz;
            output.tangentWS = input.interp1.xyzw;
            output.texCoord0 = input.interp2.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float3 NormalTS;
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.NormalTS = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);



            output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);


            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthNormalsOnlyPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "Meta"
            Tags
            {
                "LightMode" = "Meta"
            }

            // Render State
            Cull Off

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 4.5
        #pragma exclude_renderers gles gles3 glcore
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            #pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define ATTRIBUTES_NEED_TEXCOORD1
            #define ATTRIBUTES_NEED_TEXCOORD2
            #define VARYINGS_NEED_TEXCOORD0
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_META
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            float4 uv1 : TEXCOORD1;
            float4 uv2 : TEXCOORD2;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float4 texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float4 interp0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyzw =  input.texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.texCoord0 = input.interp0.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float3 BaseColor;
            float3 Emission;
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float4 _Property_4df69b746db5405b8aa881df92700524_Out_0 = IsGammaSpace() ? LinearToSRGB(_EmissionColor) : _EmissionColor;
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.BaseColor = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            surface.Emission = (_Property_4df69b746db5405b8aa881df92700524_Out_0.xyz);
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/LightingMetaPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            // Name: <None>
            Tags
            {
                "LightMode" = "Universal2D"
            }

            // Render State
            Cull Back
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite Off

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 4.5
        #pragma exclude_renderers gles gles3 glcore
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define VARYINGS_NEED_TEXCOORD0
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_2D
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float4 texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float4 interp0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyzw =  input.texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.texCoord0 = input.interp0.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float3 BaseColor;
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.BaseColor = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBR2DPass.hlsl"

            ENDHLSL
        }
    }
    SubShader
    {
        Tags
        {
            "RenderPipeline"="UniversalPipeline"
            "RenderType"="Transparent"
            "UniversalMaterialType" = "Lit"
            "Queue"="Transparent"
        }
        Pass
        {
            Name "Universal Forward"
            Tags
            {
                "LightMode" = "UniversalForward"
            }

            // Render State
            Cull Back
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite Off

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 2.0
        #pragma only_renderers gles gles3 glcore d3d11
        #pragma multi_compile_instancing
        #pragma multi_compile_fog
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            #pragma multi_compile _ _SCREEN_SPACE_OCCLUSION
        #pragma multi_compile _ LIGHTMAP_ON
        #pragma multi_compile _ DIRLIGHTMAP_COMBINED
        #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
        #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
        #pragma multi_compile _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS _ADDITIONAL_OFF
        #pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
        #pragma multi_compile _ _SHADOWS_SOFT
        #pragma multi_compile _ LIGHTMAP_SHADOW_MIXING
        #pragma multi_compile _ SHADOWS_SHADOWMASK
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define ATTRIBUTES_NEED_TEXCOORD1
            #define VARYINGS_NEED_POSITION_WS
            #define VARYINGS_NEED_NORMAL_WS
            #define VARYINGS_NEED_TANGENT_WS
            #define VARYINGS_NEED_TEXCOORD0
            #define VARYINGS_NEED_VIEWDIRECTION_WS
            #define VARYINGS_NEED_FOG_AND_VERTEX_LIGHT
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_FORWARD
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            float4 uv1 : TEXCOORD1;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float3 positionWS;
            float3 normalWS;
            float4 tangentWS;
            float4 texCoord0;
            float3 viewDirectionWS;
            #if defined(LIGHTMAP_ON)
            float2 lightmapUV;
            #endif
            #if !defined(LIGHTMAP_ON)
            float3 sh;
            #endif
            float4 fogFactorAndVertexLight;
            float4 shadowCoord;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float3 TangentSpaceNormal;
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float3 interp0 : TEXCOORD0;
            float3 interp1 : TEXCOORD1;
            float4 interp2 : TEXCOORD2;
            float4 interp3 : TEXCOORD3;
            float3 interp4 : TEXCOORD4;
            #if defined(LIGHTMAP_ON)
            float2 interp5 : TEXCOORD5;
            #endif
            #if !defined(LIGHTMAP_ON)
            float3 interp6 : TEXCOORD6;
            #endif
            float4 interp7 : TEXCOORD7;
            float4 interp8 : TEXCOORD8;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyz =  input.positionWS;
            output.interp1.xyz =  input.normalWS;
            output.interp2.xyzw =  input.tangentWS;
            output.interp3.xyzw =  input.texCoord0;
            output.interp4.xyz =  input.viewDirectionWS;
            #if defined(LIGHTMAP_ON)
            output.interp5.xy =  input.lightmapUV;
            #endif
            #if !defined(LIGHTMAP_ON)
            output.interp6.xyz =  input.sh;
            #endif
            output.interp7.xyzw =  input.fogFactorAndVertexLight;
            output.interp8.xyzw =  input.shadowCoord;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.positionWS = input.interp0.xyz;
            output.normalWS = input.interp1.xyz;
            output.tangentWS = input.interp2.xyzw;
            output.texCoord0 = input.interp3.xyzw;
            output.viewDirectionWS = input.interp4.xyz;
            #if defined(LIGHTMAP_ON)
            output.lightmapUV = input.interp5.xy;
            #endif
            #if !defined(LIGHTMAP_ON)
            output.sh = input.interp6.xyz;
            #endif
            output.fogFactorAndVertexLight = input.interp7.xyzw;
            output.shadowCoord = input.interp8.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float3 BaseColor;
            float3 NormalTS;
            float3 Emission;
            float Metallic;
            float Smoothness;
            float Occlusion;
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float4 _Property_4df69b746db5405b8aa881df92700524_Out_0 = IsGammaSpace() ? LinearToSRGB(_EmissionColor) : _EmissionColor;
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.BaseColor = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            surface.NormalTS = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            surface.Emission = (_Property_4df69b746db5405b8aa881df92700524_Out_0.xyz);
            surface.Metallic = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            surface.Smoothness = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            surface.Occlusion = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);



            output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);


            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBRForwardPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "ShadowCaster"
            Tags
            {
                "LightMode" = "ShadowCaster"
            }

            // Render State
            Cull Back
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite On
        ColorMask 0

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 2.0
        #pragma only_renderers gles gles3 glcore d3d11
        #pragma multi_compile_instancing
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define VARYINGS_NEED_TEXCOORD0
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_SHADOWCASTER
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float4 texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float4 interp0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyzw =  input.texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.texCoord0 = input.interp0.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShadowCasterPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "DepthOnly"
            Tags
            {
                "LightMode" = "DepthOnly"
            }

            // Render State
            Cull Back
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite On
        ColorMask 0

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 2.0
        #pragma only_renderers gles gles3 glcore d3d11
        #pragma multi_compile_instancing
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define VARYINGS_NEED_TEXCOORD0
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_DEPTHONLY
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float4 texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float4 interp0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyzw =  input.texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.texCoord0 = input.interp0.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthOnlyPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "DepthNormals"
            Tags
            {
                "LightMode" = "DepthNormals"
            }

            // Render State
            Cull Back
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite On

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 2.0
        #pragma only_renderers gles gles3 glcore d3d11
        #pragma multi_compile_instancing
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define ATTRIBUTES_NEED_TEXCOORD1
            #define VARYINGS_NEED_NORMAL_WS
            #define VARYINGS_NEED_TANGENT_WS
            #define VARYINGS_NEED_TEXCOORD0
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_DEPTHNORMALSONLY
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            float4 uv1 : TEXCOORD1;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float3 normalWS;
            float4 tangentWS;
            float4 texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float3 TangentSpaceNormal;
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float3 interp0 : TEXCOORD0;
            float4 interp1 : TEXCOORD1;
            float4 interp2 : TEXCOORD2;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyz =  input.normalWS;
            output.interp1.xyzw =  input.tangentWS;
            output.interp2.xyzw =  input.texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.normalWS = input.interp0.xyz;
            output.tangentWS = input.interp1.xyzw;
            output.texCoord0 = input.interp2.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float3 NormalTS;
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.NormalTS = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);



            output.TangentSpaceNormal =          float3(0.0f, 0.0f, 1.0f);


            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/DepthNormalsOnlyPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            Name "Meta"
            Tags
            {
                "LightMode" = "Meta"
            }

            // Render State
            Cull Off

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 2.0
        #pragma only_renderers gles gles3 glcore d3d11
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            #pragma shader_feature _ _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define ATTRIBUTES_NEED_TEXCOORD1
            #define ATTRIBUTES_NEED_TEXCOORD2
            #define VARYINGS_NEED_TEXCOORD0
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_META
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            float4 uv1 : TEXCOORD1;
            float4 uv2 : TEXCOORD2;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float4 texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float4 interp0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyzw =  input.texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.texCoord0 = input.interp0.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float3 BaseColor;
            float3 Emission;
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float4 _Property_4df69b746db5405b8aa881df92700524_Out_0 = IsGammaSpace() ? LinearToSRGB(_EmissionColor) : _EmissionColor;
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.BaseColor = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            surface.Emission = (_Property_4df69b746db5405b8aa881df92700524_Out_0.xyz);
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/LightingMetaPass.hlsl"

            ENDHLSL
        }
        Pass
        {
            // Name: <None>
            Tags
            {
                "LightMode" = "Universal2D"
            }

            // Render State
            Cull Back
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
        ZTest LEqual
        ZWrite Off

            // Debug
            // <None>

            // --------------------------------------------------
            // Pass

            HLSLPROGRAM

            // Pragmas
            #pragma target 2.0
        #pragma only_renderers gles gles3 glcore d3d11
        #pragma multi_compile_instancing
        #pragma vertex vert
        #pragma fragment frag

            // DotsInstancingOptions: <None>
            // HybridV1InjectedBuiltinProperties: <None>

            // Keywords
            // PassKeywords: <None>
            // GraphKeywords: <None>

            // Defines
            #define _SURFACE_TYPE_TRANSPARENT 1
            #define _AlphaClip 1
            #define _NORMALMAP 1
            #define _NORMAL_DROPOFF_TS 1
            #define ATTRIBUTES_NEED_NORMAL
            #define ATTRIBUTES_NEED_TANGENT
            #define ATTRIBUTES_NEED_TEXCOORD0
            #define VARYINGS_NEED_TEXCOORD0
            #define FEATURES_GRAPH_VERTEX
            /* WARNING: $splice Could not find named fragment 'PassInstancing' */
            #define SHADERPASS SHADERPASS_2D
            /* WARNING: $splice Could not find named fragment 'DotsInstancingVars' */

            // Includes
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Texture.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/TextureStack.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

            // --------------------------------------------------
            // Structs and Packing

            struct Attributes
        {
            float3 positionOS : POSITION;
            float3 normalOS : NORMAL;
            float4 tangentOS : TANGENT;
            float4 uv0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : INSTANCEID_SEMANTIC;
            #endif
        };
        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float4 texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };
        struct SurfaceDescriptionInputs
        {
            float4 uv0;
        };
        struct VertexDescriptionInputs
        {
            float3 ObjectSpaceNormal;
            float3 ObjectSpaceTangent;
            float3 ObjectSpacePosition;
        };
        struct PackedVaryings
        {
            float4 positionCS : SV_POSITION;
            float4 interp0 : TEXCOORD0;
            #if UNITY_ANY_INSTANCING_ENABLED
            uint instanceID : CUSTOM_INSTANCE_ID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            uint stereoTargetEyeIndexAsBlendIdx0 : BLENDINDICES0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            uint stereoTargetEyeIndexAsRTArrayIdx : SV_RenderTargetArrayIndex;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            FRONT_FACE_TYPE cullFace : FRONT_FACE_SEMANTIC;
            #endif
        };

            PackedVaryings PackVaryings (Varyings input)
        {
            PackedVaryings output;
            output.positionCS = input.positionCS;
            output.interp0.xyzw =  input.texCoord0;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }
        Varyings UnpackVaryings (PackedVaryings input)
        {
            Varyings output;
            output.positionCS = input.positionCS;
            output.texCoord0 = input.interp0.xyzw;
            #if UNITY_ANY_INSTANCING_ENABLED
            output.instanceID = input.instanceID;
            #endif
            #if (defined(UNITY_STEREO_MULTIVIEW_ENABLED)) || (defined(UNITY_STEREO_INSTANCING_ENABLED) && (defined(SHADER_API_GLES3) || defined(SHADER_API_GLCORE)))
            output.stereoTargetEyeIndexAsBlendIdx0 = input.stereoTargetEyeIndexAsBlendIdx0;
            #endif
            #if (defined(UNITY_STEREO_INSTANCING_ENABLED))
            output.stereoTargetEyeIndexAsRTArrayIdx = input.stereoTargetEyeIndexAsRTArrayIdx;
            #endif
            #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
            output.cullFace = input.cullFace;
            #endif
            return output;
        }

            // --------------------------------------------------
            // Graph

            // Graph Properties
            CBUFFER_START(UnityPerMaterial)
        float4 _Color;
        float4 _BaseMap_TexelSize;
        float4 _NormalMap_TexelSize;
        float _NormalScale;
        float2 _Tiling;
        float2 _Offset;
        float4 _Occlusion_TexelSize;
        float _Metallic;
        float _OcclusionStrength;
        float _Smoothness;
        float _AlphaClipThreshold;
        float4 _EmissionColor;
        CBUFFER_END

        // Object and Global properties
        SAMPLER(SamplerState_Linear_Repeat);
        TEXTURE2D(_BaseMap);
        SAMPLER(sampler_BaseMap);
        TEXTURE2D(_NormalMap);
        SAMPLER(sampler_NormalMap);
        TEXTURE2D(_Occlusion);
        SAMPLER(sampler_Occlusion);

            // Graph Functions
            
        void Unity_TilingAndOffset_float(float2 UV, float2 Tiling, float2 Offset, out float2 Out)
        {
            Out = UV * Tiling + Offset;
        }

        void Unity_Multiply_float(float4 A, float4 B, out float4 Out)
        {
            Out = A * B;
        }

        void Unity_NormalStrength_float(float3 In, float Strength, out float3 Out)
        {
            Out = float3(In.rg * Strength, lerp(1, In.b, saturate(Strength)));
        }

        void Unity_Lerp_float(float A, float B, float T, out float Out)
        {
            Out = lerp(A, B, T);
        }

        void Unity_Multiply_float(float A, float B, out float Out)
        {
            Out = A * B;
        }

        void Unity_OneMinus_float(float In, out float Out)
        {
            Out = 1 - In;
        }

        void Unity_Add_float(float A, float B, out float Out)
        {
            Out = A + B;
        }

        struct Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa
        {
            half4 uv0;
        };

        void SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(float4 Color_4AE0AC13, UnityTexture2D Texture2D_E1E878EA, UnityTexture2D Texture2D_31BC5372, float Vector1_5B1B16DB, float2 Vector2_CEDA4754, float2 Vector2_D888B890, UnityTexture2D Texture2D_244001D7, float Vector1_AF14CD8F, float Vector1_A88E4ECE, float Vector1_75CD6DF1, Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa IN, out float3 OutAlbedo_1, out float3 OutNormal_2, out float OutMetallic_3, out float OutSmoothness_5, out float OutOcclusion_4, out float Alpha_6)
        {
            float4 _Property_b4a4c448dd05998cba3b83938680b2c8_Out_0 = Color_4AE0AC13;
            UnityTexture2D _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0 = Texture2D_E1E878EA;
            float2 _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0 = Vector2_CEDA4754;
            float2 _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0 = Vector2_D888B890;
            float2 _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3;
            Unity_TilingAndOffset_float(IN.uv0.xy, _Property_c76499329235d485a5c23c7da6cbfa8c_Out_0, _Property_0be03667fefe3989b2b16fcaeda1bd59_Out_0, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float4 _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0 = SAMPLE_TEXTURE2D(_Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.tex, _Property_e7351e85b1eb6284bd8da100dcd642e8_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_R_4 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.r;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_G_5 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.g;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_B_6 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.b;
            float _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_A_7 = _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0.a;
            float4 _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2;
            Unity_Multiply_float(_Property_b4a4c448dd05998cba3b83938680b2c8_Out_0, _SampleTexture2D_7c7e4acaad08b38aa88bcab08ec82926_RGBA_0, _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2);
            UnityTexture2D _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0 = Texture2D_31BC5372;
            float4 _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.tex, _Property_2f71ec8fc12ed98091a581a2e13b684e_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.rgb = UnpackNormal(_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0);
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_R_4 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.r;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_G_5 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.g;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_B_6 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.b;
            float _SampleTexture2D_382d1125d05f6b8693a959042642a516_A_7 = _SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.a;
            float _Property_36fcb9e613c0998595213066d92b814c_Out_0 = Vector1_5B1B16DB;
            float3 _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            Unity_NormalStrength_float((_SampleTexture2D_382d1125d05f6b8693a959042642a516_RGBA_0.xyz), _Property_36fcb9e613c0998595213066d92b814c_Out_0, _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2);
            UnityTexture2D _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0 = Texture2D_244001D7;
            float4 _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0 = SAMPLE_TEXTURE2D(_Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.tex, _Property_2a7ae03c6e678288a455928c54abf4bf_Out_0.samplerstate, _TilingAndOffset_ad9638158054528b8ecf84fe0eb68235_Out_3);
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.r;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.g;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_B_6 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.b;
            float _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7 = _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_RGBA_0.a;
            float _Property_347641f665acd58ca45602e407004e42_Out_0 = Vector1_AF14CD8F;
            float _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            Unity_Lerp_float(0, _SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_R_4, _Property_347641f665acd58ca45602e407004e42_Out_0, _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3);
            float _Property_4662bac86bad768181dd2e7063c45df9_Out_0 = Vector1_75CD6DF1;
            float _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_A_7, _Property_4662bac86bad768181dd2e7063c45df9_Out_0, _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2);
            float _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0 = Vector1_A88E4ECE;
            float _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2;
            Unity_Multiply_float(_SampleTexture2D_4a25fc79d4f54a81a6a67181d29626ce_G_5, _Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2);
            float _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1;
            Unity_OneMinus_float(_Property_2220fce8b419a48fb0c82a4b9ff6676d_Out_0, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1);
            float _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Unity_Add_float(_Multiply_eae17a3173e4938d8686d4aafd2e3d5c_Out_2, _OneMinus_7d149b6a34668288a111aa3176d6c1a8_Out_1, _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2);
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_R_1 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[0];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_G_2 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[1];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_B_3 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[2];
            float _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4 = _Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2[3];
            OutAlbedo_1 = (_Multiply_ed8ae55e6853fa8c87b53402c2d59680_Out_2.xyz);
            OutNormal_2 = _NormalStrength_0d3aa5554fd50286aed4276459f7ae04_Out_2;
            OutMetallic_3 = _Lerp_d383aee966e5ea8aa4c2923c99bc9e0a_Out_3;
            OutSmoothness_5 = _Multiply_e97a1a21dcfa9485afa370f68df4a49b_Out_2;
            OutOcclusion_4 = _Add_2205b2f8cfc34c8794a474dec0ee5ec2_Out_2;
            Alpha_6 = _Split_ff0860a1c6b649ba8ec0c8e855a6ae42_A_4;
        }

            // Graph Vertex
            struct VertexDescription
        {
            float3 Position;
            float3 Normal;
            float3 Tangent;
        };

        VertexDescription VertexDescriptionFunction(VertexDescriptionInputs IN)
        {
            VertexDescription description = (VertexDescription)0;
            description.Position = IN.ObjectSpacePosition;
            description.Normal = IN.ObjectSpaceNormal;
            description.Tangent = IN.ObjectSpaceTangent;
            return description;
        }

            // Graph Pixel
            struct SurfaceDescription
        {
            float3 BaseColor;
            float Alpha;
            float AlphaClipThreshold;
        };

        SurfaceDescription SurfaceDescriptionFunction(SurfaceDescriptionInputs IN)
        {
            SurfaceDescription surface = (SurfaceDescription)0;
            float4 _Property_e9c1c007bf314753913be444145d8a91_Out_0 = _Color;
            UnityTexture2D _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0 = UnityBuildTexture2DStructNoScale(_BaseMap);
            UnityTexture2D _Property_8581c84343d6489aae368d6dea184550_Out_0 = UnityBuildTexture2DStructNoScale(_NormalMap);
            float _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0 = _NormalScale;
            float2 _Property_e1362fd9ac7d492d9de873df194060dd_Out_0 = _Tiling;
            float2 _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0 = _Offset;
            UnityTexture2D _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0 = UnityBuildTexture2DStructNoScale(_Occlusion);
            float _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0 = _Metallic;
            float _Property_bf94bfcb871646cfb10a9802039ea350_Out_0 = _OcclusionStrength;
            float _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0 = _Smoothness;
            Bindings_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3;
            _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3.uv0 = IN.uv0;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            float3 _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4;
            float _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            SG_PBRMetallicSubGraph_c3e2a18295de6ab49a927e0a79be53aa(_Property_e9c1c007bf314753913be444145d8a91_Out_0, _Property_06ff2b4730864c99a4a1900975ea5fdf_Out_0, _Property_8581c84343d6489aae368d6dea184550_Out_0, _Property_0ba75adbbf5a46de8dd85408ffa00ce1_Out_0, _Property_e1362fd9ac7d492d9de873df194060dd_Out_0, _Property_2733b1b6a1814b5bbad181390c74ae5f_Out_0, _Property_2ee310e9b5144cca809d3e0f10ab08cf_Out_0, _Property_2fbefbe1ca984d28b4487fe0efcef4d2_Out_0, _Property_bf94bfcb871646cfb10a9802039ea350_Out_0, _Property_3685edcab3c24135a7c529a6c1bd842e_Out_0, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutNormal_2, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutMetallic_3, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutSmoothness_5, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutOcclusion_4, _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6);
            float _Property_b319139b7e374804a760f084931670b3_Out_0 = _AlphaClipThreshold;
            surface.BaseColor = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_OutAlbedo_1;
            surface.Alpha = _PBRMetallicSubGraph_be9f5ed74d09435eac6b798bfacaf2d3_Alpha_6;
            surface.AlphaClipThreshold = _Property_b319139b7e374804a760f084931670b3_Out_0;
            return surface;
        }

            // --------------------------------------------------
            // Build Graph Inputs

            VertexDescriptionInputs BuildVertexDescriptionInputs(Attributes input)
        {
            VertexDescriptionInputs output;
            ZERO_INITIALIZE(VertexDescriptionInputs, output);

            output.ObjectSpaceNormal =           input.normalOS;
            output.ObjectSpaceTangent =          input.tangentOS.xyz;
            output.ObjectSpacePosition =         input.positionOS;

            return output;
        }
            SurfaceDescriptionInputs BuildSurfaceDescriptionInputs(Varyings input)
        {
            SurfaceDescriptionInputs output;
            ZERO_INITIALIZE(SurfaceDescriptionInputs, output);





            output.uv0 =                         input.texCoord0;
        #if defined(SHADER_STAGE_FRAGMENT) && defined(VARYINGS_NEED_CULLFACE)
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN output.FaceSign =                    IS_FRONT_VFACE(input.cullFace, true, false);
        #else
        #define BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN
        #endif
        #undef BUILD_SURFACE_DESCRIPTION_INPUTS_OUTPUT_FACESIGN

            return output;
        }

            // --------------------------------------------------
            // Main

            #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/ShaderPass.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/Varyings.hlsl"
        #include "Packages/com.unity.render-pipelines.universal/Editor/ShaderGraph/Includes/PBR2DPass.hlsl"

            ENDHLSL
        }
    }
    CustomEditor "ShaderGraph.PBRMasterGUI"
    FallBack "Hidden/Shader Graph/FallbackError"
}