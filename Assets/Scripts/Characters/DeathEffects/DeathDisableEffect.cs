﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Эффект, уничтожающий (выключающий) модель через некоторое время после смерти
/// </summary>
public class DeathDisableEffect : DeathEffect
{
    /// <summary>
    /// Задать состояние объекта при возрождении на контрольной точке.
    /// </summary>
    /// <param name="state"></param>
    public override void SetState(object state)
    {
        base.SetState(state);
        damageableObject.DamageableCollider.enabled = true;
        damageableObject.gameObject.SetActive(true);
    }

    /// <summary>
    /// Обработать событие смерти
    /// </summary>
    protected override void ProcessDeath()
    {
        // Коллайдер выключим сразу
        damageableObject.DamageableCollider.enabled = false;
        // Модель объекта выключаем через некоторое время
        StartCoroutine(DisableAfterTime(deathDuration));
    }

    /// <summary>
    /// Корутина, выключающая (делающая невидимым) объект через некоторое время
    /// </summary>
    /// <returns></returns>
    private IEnumerator DisableAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        damageableObject.gameObject.SetActive(false);
    }
}
