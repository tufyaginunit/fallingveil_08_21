﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Эффект смерти врага, осуществляющий плавное изменение Y-координаты
/// </summary>
public class YShiftDeathEffect : DeathEffect
{
    [SerializeField]
    [Tooltip("Смещение по Y-координате объекта " +
        "(для плавного погружения под землю или в небо)")]
    protected float smoothYShift;

    /// <summary>
    /// Обработать событие смерти
    /// </summary>
    protected override void ProcessDeath()
    {
        if (smoothYShift != 0)
        {
            StartCoroutine(PerformSmoothShift(deathDuration));
        }
    }

    /// <summary>
    /// Корутина, выполняющая плавное смещение по Y-координате
    /// </summary>
    /// <returns></returns>
    private IEnumerator PerformSmoothShift(float time)
    {
        float coordChangeSpeed = smoothYShift / time;
        float endTime = Time.time + deathDuration;
        while (Time.time < endTime)
        {
            damageableObject.Transform.Translate(new Vector3(0, coordChangeSpeed * Time.deltaTime, 0));
            yield return null;
        }
    }
}
