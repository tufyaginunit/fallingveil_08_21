using UnityEngine;

/// <summary>
/// ����������� ����� ������� ������ ������������� �������.
/// </summary>
public abstract class DeathEffect : MonoBehaviour, ISaveable
{
    [SerializeField, Tooltip("������������ ������")]
    protected DamageableObject damageableObject;

    [SerializeField, Tooltip("������������ ������� ������")]
    protected float deathDuration;

    /// <summary>
    /// ������ ��������� �������. 
    /// ������������ ��� �������������� ��������� ������� ��� ����������� �� ����������� �����.
    /// ������������ ����� ����� ���� ����������� ��� �������� �� �����,
    /// �� ������ �� ���������� ��� ������ ��� ����, ����� ��� ������ ���� � ����������� �����
    /// ������������ �������������� ��������� �������
    /// </summary>
    /// <param name="state"></param>
    public virtual void SetState(object state)
    {
    }

    /// <summary>
    /// �������� ��������� �������.
    /// ������������ ����� ����� ���� ����������� ��� ���������� ������� � ����,
    /// ������ ��� �� ���������, ������� ���������� ������ ������ � �������� ���������
    /// </summary>
    /// <returns></returns>
    public virtual object GetState()
    {
        return "";
    }

    /// <summary>
    /// ���������� ������� ������
    /// </summary>
    protected abstract void ProcessDeath();

    protected virtual void Awake()
    {
        damageableObject.RegisterKilledListener(ProcessDeath);
    }

    protected virtual void OnDestroy()
    {
        if (damageableObject != null)
        {
            damageableObject.UnregisterKilledListener(ProcessDeath);
        }
    }
}
