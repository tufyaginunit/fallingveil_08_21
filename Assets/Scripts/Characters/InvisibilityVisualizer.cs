﻿using UnityEngine;

/// <summary>
/// Визуализатор невидимости объекта
/// </summary>
public class InvisibilityVisualizer : MonoBehaviour
{
    [SerializeField, Tooltip("Объект, невидимость которого нужно визуализировать")]
    private DamageableObject invisibleObject = null;
    
    [SerializeField, Tooltip("Модель персонажа, материалы которого будут изменяться")]
    private Transform model = null;
    
    [SerializeField, Tooltip("Стратегия изменения материалов модели")]
    private OpacityChangingStrategy changingStrategy;

    /// <summary>
    /// Объект, изменяющий свои материалы
    /// </summary>
    private MaterialChangingObject materialChangingObject = null;

    private void OnEnable()
    {
        materialChangingObject = new MaterialChangingObject(model, changingStrategy);

        invisibleObject.RegisterInvisibilityChangedListener(OnInvisibilityChanged);
    }

    private void OnDisable()
    {
        invisibleObject.UnregisterHealthChangedListener(OnInvisibilityChanged);
    }

    private void OnInvisibilityChanged()
    {
        materialChangingObject.SetNormalizedValue(invisibleObject.IsInvisible() ? 0 : 1);
    }
}
