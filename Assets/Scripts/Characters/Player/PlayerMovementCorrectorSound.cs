﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Контроллер звука корректора движения игрока
/// </summary>
public class PlayerMovementCorrectorSound : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Контроллер включен/выключен")]
    private bool isEnabled = true;

    [SerializeField]
    [Tooltip("Корректор движения")]
    private PlayerMovementCorrector movementCorrector = null;

    [SerializeField]
    [Tooltip("Звук, проигрываемый при движении в неправильном направлении")]
    private AudioSource wrongDirectionSound;

    private void Awake()
    {
        movementCorrector.RegisterCorrectedByChangingObstaclesListener(OnMovementCorrectedByChangingObstacles);
    }

    private void OnDestroy()
    {
        if (movementCorrector != null)
        {
            movementCorrector.UnregisterCorrectedByChangingObstaclesListener(OnMovementCorrectedByChangingObstacles);
        }
    }

    /// <summary>
    /// Обработчик события корректировки движения по прозрачным препятствиям 
    /// </summary>
    /// <param name="obstacles"></param>
    private void OnMovementCorrectedByChangingObstacles(List<ChangingObstacle> obstacles)
    {
        if (!isEnabled || obstacles == null || obstacles.Count == 0)
        {
            return;
        }

        if (!wrongDirectionSound.isPlaying)
        {
            wrongDirectionSound.Play();
        }
    }
}
