using System.Collections;
using UnityEngine;

/// <summary>
/// ���������� ������. ��������� ������� �������� � �������� ������, ���������� �� PlayerInput.
/// ��������� ������ ����� �� ���� ���������� � ������� ��������, ���������� � �������� ������.
/// ����������� �� ������� ������ ������.
/// �������, ����� �� ������ ������ ��� �������� Rigidbody � ���������.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class PlayerController : MovingObject
{    
    /// <summary>
    /// ��������� ���������. ��������, ������� ����� ������� ��� �������� �������� �������� ������
    /// � ���� ��� ������� � RigidBody
    /// </summary>
    private const float speedToForceConvertShift = 0.5f;

    [SerializeField]
    [Tooltip("��������� ��������")]
    private PlayerMovementCorrector movementCorrector = null;

    /// <summary>
    /// Rigidbody ������
    /// </summary>
    private Rigidbody playerRigidbody;

    /// <summary>
    /// ����, � ������� �������������� ����������� �� Rigidbody ��� ��������
    /// </summary>
    private float playerForce;

    /// <summary>
    /// ������� �������� �������� ������������ ��������
    /// </summary>
    private Quaternion targetRotation;

    /// <summary>
    /// ������� �������� ������
    /// </summary>
    public override Vector3 CurrentVelocity => playerRigidbody.velocity;

    /// <summary>
    /// ���������� ������� ��������
    /// </summary>
    public override void ResetSpeed()
    {
        playerRigidbody.velocity = Vector3.zero;
        playerRigidbody.angularVelocity = Vector3.zero;
        targetRotation = transform.rotation;
    }

    protected override void Awake()
    {
        base.Awake();

        targetRotation = transform.rotation;

        playerRigidbody = GetComponent<Rigidbody>();

        movementCorrector.Initialize(playerRigidbody, GetComponent<CapsuleCollider>());

        // ������������ �������� � ����
        playerForce = (speed + speedToForceConvertShift) * playerRigidbody.mass * playerRigidbody.drag;
    }

    /// <summary>
    /// �������� ������ �� ���� X, Z
    /// </summary>
    /// <param name="movementDirection">������� ����������� ��������</param>
    public override void Move(Vector3 movementDirection)
    {
        movementDirection = movementCorrector.GetCorrectedMovementVector(movementDirection);
        if (movementDirection != Vector3.zero)
        {
            playerRigidbody.AddForce(movementDirection * playerForce);
        }
    }

    /// <summary>
    /// ��������� ������� ������, ����� ����� ��������� ����� ������ ����� � �����
    /// </summary>
    /// <param name="lookAtPoint">�����, � ������� ����� �����������</param>
    public virtual void RotateSmooth(Vector3 lookAtPoint)
    {
        lookAtPoint.y = transform.position.y;
        targetRotation = Quaternion.LookRotation(lookAtPoint - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation,
            Time.fixedDeltaTime * rotationSpeed);
    }

    /// <summary>
    /// �������� ������� ����������� ��������: ������ ��� �����
    /// </summary>
    /// <returns>1 - ������� �� ������� �������, -1 - ������, 0 - �������� �� ���������</returns>
    public override int GetTurnDirection()
    {
        const float minRotationDifferenceForTurn = 1;

        // ���� ������� �������� �������� ��������� � �������, ����������, � ����� ����������� �� ����� ��������������
        float angle1 = transform.rotation.eulerAngles.y;
        float angle2 = targetRotation.eulerAngles.y;
        float difference = angle2 - angle1;

        float differenceAbs = Mathf.Abs(difference);
        // �� ����� ������������ �������, ���� ������� ����� ������ ����� ����.
        if (differenceAbs <= minRotationDifferenceForTurn)
        {
            return 0;
        }

        // ���� ������� �������������, ������ ������� ������� ������ ��������, � �� �������������� �� ������� �������,
        // � ��������� ������ - ������ �������.
        // ������, ���� ������� ����� ������ ������ 180, �� ����� ������������� 
        if (differenceAbs > 180)
        {
            difference += difference > 0 ? -360 : 360;
        }

        return difference > 0 ? 1 : -1;
    }
}
