﻿using UnityEngine;

/// <summary>
/// Разблокировщик способностей персонажа
/// </summary>
public class PlayerAbilityUnlocker : MonoBehaviour
{
    [SerializeField, Tooltip("Разблокируемые действия")]
    private AllowedInput newAbilities = AllowedInput.None;

    [SerializeField, Tooltip("Объект-триггер, на который нужно наступить для разблокировки")]
    private TriggerEvent triggerEvent = null;

    [SerializeField, Tooltip("Запускаемое обучение")]
    private TutorialController tutorial = null;

    /// <summary>
    /// Действия, доступные персонажем перед разблокировкой новой способности
    /// </summary>
    private AllowedInput previousAllowedInput = AllowedInput.None;

    private void OnEnable()
    {
        triggerEvent.Entered += OnTriggerEventEnter;
    }

    private void OnDisable()
    {
        triggerEvent.Entered -= OnTriggerEventEnter;
    }

    /// <summary>
    /// Обработчик события входа в триггер активации новой способности
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEventEnter(Collider other)
    {
        if (other.transform == GameManager.Instance.Player.transform)
        {
            previousAllowedInput = GameManager.Instance.Controller.PlayerInput.AllowedInput;
            if (tutorial.IsEnabled &&
                StoredGameDataManager.IntroOptions.GetPlayTutorialOption())
            {
                tutorial.TimeLine.stopped += (t) => OnTutorialFinished();
                tutorial.Play();
            }
            else
            {
                AddAbility();
            }
                       
            triggerEvent.Entered -= OnTriggerEventEnter;
            Destroy(triggerEvent.gameObject);
        }       
    }

    /// <summary>
    /// Добавить персонажу новое доступное действие
    /// </summary>
    private void AddAbility()
    {        
        GameManager.Instance.Controller.AddAllowedInput(newAbilities);
    }

    /// <summary>
    /// Обработать событие окончания обучения
    /// </summary>
    private void OnTutorialFinished()
    {
        GameManager.Instance.Controller.SetAllowedInput(previousAllowedInput);
        AddAbility();
    }
}
