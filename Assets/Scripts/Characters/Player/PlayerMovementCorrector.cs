﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс, служащий для корректировки движения игрока, в зависимости от ситуации.
/// Во время движения в желаемом направлении игрок может врезаться в препятствие или попытаться проявить
/// невидимое препятствие, находясь на нём. Класс PlayerController использует этот класс для того, 
/// чтобы получить корректный вектор движения или запретить движение в желаемом направлении.
/// </summary>
public class PlayerMovementCorrector : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Максимальный угол между желаемым вектором движения и вектором огибания препятствия, " +
        "при котором персонаж будет огибать препятствие")]
    private float maxMoveArountAngle = 80;

    /// <summary>
    /// Rigidbody игрока
    /// </summary>
    private Rigidbody playerRigidbody;
    
    /// <summary>
    /// Коллайдер игрока
    /// </summary>
    private CapsuleCollider playerCollider;

    /// <summary>
    /// Контроллер прозрачности
    /// </summary>
    private OpacityController opacityController = null;

    /// <summary>
    /// Событие, возникающее, когда вектор движения был скорректирован в свяи с тем, что игрок 
    /// пытается проявить невидимое препятствие, находясь на нём.
    /// </summary>
    private event Action<List<ChangingObstacle>> CorrectedByChangingObstacles;

    /// <summary>
    /// Добавить слушателя события корректировки направления движения
    /// </summary>
    /// <param name="listener"></param>
    public void RegisterCorrectedByChangingObstaclesListener(Action<List<ChangingObstacle>> listener)
    {
        CorrectedByChangingObstacles += listener;
    }

    /// <summary>
    /// Удалить слушателя события корректировки направления движения
    /// </summary>
    /// <param name="listener"></param>
    public void UnregisterCorrectedByChangingObstaclesListener(Action<List<ChangingObstacle>> listener)
    {
        CorrectedByChangingObstacles -= listener;
    }

    /// <summary>
    /// Инициализировать поля корректора
    /// </summary>
    /// <param name="playerRigidbody"></param>
    /// <param name="playerCollider"></param>
    public void Initialize(Rigidbody playerRigidbody, CapsuleCollider playerCollider)
    {
        this.playerRigidbody = playerRigidbody;
        this.playerCollider = playerCollider;
    }

    /// <summary>
    /// Получить скорректированный вектор движения
    /// </summary>
    /// <param name="movementVector"></param>
    /// <returns></returns>
    public Vector3 GetCorrectedMovementVector(Vector3 movementVector)
    {
        movementVector = GetMovementVectorCorrectedByOpaqueObstacles(movementVector);
        movementVector = GetMovementVectorCorrectedByChangingObstacles(movementVector);
        return movementVector;
    }

    private void Start()
    {
        opacityController = GameManager.Instance.Controller.OpacityController;
    }

    /// <summary>
    /// Попытаться получить вектор обхода препятствия.
    /// </summary>
    /// <param name="movementDirection">Желаемое направление движения</param>
    /// <param name="moveAroundVector">Рассчитанный вектор обхода препятствия</param>
    /// <returns>true - вектор найден, false - ну пути нет препятствий</returns>
    private bool TryGetMoveAroundVector(Vector3 movementDirection, out Vector3 moveAroundVector)
    {
        // Тестируем, есть ли препятствия на пути движения по вектору movementDirection
        if (playerRigidbody.SweepTest(movementDirection, out RaycastHit hit, 1, QueryTriggerInteraction.Ignore))
        {
            // Если есть препятствие, его нужно обойти слева или справа. Для этого получаем нормаль к поверхности
            // препятствия. Вектор обхода - это повёрнутый на 90 градусов вектор нормали.
            // Из двух вариантов огибания (слева или справа) берём вектор, который имеет меньший угол
            // с вектором желаемого направления движения.

            Vector3 normal = hit.normal;
            normal.y = 0;

            moveAroundVector = VectorFunc.GetRotatedUnitVector(normal, 90);

            if (Vector3.Angle(movementDirection, -moveAroundVector)
                < Vector3.Angle(movementDirection, moveAroundVector))
            {
                moveAroundVector = -moveAroundVector;
            }          

            return true;
        }

        moveAroundVector = Vector3.zero;
        return false;
    }

    /// <summary>
    /// Применить движение с огибанием препятствий, встреченных на пути
    /// </summary>
    /// <param name="movementDirection">Текущее направление движения</param>
    private Vector3 GetMovementVectorCorrectedByOpaqueObstacles(Vector3 movementDirection)
    {
        // Нет смысла обходить препятствия, если не двигаемся
        if (movementDirection == Vector3.zero)
        {
            return Vector3.zero;
        }

        Vector3 newDirection = movementDirection;

        // Проверем, есть ли на пути вектора желаемого движения препятствие. Если есть, найдём скорректированный вектор
        // движения, огибающий препятствие.
        if (TryGetMoveAroundVector(movementDirection, out Vector3 moveAroundVector))
        {
            // Не будем двигаться, если угол между вектором желаемого движения и вектором огибания слишком большой
            if (Vector3.Angle(movementDirection, moveAroundVector) > maxMoveArountAngle)
            {
                return Vector3.zero;
            }

            // Не будем двигаться, если при движении в новом направлении также упираемся в препятствие
            if (TryGetMoveAroundVector(moveAroundVector, out _))
            {
                return Vector3.zero;
            }

            newDirection = moveAroundVector;
        }

        return newDirection;
    }

    private Vector3 GetMovementVectorCorrectedByChangingObstacles(Vector3 movementDirection)
    {
        // Если прозрачность не находится в граничном состоянии,
        // значит все коллайдеры непрозрачные. Нас интересуют только прозрачные.
        if (opacityController.OpacityValue > OpacityController.MinOpacityValue &&
            opacityController.OpacityValue < OpacityController.MaxOpacityValue)
        {
            return movementDirection;
        }

        // Накрываем игрока сверху куполом SphereCastAll и проверяем, в каких изменяющихся препятствиях
        // он сейчас находится

        Vector3 topPoint = playerRigidbody.position;
        topPoint.y = VectorFunc.MaxRaycastDistance;

        RaycastHit[] raycastHits = Physics.SphereCastAll(topPoint, playerCollider.radius * transform.lossyScale.x,
            Vector3.down,
            VectorFunc.MaxRaycastDistance - GameManager.Instance.GroundY,
            GameManager.Instance.ChangingObstacleLayers);

        if (raycastHits.Length == 0)
        {
            return movementDirection;
        }

        // Скорректируем вектор движения в зависимости от препятствий, на которых сейчас стоит игрок,
        // чтобы предотвратить ситуацию появления препятствия в игроке
        List<ChangingObstacle> obstacleList = new List<ChangingObstacle>();
        foreach (var raycastHit in raycastHits)
        {
            ChangingObstacle obstacle = raycastHit.collider.GetComponentInParent<ChangingObstacle>();

            if (obstacle == null)
            {
                continue;
            }

            Vector3 opacityIncreaseVector = opacityController.OpacityIncreaseVector;
            if (obstacle.InvertedChange)
            {
                opacityIncreaseVector = -opacityIncreaseVector;
            }

            float movementValueInOpacityIncreaseDirection = Vector3.Dot(movementDirection, opacityIncreaseVector);
            if (movementValueInOpacityIncreaseDirection > 0)
            {
                movementDirection -= movementValueInOpacityIncreaseDirection * opacityIncreaseVector;

                // Изменяющиеся препятствия могут быть вложены друг в друга. Будем рассматривать только
                // препятствия верхнего уровня
                if (obstacle.Owner != null)
                {
                    obstacle = obstacle.Owner;
                }

                if (obstacleList.Contains(obstacle))
                {
                    continue;
                }

                obstacleList.Add(obstacle);
            }
        }

        if (obstacleList.Count == 0)
        {
            return movementDirection;
        }

        CorrectedByChangingObstacles?.Invoke(obstacleList);

        float movementDirectionLength = movementDirection.magnitude;
        if (movementDirectionLength < 0.1f)
        {
            return Vector3.zero;
        }
        else
        {
            return movementDirection / movementDirectionLength;
        }

    }
}
