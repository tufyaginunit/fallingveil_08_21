﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Флаги разрешённых действий по управлению персонажем
/// </summary>
[Flags]
public enum AllowedInput
{
    None = 0,
    Up = 1,
    Down = 2,
    Left = 4,
    Right = 8,
    Attack = 16,
    Invisibility = 32,
    All = 63
}

/// <summary>
/// Обработчик ввода от игрока.
/// Реагирует на события движения мыши и нажатия клавиш.
/// Добавляется на объект игрового персонажа.
/// Требует наличия на объекте компонентов PlayerController (для задания ему
/// движения и поворота) и WeaponController (для управления оружием)
/// </summary>
[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(WeaponController))]
public class PlayerInput : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Радиус области вокруг курсора, в которой происходит автонацеливание на врагов")]
    private float autoTargetingRadius = 2;

    [SerializeField]
    [Tooltip("Объект HP игрока")]
    private DamageableObject playerHealthObject = null;

    [SerializeField]
    [Tooltip("Разрешённые действия по управлению персонажем")]
    private AllowedInput allowedInput = 0;

    /// <summary>
    /// Текущее направление движения игрока
    /// </summary>
    private Vector3 moveDirection;

    /// <summary>
    /// Контроллер игрока
    /// </summary>
    private PlayerController playerController;

    /// <summary>
    /// Контроллер оружия игрока
    /// </summary>
    private WeaponController weaponController = null;

    /// <summary>
    /// Позиция курсора мыши на плоскость XZ, в которой находится игрок
    /// </summary>
    private Vector3 mouseGroundPosition;

    /// <summary>
    /// Текущая цель в радиусе автонацеливания вокруг курсора
    /// </summary>
    private IDamageable currentTarget = null;

    /// <summary>
    /// Флаг, сигнализирующий о том, что игрок хочет атаковать, но пока не может, 
    /// т.к. не завершится кулдаун от предыдущей атаки
    /// </summary>
    private bool waitingForCooldown = false;

    /// <summary>
    /// Радиус области вокруг курсора, в которой происходит автонацеливание на врагов
    /// </summary>
    public float AutoTargetingRadius => autoTargetingRadius;

    /// <summary>
    /// Разрешённые действия по управлению персонажем
    /// </summary>
    public AllowedInput AllowedInput => allowedInput;

    /// <summary>
    /// Включить/выключить управление персонажем
    /// </summary>
    /// <param name="enabled"></param>
    public void SetEnabled(bool enabled)
    {
        playerController.ResetSpeed();
        this.enabled = enabled;
    }

    /// <summary>
    /// Получить текущую цель игрока
    /// </summary>
    /// <returns></returns>
    public IDamageable GetTarget()
    {
        return currentTarget;
    }

    /// <summary>
    /// Установить разрешённые действия по управлению персонажем
    /// </summary>
    /// <param name="input"></param>
    public void SetAllowedInput(AllowedInput input)
    {
        allowedInput = input;
    }

    /// <summary>
    /// Добавить разрешённое действие
    /// </summary>
    /// <param name="input"></param>
    public void AddAllowedInput(AllowedInput input)
    {
        allowedInput |= input;
    }

    /// <summary>
    /// Разрешить выполнять все возможные действия управления персонажем
    /// </summary>
    /// <param name="allowed"></param>
    public void SetAllowedAllInput(bool allowed)
    {
        allowedInput = allowed ? AllowedInput.All : AllowedInput.None;
    }

    /// <summary>
    /// Проверить, разрешено ли выполнять указанное действие по управлению персонажем
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public bool IsInputAllowed(AllowedInput input)
    {
        return (allowedInput & input) == input;
    }

    private void Awake()
    {
        playerController = GetComponent<PlayerController>();
        weaponController = GetComponent<WeaponController>();
    }

    private void Update()
    {
        if (GameManager.Instance.Controller.GamePaused)
        {
            return;
        }

        // Если игрок убит, то прерываем атаку и больше не реагируем ни на какие нажатия клавиш
        if (!playerHealthObject.IsAlive())
        {
            if (weaponController.IsAttacking() && !weaponController.IsInterruptionRequested)
            {
                weaponController.InterruptAttack();
            }
            return;
        }

        float horizontalInput = GetHorizontalInput();
        float verticalInput = GetVerticalInput();
        moveDirection = new Vector3(-horizontalInput, 0, -verticalInput).normalized;

        mouseGroundPosition = GameManager.Instance.GetGroundPositionFromScreen(Input.mousePosition);

        // Если уже есть цель, по которой ведём огонь, не переключаемся на другую.
        // В противном случае будем искать цель в окрестности курсора.
        if (weaponController.IsHitPerforming() && weaponController.GetTarget() != null)
        {
            currentTarget = weaponController.GetTarget();
        }
        else
        {
            currentTarget = FindNearestTargetInVicinity(mouseGroundPosition, autoTargetingRadius);
        }

        // Будем атаковать, если кнопка атаки нажата в текущем кадре.
        // Также возможна ситуация, когда пользователь зажал кнопку атаки во время кулдауна предыдущей атаки.
        // В этом случае делаем попытки атаковать каждый кадр, пока зажата кнопка. 

        bool attackPressedInCurrentFrame = Input.GetButtonDown(ControlConstants.AttackButton);
        bool attackHolding = Input.GetButton(ControlConstants.AttackButton);

        if (attackHolding)
        {
            if (!waitingForCooldown)
            {
                waitingForCooldown = attackPressedInCurrentFrame && weaponController.IsCoolDowning;
            }
        }
        else
        {
            waitingForCooldown = false;
        }

        if ((attackPressedInCurrentFrame || waitingForCooldown)
            && IsInputAllowed(AllowedInput.Attack))
        {
            weaponController.Attack();
            playerHealthObject.SetEnabledInvisibility(false);
        }
        else if (!attackHolding && weaponController.IsAttacking() &&
            weaponController.GetTarget() == null && !weaponController.IsInterruptionRequested)
        {
            weaponController.InterruptAttack();
        }

        if (Input.GetButtonDown(ControlConstants.InvisibilityButton)
            && IsInputAllowed(AllowedInput.Invisibility))
        {
            weaponController.InterruptAttack();
            playerHealthObject.SetEnabledInvisibility(!playerHealthObject.IsInvisible());
        }
    }

    /// <summary>
    /// Получить значение горизонтального движения персонажа
    /// </summary>
    /// <returns></returns>
    private float GetHorizontalInput()
    {
        float horizontalInput = Input.GetAxis(ControlConstants.HorizontalMove);
        if (horizontalInput > 0)
        {
            if (!IsInputAllowed(AllowedInput.Right))
            {
                horizontalInput = 0;
            }
        }
        else if (horizontalInput < 0)
        {
            if (!IsInputAllowed(AllowedInput.Left))
            {
                horizontalInput = 0;
            }
        }
        return horizontalInput;
    }

    /// <summary>
    /// Получить значение вертикального движения персонажа
    /// </summary>
    /// <returns></returns>   
    private float GetVerticalInput()
    {
        float verticalInput = Input.GetAxis(ControlConstants.VerticalMove);
        if (verticalInput > 0)
        {
            if (!IsInputAllowed(AllowedInput.Up))
            {
                verticalInput = 0;
            }
        }
        else if (verticalInput < 0)
        {
            if (!IsInputAllowed(AllowedInput.Down))
            {
                verticalInput = 0;
            }
        }
        return verticalInput;
    }

    private void FixedUpdate()
    {
        if (!playerHealthObject.IsAlive())
        {
            return;
        }

        playerController.Move(moveDirection);

        // При атаке поворачиваемся туда, где находится цель (если уже ведём огонь по цели, то
        // то поворачиваемся к ней; если нет, но пытаемся атаковать, поворачиваемся к желаемой цели атаки).
        // Если цели нет, смотрим на курсор.

        IDamageable target = weaponController.GetTarget();
        
        if (target == null && weaponController.IsAttacking() && !weaponController.IsHitPerforming()
            && !weaponController.IsCoolDowning)
        {
            target = currentTarget;
        }
        
        Vector3 lookAtPoint = target != null ? target.Transform.position : mouseGroundPosition;
        playerController.RotateSmooth(lookAtPoint);
    }

    /// <summary>
    /// Найти врага, ближайшего к указанной позиции
    /// </summary>
    /// <param name="point">Центр окрестности, в которой осуществляется поиск</param>
    /// <param name="radius">Радиус окрестности</param>
    /// <returns></returns>
    private IDamageable FindNearestTargetInVicinity(Vector3 point, float radius)
    {
        Vector3 topPoint = point;
        topPoint.y = VectorFunc.MaxRaycastDistance;

        RaycastHit[] hitInfo = Physics.SphereCastAll(topPoint, radius, Vector3.down,
            VectorFunc.MaxRaycastDistance, GameManager.Instance.EnemyLayers, QueryTriggerInteraction.Ignore);

        if (hitInfo.Length == 0)
        {
            return null;
        }

        Transform minDistTransform = hitInfo[0].transform;
        float minSqrDistToTarget = Vector3.SqrMagnitude(minDistTransform.position - point);

        // Если целей в окрестности точки несколько, найдём ту, которая ближе всего к центру окрестности
        for (int i = 1; i < hitInfo.Length; i++)
        {
            float currentSqrDist = Vector3.SqrMagnitude(hitInfo[i].transform.position - point);
            if (currentSqrDist < minSqrDistToTarget)
            {
                minSqrDistToTarget = currentSqrDist;
                minDistTransform = hitInfo[i].transform;
            }
        }

        return minDistTransform.GetComponent<IDamageable>();
    }

}
