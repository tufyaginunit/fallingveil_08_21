﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Контроллер визуализации корректора движения игрока
/// </summary>
public class PlayerMovementCorrectorDrawer : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Контроллер включен/выключен")]
    private bool isEnabled = true;

    [SerializeField]
    [Tooltip("Корректор движения")]
    private PlayerMovementCorrector movementCorrector = null;

    [SerializeField]
    [Tooltip("Материал, используемый для подсветки препятствий, в направлении появления которых нельзя идти")]
    private Material highlightMaterial;

    [SerializeField]
    [Tooltip("Продолжительность подсветки")]
    private float highlightDuration = 1;

    /// <summary>
    /// Токен корутины подсветки
    /// </summary>
    private CoroutineTask coroutineTask = null;

    private void Awake()
    {
        coroutineTask = new CoroutineTask();
        movementCorrector.RegisterCorrectedByChangingObstaclesListener(OnMovementCorrectedByChangingObstacles);
    }

    private void OnDestroy()
    {
        if (movementCorrector != null)
        {
            movementCorrector.UnregisterCorrectedByChangingObstaclesListener(OnMovementCorrectedByChangingObstacles);
        }
    }

    /// <summary>
    /// Обработчик события корректировки движения по прозрачным препятствиям 
    /// </summary>
    /// <param name="obstacles"></param>
    private void OnMovementCorrectedByChangingObstacles(List<ChangingObstacle> obstacles)
    {
        if (!isEnabled || obstacles == null || obstacles.Count == 0)
        {
            return;
        }

        List<Renderer> rendererList = new List<Renderer>();
        foreach (var item in obstacles)
        {
            rendererList.AddRange(item.GetComponentsInChildren<Renderer>());
        }
       
        if (!coroutineTask.IsPerforming)
        {
            StartCoroutine(Rendering(rendererList, coroutineTask));
        }
    }

    /// <summary>
    /// Корутина, на время подсвечивающая прозрачные препятствия, в направлении которых нельзя идти
    /// </summary>
    /// <param name="rendererList">Список компонентов Renderer препятствий</param>
    /// <param name="task">Задача корутины</param>
    /// <returns></returns>
    private IEnumerator Rendering(List<Renderer> rendererList, CoroutineTask task)
    {
        task.Start();

        // Будем подменять все материалы указанных рендереров на материал подсветки,
        // после чего через некоторое время восстанавливать исходные материалы

        List<Material[]> initialSharedMaterialsList = new List<Material[]>();
        List<bool> initialRenderersEnabledStateList = new List<bool>();
        for (int i = 0; i < rendererList.Count; i++)
        {
            initialRenderersEnabledStateList.Add(rendererList[i].enabled);
            rendererList[i].enabled = true;
            initialSharedMaterialsList.Add(rendererList[i].sharedMaterials);
            Material[] highlightedSharedMaterialList = new Material[rendererList[i].sharedMaterials.Length];
            for (int j = 0; j < rendererList[i].sharedMaterials.Length; j++)
            {
                highlightedSharedMaterialList[j] = highlightMaterial;
            }
            rendererList[i].sharedMaterials = highlightedSharedMaterialList;
        }

        yield return new WaitForSeconds(highlightDuration);

        for (int i = 0; i < rendererList.Count; i++)
        {
            rendererList[i].sharedMaterials = initialSharedMaterialsList[i];
            rendererList[i].enabled = initialRenderersEnabledStateList[i];
        }

        task.Stop();
    }
}
