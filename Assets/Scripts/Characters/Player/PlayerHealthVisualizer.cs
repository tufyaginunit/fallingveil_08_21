﻿using UnityEngine;

/// <summary>
/// Визуализатор здоровья игрока, отображающий измнение HP изменением цвета рубашки и луча
/// </summary>
public class PlayerHealthVisualizer: MonoBehaviour
{
    [SerializeField, Tooltip("Объект, изменение здоровья которого нужно отслеживать")]
    protected DamageableObject healthObject = null;

    [SerializeField, Tooltip("Рендерер материала, у которого изменяем свечение")]
    private Renderer glowingMaterialRenderer = null;

    [SerializeField, Tooltip("Рендерер луча")]
    private AttackBeamRenderer attackBeamRenderer;

    [SerializeField, Tooltip("Значение HP, при котором начинается плавный переход к цвету опасности")]
    private int startDangerHP = 50;

    [SerializeField, Tooltip("Значение HP, при котором достигается максимальный цвет опасности")]
    private int maxDangerHP = 10;

    [SerializeField, Tooltip("Максимальный цвет свечения при нормальном уровне HP")]
    private Color maxEmissionColor;

    [SerializeField, Tooltip("Цвет свечения при максимальной опасности")]
    private Color maxDangerEmissionColor;

    [SerializeField, Tooltip("Базовый цвет, в который плавно переходит текстура при увеличении опасности")]
    private Color dangerBaseColor;

    [SerializeField, Tooltip("Минимальное значение свечения")]
    private float minEmissionValue;

    [SerializeField, Tooltip("Манимальное значение свечения")]
    private float maxEmissionValue;

    /// <summary>
    /// Базовый цвет текстуры
    /// </summary>
    private Color initialBaseColor;

    /// <summary>
    /// Материал рендерера
    /// </summary>
    private Material material;

    private void Awake()
    {
        healthObject.RegisterHealthChangedListener(OnHealthChanged);

        material = glowingMaterialRenderer.sharedMaterial;
        initialBaseColor = glowingMaterialRenderer.sharedMaterial.GetColor("_BaseColor");
    }

    private void OnDestroy()
    {
        if (healthObject != null)
        {
            healthObject.UnregisterHealthChangedListener(OnHealthChanged);
        }
    }

    /// <summary>
    /// Обработчик события изменения HP
    /// </summary>
    private void OnHealthChanged()
    {
        float emissionFactor;
        Color newEmissionColor;
        Color newBaseColor;

        if (healthObject.Health > startDangerHP)
        {
            emissionFactor = 1 - (healthObject.Health - startDangerHP) / (float)(healthObject.MaxHealth - startDangerHP);
            newEmissionColor = maxEmissionColor;
            newBaseColor = initialBaseColor;
        }
        else
        {
            emissionFactor = 1;

            float colorBlendFactor = (healthObject.Health - maxDangerHP) / (float)(startDangerHP - maxDangerHP);
            newEmissionColor = Color.Lerp(maxDangerEmissionColor, maxEmissionColor, colorBlendFactor);
            newBaseColor = Color.Lerp(dangerBaseColor, initialBaseColor, colorBlendFactor);
        }
        
        float intensity = Mathf.Lerp(minEmissionValue, maxEmissionValue, emissionFactor);

        Color materialEmissionColor;
        if (intensity > minEmissionValue)
        {
            materialEmissionColor = newEmissionColor * Mathf.Pow(2, intensity);
        }
        else
        {
            materialEmissionColor = Color.black;
        }

        material.SetColor("_BaseColor", newBaseColor);
        material.SetColor("_EmissionColor", materialEmissionColor);

        attackBeamRenderer.SetColor(materialEmissionColor);
    }
}
