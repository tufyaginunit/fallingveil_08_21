﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Эффект смерти, основанный на изменении материалов.
/// Рекомендуется добавлять на отдельный дочерний объект врага.
/// </summary>
public class MaterialChangeDeathEffect : DeathEffect
{
    [SerializeField, Tooltip("Контейнер моделей, материалы которых будут изменяться")]
    private Transform model = null;

    [SerializeField, Tooltip("Название свойства материала")]
    private string propertyName = "";

    [SerializeField, Tooltip("Начальное значение свойства материала")]
    private float startValue = 0;

    [SerializeField, Tooltip("Конечное значение свойства материала")]
    private float endValue = 1;

    /// <summary>
    /// Объект, изменяющий свои материалы
    /// </summary>
    private MaterialChangingObject materialChangingModel = null;

    /// <summary>
    /// Обработать событие смерти
    /// </summary>
    protected override void ProcessDeath()
    {
        StartCoroutine(PerformDeath(deathDuration));
    }

    protected override void Awake()
    {
        base.Awake();
        ChangingFloatPropStrategy changingStrategy = new ChangingFloatPropStrategy(propertyName, startValue, endValue);
        materialChangingModel = new MaterialChangingObject(model, changingStrategy);
    }

    /// <summary>
    /// Корутина, выполняющая плавное растворение врага
    /// </summary>
    /// <returns></returns>
    private IEnumerator PerformDeath(float deathDuration)
    {
        float timePassed = 0;
        while (timePassed < deathDuration)
        {
            yield return null;
            timePassed += Time.deltaTime;
            materialChangingModel.SetNormalizedValue(timePassed / deathDuration);           
        }
    }

    /// <summary>
    /// Задать состояние объекта. 
    /// Используется для восстановления состояния объекта при возрождении на контрольной точке.
    /// Потенциально метод может быть использован для загрузки из файла,
    /// но сейчас мы используем его только для того, чтобы при начале игры с контрольной точки
    /// восстановить первоначальное состояние объекта
    /// </summary>
    /// <param name="state"></param>
    public override void SetState(object state)
    {
        base.SetState(state);
        materialChangingModel.SetNormalizedValue(startValue);
    }

}
