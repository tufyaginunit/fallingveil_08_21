﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Визуализатор текущего уровня здоровья врага
/// </summary>
public class EnemyHealthVisualizer : MonoBehaviour
{
    [SerializeField, Tooltip("Повреждаемый объект, здоровье которого нужно визуализировать")]
    private DamageableObject healthObject = null;

    [SerializeField, Tooltip("Модель персонажа, материалы которого будут изменяться")]
    private Transform model = null;

    [SerializeField, Tooltip("Стратегия изменения материалов модели")]
    private OpacityChangingStrategy changingStrategy;

    /// <summary>
    /// Объект, изменяющий свои материалы
    /// </summary>
    private MaterialChangingObject materialChangingObject = null;

    private void Awake()
    {
        materialChangingObject = new MaterialChangingObject(model, changingStrategy);

        healthObject.RegisterHealthChangedListener(OnHealthChanged);
    }

    private void OnDestroy()
    {
        if (healthObject != null)
        {
            healthObject.UnregisterHealthChangedListener(OnHealthChanged);
        }
    }

    /// <summary>
    /// Обработчик события изменения здоровья врага
    /// </summary>
    private void OnHealthChanged()
    {
        materialChangingObject.SetNormalizedValue(healthObject.Health / (float)healthObject.MaxHealth);
    }
}
