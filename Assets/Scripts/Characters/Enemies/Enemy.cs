﻿using System;
using UnityEngine;

/// <summary>
/// Повреждаемый объект, представляющий врага.
/// Добавляется непосредственно на объект врага, благодаря чему он приобретает HP,
/// и становится возможным наносить ему повреждения
/// </summary>
public class Enemy : DamageableObject
{   
    [SerializeField, Tooltip("AIStateMachine, из которой берется текущее состояние врага")]
    private FsmCore stateMachine;

    [SerializeField, Tooltip("Объект поля зрения врага")]
    private FieldOfView fov;

    /// <summary>
    /// Находится ли данный враг под прикрытием (попадает в поле зрения другого врага)
    /// </summary>
    public bool UnderCover => fov.UnderCover;

    public FovDetectingState DetectingState => fov.DetectingState;

    /// <summary>
    /// Текущее состояние врага
    /// </summary>
    public AIState CurrentState { get; private set; } = AIState.Idle;

    /// <summary>
    /// Предыдущее состояние врага
    /// </summary>
    public AIState PreviousState { get; private set; } = AIState.Idle;

    public FieldOfView Fov => fov;

    /// <summary>
    /// Событие изменения состояния врага
    /// </summary>
    private event Action StateChanged = null;

    /// <summary>
    /// Добавить слушателя события изменения состояния врага
    /// </summary>
    /// <param name="listener"></param>
    public void RegisterStateChangedListener(Action listener)
    {
        StateChanged += listener;
    }

    /// <summary>
    /// Удалить слушателя события изменения состояния врага
    /// </summary>
    /// <param name="listener"></param>
    public void UnregisterStateChangedListener(Action listener)
    {
        StateChanged -= listener;
    }

    /// <summary>
    /// Находится ли указанный союзник врага в его поле зрения
    /// </summary>
    /// <param name="ally"></param>
    /// <returns></returns>
    public bool IsAllyInFov(Enemy ally)
    {
        return fov.IsAllyInFov(ally);
    }

    /// <summary>
    /// Нуждается ли враг в помощи союзника
    /// </summary>
    /// <returns></returns>
    public bool IsNeedHelp()
    {
        return CurrentState == AIState.Attacking || CurrentState == AIState.Retreating ||
            (CurrentState == AIState.Chasing && DetectingState == FovDetectingState.Detected);
    }
    
    /// <summary>
    /// Метод, вызываемый при воскрешении игрока на контрольной точке
    /// </summary>
    /// <param name="state"></param>
    public override void SetState(object state)
    {
        base.SetState(state);
        CurrentState = AIState.Idle;
        PreviousState = CurrentState;
    }

    private void OnEnable()
    {
        if (stateMachine != null)
        {
            stateMachine.RegisterStateChangedListener(OnStateMachineStateChanged);
        }
    }

    private void OnDisable()
    {
        if (stateMachine != null)
        {
            stateMachine.UnregisterStateChangedListener(OnStateMachineStateChanged);
        }
    }

    /// <summary>
    /// Обработчик события изменения состояния машины состояний врага
    /// </summary>
    private void OnStateMachineStateChanged()
    {
        PreviousState = CurrentState;

        FsmState fsmState = GetCurrentFsmState();
        if (fsmState == null)
        {
            CurrentState = AIState.Idle;
            return;
        }

        CurrentState = fsmState.AiState;
        if (CurrentState == AIState.NotAssigned)
        {
            //Debug.LogWarning($"Transition to a not assigned state: {fsmState}");
        }

        StateChanged?.Invoke();
    }

    /// <summary>
    /// Получить текущее состояние машины состояний
    /// </summary>
    /// <returns></returns>
    private FsmState GetCurrentFsmState()
    {
        FsmCore.StateEntity currentState = stateMachine.GetCurrentState();
        if (currentState == null)
        {
            return null;
        }

        return currentState.State;
    }
}
