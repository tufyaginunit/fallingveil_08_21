﻿using UnityEngine;

/// <summary>
/// Элемент поля обнаружения, отвечающий за зону чувствительности
/// </summary>
public class SensitivityFovElement : FieldOfViewElement
{
    [SerializeField, Tooltip("Объект движения врага")]
    private AIMovingObject enemyMovingObject = null;

    [SerializeField, Tooltip("Максимальная длина маршрута до игрока, при которой враг может его обнаружить")]
    private float maxPathLengthToDetect = 50;

    [SerializeField, Tooltip("Минимальная скорость цели, чтобы её можно было обнаружить")]
    private float detectingTargetSpeed = 0.5f;

    /// <summary>
    /// Квадрат скорости цели (для оптимизации)
    /// </summary>
    private float sqrDetectingTargetSpeed = 0;

    /// <summary>
    /// Движущийся объект цели (игрока)
    /// </summary>
    private MovingObject targetMovingObject = null;

    /// <summary>
    /// Оружие цели (игрока)
    /// </summary>
    private WeaponController targetWeaponController = null;

    public AIMovingObject MovingObject => enemyMovingObject;

    public float MaxPathLengthToDetect => maxPathLengthToDetect;

    protected override void Start()
    {        
        targetMovingObject = GameManager.Instance.PlayerMovingObject;
        targetWeaponController = GameManager.Instance.PlayerWeapon;
        sqrDetectingTargetSpeed = detectingTargetSpeed * detectingTargetSpeed;
        base.Start();
    }

    /// <summary>
    /// Может ли враг дойти до указанной точки
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public bool IsAchievablePointNearObstacle(Vector3 point)
    {
        return MovingObject.CalculatePathToPoint(point, out Vector3[] path)
            && VectorFunc.GetPathLength(path) <= MaxPathLengthToDetect;
    }

    /// <summary>
    /// Может ли враг зайти за препятствие
    /// </summary>
    /// <param name="hitInfo"></param>
    /// <param name="fovVectorDirection"></param>
    /// <param name="checkDistance"></param>
    /// <returns></returns>
    public bool CanMoveBehindObstacle(RaycastHit hitInfo, Vector3 fovVectorDirection, float checkDistance = 1)
    {
        Vector3 obstacleOutsidePoint = hitInfo.point + checkDistance * hitInfo.normal;

        Vector3 checkPoint = checkDistance * VectorFunc.GetRotatedUnitVector(hitInfo.normal, 90);
        if (Vector3.Dot(checkPoint, fovVectorDirection) < 0)
        {
            checkPoint = -checkPoint;
        }
        return IsAchievablePointNearObstacle(obstacleOutsidePoint) ||
            IsAchievablePointNearObstacle(obstacleOutsidePoint + checkDistance * checkPoint);
    }

    /// <summary>
    /// Проверить, обнаруживается ли цель в данный момент зоной чувствительности
    /// </summary>
    /// <returns></returns>
    public override bool IsTargetDetectingByFov()
    {
        if (!IsPointDetectingByFov(target.Transform.position))
        {
            return false;
        }

        if (targetMovingObject.CurrentVelocity.sqrMagnitude < sqrDetectingTargetSpeed
            && !targetWeaponController.IsHitPerforming())
        {
            return false;
        }

        Vector3 targetPosition = target.Transform.position;
        Vector3 checkDirection = pointOfSight.position - targetPosition;
        RaycastHit hitInfo = VisionFovElement.GetFovRaycastHit(targetPosition, checkDirection, Distance);

        return hitInfo.collider == null || CanMoveBehindObstacle(hitInfo, checkDirection);
    }
}
