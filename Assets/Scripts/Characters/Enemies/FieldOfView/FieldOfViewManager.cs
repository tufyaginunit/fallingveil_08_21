﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Синглтон-менеджер объектов, реализующих поле зрения.
/// Хранит общие настройки для полей зрения врагов
/// </summary>
public class FieldOfViewManager : Singleton<FieldOfViewManager>
{
    [SerializeField, Tooltip("Шаг между векторами поля зрения в градусах. " +
        "Большее значение уменьшает точность нахождения границ, но увеличивает производительность")]
    [Range(1, 20)]
    private float linesStepInDegrees = 10;

    [SerializeField, Tooltip("Материал поля зрения врага в спокойном состоянии")]
    private Renderer fovRenderer;

    [SerializeField, Tooltip("Материал маски поля зрения врага в спокойном состоянии")]
    private Material fovMaskMaterial;

    [SerializeField, Tooltip("Материал маски поля зрения врага, который обнаружил игрока")]
    private Material fovMaskDetectedMaterial;

    [SerializeField, Tooltip("Материал маски поля зрения врага, когда игрок скоро будет замечен")]
    private Material fovMaskDangerMaterial;

    [SerializeField, Tooltip("Плоскость области видимости ")]
    private Transform fovPlane = null;

    /// <summary>
    /// Шаг между векторами поля зрения в градусах (статическое поле, которое можно получить прямо из класса)
    /// </summary>
    public float LinesStepInDegrees => linesStepInDegrees;

    public Material FovMaterial => fovRenderer.material;

    /// <summary>
    /// Материал поля зрения врага в спокойном состоянии
    /// </summary>
    public Material FovMaskMaterial => fovMaskMaterial;

    /// <summary>
    /// Материал поля зрения врага, когда игрок обнаружен
    /// </summary>
    public Material FovMaskDetectedMaterial => fovMaskDetectedMaterial;

    /// <summary>
    /// Материал поля зрения врага в состоянии, когда игрок в поле зрения, но ещё не обнаружен
    /// </summary>
    public Material FovMaskDangerMaterial => fovMaskDangerMaterial;

    /// <summary>
    /// Отображается ли поле зрения врагов
    /// </summary>
    public bool IsVisible { get; private set; } = true;

    /// <summary>
    /// Включить/выключить отображение поля зрения врагов
    /// </summary>
    /// <param name="visible"></param>
    public void SetEnemiesFovVisible(bool visible)
    {
        IsVisible = visible;
        fovPlane.gameObject.SetActive(visible);
    }

    private void Awake()
    {        
        fovPlane.gameObject.SetActive(true);
    }

}
