﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Элемент поля обнаружения врагов.
/// Базовый класс для разных типов полей обнаружения (зона зрения, зона чувствительности)
/// </summary>
public abstract class FieldOfViewElement : MonoBehaviour
{
    /// <summary>
    /// Задержка проверки в секундах (чтобы не каждый кадр проверять) 
    /// </summary>
    public const float CheckObjectsInFovDelay = 0.1f;

    [SerializeField]
    [Tooltip("Враг, к которому относится данное поле обнаружения")]
    protected Enemy enemyObject = null;

    [SerializeField]
    [Tooltip("Точка центра поля обнаружения")]
    protected Transform pointOfSight = null;

    [SerializeField]
    [Tooltip("Угол поля зрения")]
    protected float angle = 0;

    [SerializeField]
    [Tooltip("Радиус поля обнаружения")]
    protected float distance = 0;

    [SerializeField]
    [Tooltip("Задержка обнаружения цели в спокойном состоянии")]
    protected float idleDetectionDelay = 1.5f;

    [SerializeField]
    [Tooltip("Задержка обнаружения цели в состоянии тревоги " +
        "(когда враг только что потерял игрока из виду)")]
    protected float alertDetectionDelay = 0;

    [SerializeField]
    [Tooltip("Задержка обнаружения нападения на союзника")]
    protected float attackedAllyDetectionDelay = 0;

    /// <summary>
    /// Объект игрока, которого враг пытается обнаружить
    /// </summary>
    protected IDamageable target = null;

    /// <summary>
    /// Текущая задержка обнаружения цели
    /// </summary>
    protected float currentDetectionDelay = 0;

    /// <summary>
    /// Квадрат значения радиуса поля обнаружения (для оптимизации) 
    /// </summary>
    protected float sqrDistance = 0;

    /// <summary>
    /// Половина значения угла поля обнаружения (для оптимизации) 
    /// </summary>
    protected float halfAngle = 0;

    /// <summary>
    /// Текущее состояние обнаружения цели
    /// </summary>
    protected FovDetectingState targetDetectingState = FovDetectingState.NotDetected;

    /// <summary>
    /// Находится ли цель в данный момент в поле обнаружения
    /// </summary>
    protected bool targetInFov = false;

    /// <summary>
    /// Находится ли в данный момент враг под прикрытием союзника (в поле зрения другого врага)
    /// </summary>
    protected bool underCover = false;

    /// <summary>
    /// Список союзников, находяющихся в данный момент в поле обнаружения
    /// </summary>
    protected readonly List<Enemy> alliesInFov = new List<Enemy>();

    /// <summary>
    /// Радиус поля обнаружения
    /// </summary>
    public float Distance => distance;

    /// <summary>
    /// Угол поля обнаружения
    /// </summary>
    public float Angle => angle;

    /// <summary>
    /// Точка центра поля обнаружения
    /// </summary>
    public Transform PointOfSight => pointOfSight;

    /// <summary>
    /// Текущее состояние обнаружения цели
    /// </summary>
    public FovDetectingState DetectingState => targetDetectingState;


    /// <summary>
    /// Находится ли в данный момент враг под прикрытием союзника (в поле зрения другого врага)
    /// </summary>
    public bool UnderCover => underCover;

    /// <summary>
    /// Находится ли цель в данный момент в поле обнаружения
    /// </summary>
    public bool TargetInFov => targetInFov;

    /// <summary>
    /// Проверить, находится ли точка в радиусе поля обнаружения
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public bool IsPointInFovRadius(Vector3 point)
    {
        float sqrDistToPoint = VectorFunc.GetSqrDistanceXZ(PointOfSight.position, point);
        return sqrDistToPoint <= sqrDistance;
    }

    /// <summary>
    /// Установить задержку обнаружения в спокойном состоянии
    /// </summary>
    public void SetIdleDetectionDelay()
    {
        currentDetectionDelay = idleDetectionDelay;
    }

    /// <summary>
    /// Установить задержку обнаружения в состоянии тревоги
    /// </summary>
    public void SetAlertDetectionDelay()
    {
        currentDetectionDelay = alertDetectionDelay;
    }

    /// <summary>
    /// Повернуть поле зрения на указанный угол
    /// </summary>
    /// <param name="rotation"></param>
    public void Rotate(float rotation)
    {
        pointOfSight.transform.Rotate(new Vector3(0, rotation, 0));
    }

    public bool IsEnabled()
    {
        return enabled;
    }

    /// <summary>
    /// Включить/выключить отображение поля обнаружения
    /// </summary>
    /// <param name="enabled"></param>
    public void SetEnabled(bool enabled)
    {
        this.enabled = enabled;
    }

    /// <summary>
    /// Находится ли указанный союзник в поле обнаружения
    /// </summary>
    /// <param name="ally"></param>
    /// <returns></returns>
    public bool IsAllyInFov(Enemy ally)
    {
        return alliesInFov.Contains(ally);
    }

    /// <summary>
    /// Проверить, находится ли точка в зоне обнаружения
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public virtual bool IsPointDetectingByFov(Vector3 point)
    {
        if (angle <= 0 || sqrDistance == 0)
        {
            return false;
        }

        Vector3 vectorToTarget = point - pointOfSight.position;
        vectorToTarget.y = 0;
        float sqrDistanceToTarget = Vector3.SqrMagnitude(vectorToTarget);

        // Проверим, не находится ли цель за пределами дальности видимости
        if (sqrDistanceToTarget > sqrDistance)
        {
            return false;
        }

        // Проверим, попадает ли цель в воронку поля зрения
        if (angle < 360 && Vector3.Angle(pointOfSight.forward, vectorToTarget) > halfAngle)
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// Проверить, обнаруживается ли цель в данный момент полем обнаружения
    /// </summary>
    /// <returns></returns>
    public abstract bool IsTargetDetectingByFov();

    /// <summary>
    /// Установить состояние объекта поля обнаружения по умолчанию
    /// </summary>    
    public void SetDefaultState()
    {
        SetTargetDetectingState(FovDetectingState.NotDetected);
        StopAllCoroutines();
        Start();
    }

    protected virtual void Awake()
    {
        sqrDistance = distance * distance;
        halfAngle = angle / 2;
        pointOfSight.forward = new Vector3(pointOfSight.forward.x, 0, pointOfSight.forward.z).normalized;
    }

    protected virtual void Start()
    {
        target = GameManager.Instance.Player;
        targetInFov = false;
        underCover = false;
        alliesInFov.Clear();

        SetEnabled(true);
        StartSearching();
    }

    protected virtual void OnDestroy()
    {
        SetEnabled(false);
        StopAllCoroutines();
    }

    /// <summary>
    /// Корутина, выполняющая обновление объектов в поле обнаружения
    /// </summary>
    /// <returns></returns>
    private IEnumerator RefreshingObjectsInFov()
    {
        while (true)
        {
            yield return new WaitForSeconds(CheckObjectsInFovDelay);
            RefreshObjectsInFov();
            yield return new WaitForFixedUpdate();
            RefreshUnderCover();
        }
    }

    /// <summary>
    /// Обновить флаг, сигнализирующий о том, находится ли враг под прикрытием
    /// </summary>
    private void RefreshUnderCover()
    {
        underCover = false;
        if (!enemyObject.IsAlive() || enemyObject.IsNeedHelp() ||
            DetectingState == FovDetectingState.Detected)
        {
            return;
        }

        foreach (var ally in EnemyManager.Instance.Enemies)
        {
            if (ally.IsAlive() && !ally.IsNeedHelp() &&
                ally.DetectingState != FovDetectingState.Detected && ally.IsAllyInFov(enemyObject))
            {
                underCover = true;
                break;
            }
        }
    }

    /// <summary>
    /// Обновить список объектов, попавших в поле обнаружения 
    /// </summary>
    private void RefreshObjectsInFov()
    {
        targetInFov = false;
        alliesInFov.Clear();
        if (!enemyObject.IsAlive())
        {
            return;
        }

        if (IsTargetDetectingByFov())
        {
            targetInFov = true;
        }

        foreach (var ally in EnemyManager.Instance.Enemies)
        {
            if (ally != enemyObject && ally.IsAlive() &&
                IsPointDetectingByFov(ally.Transform.position))
            {
                alliesInFov.Add(ally);
            }
        }
    }

    /// <summary>
    /// Корутина, выполняющая постоянный поиск цели.
    /// Данный поиск имеет больший приоритет, чем поиск атакованного союзника, 
    /// т.е. из состоянии AllyIsUnderAttack мы можем перейти в состояние TargetDetectingState.Detected,
    /// но не наоборот.
    /// </summary>
    /// <returns></returns>
    private IEnumerator TargetSearching()
    {
        while (true)
        {
            if (targetInFov)
            {
                if (targetDetectingState != FovDetectingState.Detected)
                {
                    yield return new WaitForSeconds(currentDetectionDelay);
                    if (targetInFov)
                    {
                        SetTargetDetectingState(FovDetectingState.Detected);
                    }
                }
            }
            else if (targetDetectingState == FovDetectingState.Detected)
            {
                SetTargetDetectingState(FovDetectingState.NotDetected);
            }

            yield return new WaitForFixedUpdate();
        }
    }

    /// <summary>
    /// Корутина, выполняющая поиск союзника, которому нужна помощь
    /// </summary>
    /// <returns></returns>
    private IEnumerator AllyToHelpSearching()
    {
        while (true)
        {
            // Если цель уже обнаружена, нет смысла выполнять поиск атакованного союзника
            if (targetDetectingState == FovDetectingState.Detected)
            {
                yield return null;
                continue;
            }

            IDamageable ally = FindAllyToHelp();
            if (ally != null)
            {
                if (targetDetectingState == FovDetectingState.NotDetected)
                {
                    yield return new WaitForSeconds(attackedAllyDetectionDelay);
                    ally = FindAllyToHelp();
                    if (ally != null && targetDetectingState != FovDetectingState.Detected)
                    {
                        SetTargetDetectingState(FovDetectingState.AllyIsUnderAttack);
                    }
                }
            }
            else if (targetDetectingState == FovDetectingState.AllyIsUnderAttack)
            {
                SetTargetDetectingState(FovDetectingState.NotDetected);
            }

            yield return new WaitForFixedUpdate();
        }
    }

    /// <summary>
    /// Начать поиск в поле обнаружения
    /// </summary>
    private void StartSearching()
    {
        StartCoroutine(RefreshingObjectsInFov());
        StartCoroutine(TargetSearching());
        StartCoroutine(AllyToHelpSearching());
    }

    /// <summary>
    /// Установить состояние обнаружения цели
    /// </summary>
    /// <param name="state"></param>
    private void SetTargetDetectingState(FovDetectingState state)
    {
        targetDetectingState = state;
    }

    /// <summary>
    /// Найти союзника, которому нужна помощь.
    /// Метод возвращает объект IDamageable, который находится под атакой игрока
    /// или который сам атакует игрока.
    /// </summary>
    /// <returns></returns>
    private IDamageable FindAllyToHelp()
    {
        IDamageable allyToHelp = null;
        foreach (var ally in alliesInFov)
        {
            if (ally.IsNeedHelp())
            {
                allyToHelp = ally;
                break;
            }
        }
        return allyToHelp;
    }
}
