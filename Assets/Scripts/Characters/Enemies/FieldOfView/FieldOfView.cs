﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Поле обнаружения (зрение, чувствильность) игровых персонажей.
/// Служит для обнаружения врагами игрока.
/// Добавляется на объект врага.
/// </summary>
public class FieldOfView : MonoBehaviour, ISaveable
{
    [SerializeField, Tooltip("Зона зрения")]
    private VisionFovElement visionFov = null;

    [SerializeField, Tooltip("Зона чувствительности")]
    private SensitivityFovElement sensitivityFov = null;

    /// <summary>
    /// Активные элементы вижена
    /// </summary>
    private readonly List<FieldOfViewElement> activeFovElements = new List<FieldOfViewElement>();

    /// <summary>
    /// Зона зрения
    /// </summary>
    public VisionFovElement VisionFov => visionFov;

    /// <summary>
    /// Зона чувствительности
    /// </summary>
    public SensitivityFovElement SensitivityFov => sensitivityFov;

    /// <summary>
    /// Текущее состояние обнаружения цели
    /// </summary>
    public FovDetectingState DetectingState => GetDetectingState();

    /// <summary>
    /// Находится ли в данный момент враг под прикрытием союзника (в поле обнаружения другого врага)
    /// </summary>
    public bool UnderCover => IsUnderCover();

    /// <summary>
    /// Включено ли поле обнаружения
    /// </summary>
    /// <returns></returns>
    public bool IsEnabled()
    {
        foreach (var item in activeFovElements)
        {
            if (item.IsEnabled())
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Включить/выключить отображение поля зрения
    /// </summary>
    /// <param name="enabled"></param>
    public void SetEnabled(bool enabled)
    {
        foreach (var item in activeFovElements)
        {
            item.SetEnabled(enabled);
        }
    }

    /// <summary>
    /// Находится ли указанный союзник в поле обнаружения
    /// </summary>
    /// <param name="ally"></param>
    /// <returns></returns>
    public bool IsAllyInFov(Enemy ally)
    {
        foreach (var item in activeFovElements)
        {
            if (item.IsAllyInFov(ally))
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Находится ли данный враг в поле обнаружения союзников
    /// </summary>
    /// <returns></returns>
    public bool IsUnderCover()
    {
        foreach (var item in activeFovElements)
        {
            if (item.UnderCover)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Установить задержку обнаружения в спокойном состоянии
    /// </summary>
    public void SetIdleDetectionDelay()
    {
        foreach (var item in activeFovElements)
        {
            item.SetIdleDetectionDelay();
        }
    }

    /// <summary>
    /// Установить задержку обнаружения в состоянии тревоги
    /// </summary>
    public void SetAlertDetectionDelay()
    {
        foreach (var item in activeFovElements)
        {
            item.SetAlertDetectionDelay();
        }
    }

    /// <summary>
    /// Находится ли точка в радиусе обнаружения
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>
    public bool IsPointInFovRadius(Vector3 point)
    {
        foreach (var item in activeFovElements)
        {
            if (item.IsPointInFovRadius(point))
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Получить состояние поля обнаружения
    /// </summary>
    /// <returns></returns>
    public FovDetectingState GetDetectingState()
    {
        FovDetectingState maxDetectingState = FovDetectingState.NotDetected;
        foreach (var item in activeFovElements)
        {
            if (item.DetectingState != FovDetectingState.NotDetected)
            {
                maxDetectingState = item.DetectingState;
                if (item.DetectingState == FovDetectingState.Detected)
                {
                    break;
                }
            }
        }
        return maxDetectingState;
    }

    /// <summary>
    /// Задать состояние объекта. 
    /// Используется для восстановления состояния объекта при возрождении на контрольной точке.
    /// Потенциально метод может быть использован для загрузки из файла,
    /// но сейчас мы используем его только для того, чтобы при начале игры с контрольной точки
    /// восстановить первоначальное состояние объекта
    /// </summary>
    /// <param name="state"></param>
    public void SetState(object state)
    {
        foreach (var item in activeFovElements)
        {
            item.SetDefaultState();
        }
    }

    /// <summary>
    /// Получить состояние объекта.
    /// Потенциально метод может быть использован для сохранения объекта в файл,
    /// сейчас это не требуется, поэтому возвращаем пустую строку в качестве состояния
    /// </summary>
    /// <returns></returns>
    public object GetState()
    {
        return "";
    }

    private void Awake()
    {
        AddFov(visionFov);
        AddFov(sensitivityFov);
    }

    /// <summary>
    /// Добавить элемент поля обнаружения в список активных полей
    /// </summary>
    /// <param name="fov"></param>
    private void AddFov(FieldOfViewElement fov)
    {
        if (fov.gameObject.activeSelf)
        {
            activeFovElements.Add(fov);
        }
    }
}
