﻿
/// <summary>
/// Состояние обнаружения цели для отрисовщика поля обнаружения врагов
/// </summary>
public enum FovDrawerState
{
    /// <summary>
    /// Цель не обнаружена
    /// </summary>
    NotDetected,
    /// <summary>
    /// Цель обнаружена
    /// </summary>
    Detected,
    /// <summary>
    /// Цель обнаруживается полем обнаружения, но враг ещё не отреагировал
    /// </summary>
    Danger
}
