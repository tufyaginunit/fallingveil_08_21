﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Меш для отрисовки поля обнаружения врагов.
/// </summary>
public class FieldOfViewMesh : MonoBehaviour
{
    [field: SerializeField, Tooltip("Меш фильтр, используемый для построения модели поля зрения")]
    private MeshFilter ViewMeshFilter { get; set; } = null;

    [field: SerializeField, Tooltip("Меш рендерер, используемый для построения модели поля зрения")]
    private MeshRenderer ViewMeshRenderer { get; set; } = null;

    [SerializeField, Tooltip("Материал маски поля зрения врага в спокойном состоянии")]
    private Material fovMaskMaterial;

    [SerializeField, Tooltip("Материал маски поля зрения врага, который обнаружил игрока")]
    private Material fovMaskDetectedMaterial;

    [SerializeField, Tooltip("Материал маски поля зрения врага, когда игрок скоро будет замечен")]
    private Material fovMaskDangerMaterial;

    /// <summary>
    /// Сгенерированный меш модели поля зрения
    /// </summary>
    private Mesh viewMesh = null;

    /// <summary>
    /// Включён или выключен меш
    /// </summary>
    public bool IsEnabled => gameObject.activeSelf && ViewMeshRenderer.enabled;

    /// <summary>
    /// Включить/выключить меш
    /// </summary>
    /// <param name="enabled"></param>
    public void SetEnabled(bool enabled)
    {       
        if (gameObject.activeSelf != enabled)
        {
            gameObject.SetActive(enabled);
        }  
    }

    public void RefreshColor(FovDrawerState fovDrawerState)
    {
        // Задаём мешу поля зрения материал, соответствующий текущему состоянию обнаружения игрока
        Material currentMaterial = null;
        switch (fovDrawerState)
        {
            case FovDrawerState.NotDetected:
                currentMaterial = fovMaskMaterial;
                break;
            case FovDrawerState.Detected:
                currentMaterial = fovMaskDetectedMaterial;
                break;
            case FovDrawerState.Danger:
                currentMaterial = fovMaskDangerMaterial;
                break;
            default:
                break;
        }

        ViewMeshRenderer.sharedMaterial = currentMaterial;
    }

    public void Draw(Vector3 fovCenter, List<Vector3> fovPoints,
        FovDrawerState fovDrawerState)
    {
        int vertexCount = fovPoints.Count + 1;

        Vector3[] vertices = new Vector3[vertexCount];
        int[] triangles = new int[(vertexCount - 2) * 3];
        vertices[0] = transform.InverseTransformPoint(fovCenter);
        for (int i = 0; i < vertexCount - 1; i++)
        {
            vertices[i + 1] = transform.InverseTransformPoint(fovPoints[i]);
            if (i < vertexCount - 2)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }
        }

        viewMesh.Clear();    
        viewMesh.vertices = vertices;
        viewMesh.triangles = triangles;
        viewMesh.RecalculateNormals();

        RefreshColor(fovDrawerState);
    }

    private void Awake()
    {
        viewMesh = new Mesh();
        viewMesh.name = "Fov Mesh";
        ViewMeshFilter.mesh = viewMesh;

        SetEnabled(IsEnabled);
    }
}
