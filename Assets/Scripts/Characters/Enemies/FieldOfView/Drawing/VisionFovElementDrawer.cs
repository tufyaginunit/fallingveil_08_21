﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Отрисовщик зоны зрения врагов
/// </summary>
public class VisionFovElementDrawer : FovElementDrawer
{
    [SerializeField, Tooltip("Зона зрения")]
    private VisionFovElement visionFovElement;

    /// <summary>
    /// Данные вектора от центра поля зрения до некоторой точки
    /// </summary>
    public struct ViewCastInfo
    {
        /// <summary>
        /// Произошло ли столкновение с препятствием
        /// </summary>
        public bool hit;

        /// <summary>
        /// Конечная точка вектора поля зрения
        /// </summary>
        public Vector3 point;

        /// <summary>
        /// Длина вектора поля зрения
        /// </summary>
        public float dist;

        /// <summary>
        /// Угол вектора поля зрения
        /// </summary>
        public float angle;

        public ViewCastInfo(bool hit, Vector3 point, float dist,
            float angle)
        {
            this.hit = hit;
            this.point = point;
            this.dist = dist;
            this.angle = angle;
        }
    }

    public struct EdgeInfo
    {
        public Vector3 pointA;
        public Vector3 pointB;

        public EdgeInfo(Vector3 pointA, Vector3 pointB)
        {
            this.pointA = pointA;
            this.pointB = pointB;
        }
    }

    /// <summary>
    /// Количество итераций по уточнению границы препятствия
    /// </summary>
    const int EdgeResolveIterationCount = 10;

    /// <summary>
    /// Разница между длинами соседних векторов, при которой считаем,
    /// что нужно провести уточнение границ объекта
    /// </summary>
    const float EdgeDistanceThreshold = 0.5f;

    /// <summary>
    /// Получить границы векторов поля зрения
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="fovRotation"></param>
    /// <param name="fowAngle"></param>
    /// <param name="fovDistance"></param>
    /// <returns></returns>
    public static List<Vector3> GetFovEdgePointsWithObstacleInfluence(
        Vector3 fovCenter, float fovRotation, float fowAngle, float fovDistance)
    {
        List<Vector3> fovLines = new List<Vector3>();
        ViewCastInfo oldViewCastInfo = new ViewCastInfo();

        float minAngle = fovRotation - fowAngle / 2;
        float maxAngle = fovRotation + fowAngle / 2;
        float currentAngle = minAngle;

        bool isFirstIteration = true;
        while (true)
        {
            ViewCastInfo newViewCastInfo = ViewCast(fovCenter, currentAngle, fovDistance);

            bool isEdgeFound = false;
            if (!isFirstIteration)
            {
                bool isThresholdEsceeded =
                    Mathf.Abs(newViewCastInfo.dist - oldViewCastInfo.dist) > EdgeDistanceThreshold;

                if (newViewCastInfo.hit != oldViewCastInfo.hit ||
                    newViewCastInfo.hit && oldViewCastInfo.hit && isThresholdEsceeded)
                {
                    newViewCastInfo = FindEdge(fovCenter, fovDistance, oldViewCastInfo,
                        newViewCastInfo);
                    isEdgeFound = true;
                    currentAngle = newViewCastInfo.angle + 0.1f;
                }
            }

            fovLines.Add(newViewCastInfo.point);

            isFirstIteration = false;
            oldViewCastInfo = newViewCastInfo;

            if (currentAngle >= maxAngle)
            {
                break;
            }

            if (!isEdgeFound)
            {
                currentAngle += FieldOfViewManager.Instance.LinesStepInDegrees;
            }

            if (currentAngle > maxAngle)
            {
                currentAngle = maxAngle;
            }
        }

        return fovLines;
    }

    /// <summary>
    /// Отрисовать зону зрения
    /// </summary>
    protected override void Draw()
    {
        Vector3 fovCenter = fovElement.PointOfSight.transform.position;
        float fovRotation = fovElement.PointOfSight.transform.eulerAngles.y;

        // Получаем вектора для отрисовки поля обзора
        List<Vector3> fovPoints = GetFovEdgePointsWithObstacleInfluence(fovCenter, fovRotation,
            fovElement.Angle, fovElement.Distance);

        fovMesh.Draw(fovCenter, fovPoints, GetFovDrawerState());
    }

    private void Awake()
    {
        Initialize(visionFovElement);
    }

    /// <summary>
    /// Определить параметры вектора, выходящего под определённым углом из центра поля зрения
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="globalAngle"></param>
    /// <param name="fovDistance"></param>
    /// <returns></returns>
    private static ViewCastInfo ViewCast(Vector3 fovCenter, float globalAngle, float fovDistance)
    {
        Vector3 fovEdgeVector = VectorFunc.GetRotatedUnitVector(globalAngle);

        RaycastHit hitInfo = VisionFovElement.GetFovRaycastHit(fovCenter, fovEdgeVector, fovDistance);
        if (hitInfo.collider != null)
        {
            return new ViewCastInfo(true, hitInfo.point, hitInfo.distance, globalAngle);
        }
        else
        {
            return new ViewCastInfo(false, fovCenter + fovEdgeVector * fovDistance, fovDistance, globalAngle);
        }
    }

    /// <summary>
    /// Уточнить границу объекта
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="fovDistance"></param>
    /// <param name="minViewCast"></param>
    /// <param name="maxViewCast"></param>
    /// <returns></returns>
    private static ViewCastInfo FindEdge(Vector3 fovCenter, float fovDistance,
        ViewCastInfo minViewCast, ViewCastInfo maxViewCast)
    {
        float minAngle = minViewCast.angle;
        float maxAngle = maxViewCast.angle;
        ViewCastInfo newViewCast = new ViewCastInfo();

        for (int i = 0; i < EdgeResolveIterationCount; i++)
        {
            float angle = (minAngle + maxAngle) * .5f;
            newViewCast = ViewCast(fovCenter, angle, fovDistance);

            bool isThresholdEsceeded =
               Mathf.Abs(newViewCast.dist - minViewCast.dist) > EdgeDistanceThreshold;

            // Будем уточнять только правую границу
            if (newViewCast.hit == minViewCast.hit && !isThresholdEsceeded)
            {
                break;
            }
            else
            {
                maxAngle = angle;
            }
        }
        return newViewCast;

    }
}
