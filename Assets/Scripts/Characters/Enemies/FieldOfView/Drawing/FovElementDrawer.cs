﻿using UnityEngine;

/// <summary>
/// Отрисовщик элемента поля обнаружения врагов
/// </summary>
public abstract class FovElementDrawer : MonoBehaviour, ICulling
{
    [SerializeField, Tooltip("Поле обнаружения")]
    protected FieldOfView fov = null;

    [SerializeField, Tooltip("Меш поля обнаружения")]
    protected FieldOfViewMesh fovMesh = null;

    /// <summary>
    /// Элемент поля обнаружения, который нужно визуализировать
    /// </summary>
    protected FieldOfViewElement fovElement = null;

    public ICulling CullingObject => this;

    float ICulling.Size => fovElement.Distance;

    bool ICulling.Active => gameObject.activeSelf;

    void ICulling.SetActive(bool active)
    {
        gameObject.SetActive(active);
    }

    /// <summary>
    /// Отрисовать элемент поля обнаружения
    /// </summary>
    protected abstract void Draw();

    protected virtual void LateUpdate()
    {
        bool fovEnabled = fovElement.IsEnabled();
        if (fovEnabled && fovMesh.IsEnabled)
        {
            Draw();
        }

        if (fovEnabled != fovMesh.IsEnabled)
        {
            fovMesh.SetEnabled(fovEnabled);
        }
    }

    /// <summary>
    /// Инициализировать отрисовщик
    /// </summary>
    /// <param name="fov">Элемент поля обнаружения, который нужно визуализировать</param>
    public virtual void Initialize(FieldOfViewElement fovElement)
    {
        this.fovElement = fovElement;
    }

    /// <summary>
    /// Получить состояние отрисовщика (цель обнаружена, не обнаружена, почти обнаружена)
    /// </summary>
    /// <returns></returns>
    protected FovDrawerState GetFovDrawerState()
    {
        FovDrawerState fowDrawerState;
        if (fov.DetectingState == FovDetectingState.NotDetected)
        {
            // В случае, если игрок находится в поле зрения врага, но пока ещё им не обнаружен, 
            // задаём его полю зрения соответствующий материал состояния опасности
            fowDrawerState = fovElement.TargetInFov ? FovDrawerState.Danger
                : FovDrawerState.NotDetected;
        }
        else
        {
            fowDrawerState = FovDrawerState.Detected;
        }

        return fowDrawerState;
    }

    bool ICulling.GetNearestBodyPoint(Vector3 point, out Vector3 nearestPoint)
    {
        nearestPoint = fovElement.PointOfSight.position;
        return true;
    }
}
