﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Отрисовщик зоны чувствительности врагов
/// </summary>
public class SensitivityFovElementDrawer : FovElementDrawer
{
    [SerializeField, Tooltip("Зона чувствительности")]
    private SensitivityFovElement sensitivityFovElement;
    
    /// <summary>
    /// Информация о столкновениях NonAlloc-рэйкаста (для оптимизации)
    /// </summary>
    private readonly RaycastHit[] currentHits = new RaycastHit[100];

    /// <summary>
    /// Получить список точек границы зоны чувствительности 
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="fovRotation"></param>
    /// <param name="fowAngle"></param>
    /// <param name="fovDistance"></param>
    /// <returns></returns>
    public List<Vector3> GetSensitivityFovEdgePoints(
        Vector3 fovCenter, float fovRotation, float fowAngle, float fovDistance)
    {
        List<Vector3> fovLines = new List<Vector3>();

        float minAngle = fovRotation - fowAngle / 2;
        float maxAngle = fovRotation + fowAngle / 2;
        float currentAngle = minAngle;
        while (currentAngle < maxAngle)
        {
            Vector3 point = GetSensitivityEdgePoint(fovCenter, currentAngle, fovDistance);

            fovLines.Add(point);

            currentAngle += FieldOfViewManager.Instance.LinesStepInDegrees;
            if (currentAngle >= maxAngle)
            {
                point = GetSensitivityEdgePoint(fovCenter, currentAngle, fovDistance);

                fovLines.Add(point);
                break;
            }
        }

        return fovLines;
    }

    /// <summary>
    /// Отрисовать зону чувствительности
    /// </summary>
    protected override void Draw()
    {
        FullDraw();
    }

    private void Awake()
    {
        Initialize(sensitivityFovElement);
    }

    /// <summary>
    /// Выполнить полную отрисовку
    /// </summary>
    private void FullDraw()
    {
        if (!fovMesh.IsEnabled)
        {
            return;
        }

        Vector3 fovCenter = fovElement.PointOfSight.transform.position;
        fovCenter.y = GameManager.Instance.GroundY;
        float fovRotation = fovElement.PointOfSight.transform.eulerAngles.y;

        // Получаем вектора для отрисовки поля чувствительности
        List<Vector3> fovPoints = GetSensitivityFovEdgePoints(fovCenter, fovRotation, fovElement.Angle, fovElement.Distance);

        fovMesh.Draw(fovCenter, fovPoints, GetFovDrawerState());
    }

    /// <summary>
    /// Получить список точек границы зоны чувствительности 
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="fovRotation"></param>
    /// <param name="fowAngle"></param>
    /// <param name="fovDistance"></param>
    /// <returns></returns>
    private static List<Vector3> GetSimpleFovEdgePoints(
        Vector3 fovCenter, float fovRotation, float fowAngle, float fovDistance)
    {
        List<Vector3> fovLines = new List<Vector3>();

        float minAngle = fovRotation - fowAngle / 2;
        float maxAngle = fovRotation + fowAngle / 2;
        float currentAngle = minAngle;
        while (currentAngle < maxAngle)
        {
            Vector3 point = fovCenter + VectorFunc.GetRotatedUnitVector(currentAngle) * fovDistance;
            fovLines.Add(point);

            currentAngle += FieldOfViewManager.Instance.LinesStepInDegrees;
            if (currentAngle >= maxAngle)
            {
                point = fovCenter + VectorFunc.GetRotatedUnitVector(maxAngle) * fovDistance;
                fovLines.Add(point);
                break;
            }
        }

        return fovLines;
    }

    /// <summary>
    /// Получить самую дальнюю достижимую точку за препятствиями, находящимися
    /// на пути вектора поля чувтсительности
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="direction"></param>
    /// <param name="fovDistance"></param>
    /// <returns></returns>
    private Vector3 GetAchievablePointBehindObstacles(Vector3 fovCenter, Vector3 direction, float fovDistance)
    {
        Vector3 fovEdgePoint = fovCenter + direction * fovDistance;
        Vector3 directionToFovCenter = -direction;

        float restDistance = fovDistance;
        while (restDistance > 0.5f)
        {
            RaycastHit hit = VisionFovElement.GetFovRaycastHit(fovEdgePoint, directionToFovCenter, restDistance);

            if (hit.collider == null)
            {
                break;
            }

            if (sensitivityFovElement.CanMoveBehindObstacle(hit, direction))
            {
                break;
            }

            fovEdgePoint = hit.point + directionToFovCenter * 0.5f;
            restDistance -= hit.distance + 0.5f;
        }
        return fovEdgePoint;
    }

    private Vector3 GetAchievablePointBehindObstaclesAllHits(Vector3 fovCenter, Vector3 direction, float fovDistance)
    {
        Vector3 fovEdgePoint = fovCenter + direction * fovDistance;
        Vector3 directionToFovCenter = -direction;

        int hitsCount = VisionFovElement.FovRaycastNonAlloc(fovEdgePoint, directionToFovCenter, fovDistance, currentHits);

        for (int i = 0; i < hitsCount - 1; i++)
        {
            for (int j = i + 1; j < hitsCount; j++)
            {
                if (currentHits[j].distance < currentHits[i].distance)
                {
                    RaycastHit temp = currentHits[i];
                    currentHits[i] = currentHits[j];
                    currentHits[j] = temp;
                }
            }
        }

        for (int i = 0; i < hitsCount; i++)
        {
            if (sensitivityFovElement.CanMoveBehindObstacle(currentHits[i], direction))
            {
                break;
            }

            fovEdgePoint = currentHits[i].point;
        }

        return fovEdgePoint;
    }
    /// <summary>
    /// Получить точку границы зоны чувствительности
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="globalAngle"></param>
    /// <param name="fovDistance"></param>
    /// <returns></returns>
    private Vector3 GetSensitivityEdgePoint(Vector3 fovCenter, float globalAngle, float fovDistance)
    {
        Vector3 direction = VectorFunc.GetRotatedUnitVector(globalAngle);

        return GetAchievablePointBehindObstacles(fovCenter, direction, fovDistance);
    }
}
