﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// Отрисовка вспомогательной информации в SceneView для настройки компонента FieldOfView
/// </summary>
[CustomEditor(typeof(FieldOfView))]
public class FieldOfViewEditor : Editor
{
    private void OnSceneGUI()
    {
        FieldOfView fov = target as FieldOfView;

        // Визуализируем поле зрения
        VisualizeFOV(fov.VisionFov);
        // Визуализируем зону чувствительности
        VisualizeFOV(fov.SensitivityFov);
    }

    private void VisualizeFOV(FieldOfViewElement fovElement)
    {
        if (!fovElement.isActiveAndEnabled)
        {
            return;
        }
        Vector3 sightCenter = fovElement.PointOfSight.transform.position;
        float fovRotation = fovElement.PointOfSight.transform.eulerAngles.y;

        VisualizeFOV(sightCenter, fovRotation, fovElement.Angle, fovElement.Distance);
    }

    /// <summary>
    /// Визуализировать поле зрения
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="fovRotation"></param>
    /// <param name="fowAngle"></param>
    /// <param name="fowDistance"></param>
    private void VisualizeFOV(Vector3 fovCenter, float fovRotation, float fowAngle, float fowDistance)
    {
        VisualizeOutsideFOV(fovCenter, fovRotation, fowAngle, fowDistance);
    }

    /// <summary>
    /// Визуализировать внешнюю часть поля зрения
    /// </summary>
    /// <param name="fovCenter"></param>
    /// <param name="fovRotation"></param>
    /// <param name="fowAngle"></param>
    /// <param name="fowDistance"></param>
    private void VisualizeOutsideFOV(Vector3 fovCenter, float fovRotation, float fowAngle, float fowDistance)
    {
        Vector3 fovEdgeVector1 = VectorFunc.GetRotatedUnitVector(fovRotation - fowAngle / 2);
        Vector3 fovEdgeVector2 = VectorFunc.GetRotatedUnitVector(fovRotation + fowAngle / 2);

        Handles.color = Color.yellow;

        Handles.DrawWireArc(fovCenter, Vector3.up, fovEdgeVector1, fowAngle, fowDistance);

        Handles.DrawLine(fovCenter, fovCenter + fovEdgeVector1 * fowDistance);
        Handles.DrawLine(fovCenter, fovCenter + fovEdgeVector2 * fowDistance);
    }
}
