﻿using UnityEngine;

/// <summary>
/// Элемент поля обнаружения, отвечающий за зону зрения
/// </summary>
public class VisionFovElement : FieldOfViewElement
{
    /// <summary>
    /// Проверить, находится ли точка в зоне зрения
    /// </summary>
    /// <param name="point"></param>
    /// <returns></returns>    
    public override bool IsPointDetectingByFov(Vector3 point)
    {
        if (!base.IsPointDetectingByFov(point))
        {
            return false;
        }

        Vector3 vectorToTarget = point - pointOfSight.position;
        vectorToTarget.y = 0;

        // Если препятствий на пути к цели нет, то цель видна
        RaycastHit hitInfo = GetFovRaycastHit(pointOfSight.position, vectorToTarget,
            vectorToTarget.magnitude);
        return hitInfo.collider == null;
    }

    /// <summary>
    /// Проверить, обнаруживается ли цель в данный момент зоной чувствительности
    /// </summary>
    /// <returns></returns>
    public override bool IsTargetDetectingByFov()
    {
        return IsPointDetectingByFov(target.Transform.position) && !target.IsInvisible();
    }

    /// <summary>
    /// Получить параметры рэйкаста вектора от центра на указанную дистанцию
    /// </summary>
    /// <param name="startPoint">Начальная точка вектора</param>
    /// <param name="direction">Направление вектора</param>
    /// <param name="distance">Длина вектора</param>
    /// <returns></returns>
    public static RaycastHit GetFovRaycastHit(Vector3 startPoint, Vector3 direction, float distance)
    {
        if (Physics.Raycast(startPoint, direction, out RaycastHit hitInfo, distance,
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore))
        {
            return hitInfo;
        }
        else
        {
            return new RaycastHit();
        }
    }

    /// <summary>
    /// Получить параметры рэйкаста вектора от центра на указанную дистанцию (все столкновения)
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="direction"></param>
    /// <param name="distance"></param>
    /// <returns></returns>
    public static RaycastHit[] GetAllFovRaycastHits(Vector3 startPoint, Vector3 direction, float distance)
    {
        return Physics.RaycastAll(startPoint, direction, distance,
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore);
    }

    /// <summary>
    /// Получить параметры рэйкаста вектора от центра на указанную дистанцию (все столкновения)
    /// Новые объекты не создаются.
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="direction"></param>
    /// <param name="distance"></param>
    /// <param name="hits"></param>
    /// <returns></returns>
    public static int FovRaycastNonAlloc(Vector3 startPoint, Vector3 direction, float distance, RaycastHit[] hits)
    {
        return Physics.RaycastNonAlloc(new Ray(startPoint, direction), hits, distance,
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore);
    }

    /// <summary>
    /// Получить результат рэйкаста определённого коллайдера
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="direction"></param>
    /// <param name="collider"></param>
    /// <returns></returns>
    public static RaycastHit GetFovRaycastHit(Vector3 startPoint, Vector3 direction, Collider collider)
    {
        Ray ray = new Ray(startPoint, direction);
        if (collider.Raycast(ray, out RaycastHit hitInfo, VectorFunc.MaxRaycastDistance))
        {
            return hitInfo;
        }
        else
        {
            return new RaycastHit();
        }
    }
}
