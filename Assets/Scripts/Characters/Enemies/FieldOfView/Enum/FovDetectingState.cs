﻿
/// <summary>
/// Состояние обнаружения цели
/// </summary>
public enum FovDetectingState
{
    /// <summary>
    /// Цель обнаружена
    /// </summary>
    Detected,
    /// <summary>
    /// Цель не обнаружена
    /// </summary>
    NotDetected,
    /// <summary>
    /// Союзник в поле зрения атакован
    /// </summary>
    AllyIsUnderAttack
}
