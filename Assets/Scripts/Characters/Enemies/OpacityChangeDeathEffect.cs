﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Эффект смерти врага. Использует указанную стратегию изменения прозрачности.
/// Рекомендуется добавлять на отдельный дочерний объект врага.
/// </summary>
public class OpacityChangeDeathEffect : DeathEffect, ISaveable
{
    [SerializeField, Tooltip("Контейнер моделей, материалы которых будут изменяться")]
    private Transform model = null;

    [SerializeField, Tooltip("Стратегия изменения материалов")]
    private OpacityChangingStrategy changingStrategy;

    private MaterialChangingObject materialChangingModel;

    /// <summary>
    /// Материалы призрака
    /// </summary>
    private List<Material> materials;

    /// <summary>
    /// Задать состояние объекта при возрождении на контрольной точке.
    /// </summary>
    /// <param name="state"></param>
    public override void SetState(object state)
    {
        base.SetState(state);
        foreach (var material in materials)
        {
            changingStrategy.SetOpacity(material, 1);
        }
    }

    /// <summary>
    /// Обработать событие смерти призрака
    /// </summary>
    protected override void ProcessDeath()
    {
        StartCoroutine(PerformDeath(deathDuration));
    }

    protected override void Awake()
    {
        base.Awake();
        materialChangingModel = new MaterialChangingObject(model, changingStrategy);
    }

    /// <summary>
    /// Корутина, выполняющая плавное растворение врага
    /// </summary>
    /// <returns></returns>
    private IEnumerator PerformDeath(float deathDuration)
    {
        float timePassed = 0;
        while (timePassed < deathDuration)
        {
            yield return null;
            timePassed += Time.deltaTime;
            materialChangingModel.SetNormalizedValue(timePassed / deathDuration);
        }
    }

}
