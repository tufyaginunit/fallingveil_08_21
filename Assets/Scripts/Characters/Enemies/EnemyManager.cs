﻿using System;
using System.Collections.Generic;

/// <summary>
/// Менеджер объектов-врагов. Ведёт список всех врагов на сцене.
/// При появлении на сцене враги обращаются к этому классу и сами добавляют себя в список.
/// Синглтон-класс. Рекомендуется добавлять как отдельный дочерний объект объекта GameManager
/// </summary>
public class EnemyManager : Singleton<EnemyManager>
{
    /// <summary>
    /// Список всех врагов
    /// </summary>
    private Enemy[] enemies = null;

    public IEnumerable<Enemy> Enemies => enemies;

    private event Action EnemyDefeated = null;

    public static float GetMaxFovDistance(IEnumerable<Enemy> enemies, bool aliveOnly)
    {
        float maxFovDistance = 0;

        foreach (var enemy in enemies)
        {
            if (aliveOnly && !enemy.IsAlive())
            {
                continue;
            }

            if (maxFovDistance < enemy.Fov.VisionFov.Distance)
            {
                maxFovDistance = enemy.Fov.VisionFov.Distance;
            }
            if (maxFovDistance < enemy.Fov.SensitivityFov.Distance)
            {
                maxFovDistance = enemy.Fov.SensitivityFov.Distance;
            }
        }

        return maxFovDistance;
    }

    public void RegisterEnemyDefeatedListener(Action listener)
    {
        EnemyDefeated += listener;
    }

    public void UnregisterEnemyDefeatedListener(Action listener)
    {
        EnemyDefeated -= listener;
    }

    /// <summary>
    /// Получить количество побеждённых врагов
    /// </summary>
    /// <returns></returns>
    public int GetDefeatedEnemyCount()
    {
        int defeatedEnemyCount = 0;
        foreach (var enemy in enemies)
        {
            if (!enemy.IsAlive())
            {
                defeatedEnemyCount++;
            }
        }
        return defeatedEnemyCount;
    }

    private void Awake()
    {
        enemies = FindObjectsOfType<Enemy>();
        foreach (var enemy in enemies)
        {
            enemy.RegisterKilledListener(OnEnemyKilled);
        }
    }

    /// <summary>
    /// Обработчик события убийства какого-либо врага
    /// </summary>
    private void OnEnemyKilled()
    {
        EnemyDefeated?.Invoke();
    }
}
