﻿using UnityEngine;

/// <summary>
/// Отрисовщик подсветки врага, который находится под прикрытием своего союзника
/// (попадает в поле зрения союзника).
/// Цвет задаётся на основе цвета поля зрения врагов
/// </summary>
public class EnemyUnderCoverDrawer: MonoBehaviour
{
    [SerializeField, Tooltip("Объект врага")]
    private Enemy enemy;

    [SerializeField, Tooltip("Контейнер моделей, на рендереры которых будут добавлены материалы обводки")]
    private Transform modelContainer;

    [SerializeField, Tooltip("Скрипт отрисовки подсветки")]
    private HighlightPlus.HighlightEffect highlightEffect;

    /// <summary>
    /// Кэшированное значение флага видимости врага своими союзниками
    /// </summary>
    private bool currentUnderCover = false;

    /// <summary>
    /// Кэшированное значение цвета поля зрения
    /// </summary>
    private Color currentFovColor = default;

    private void Start()
    {
        RefreshHighlighted();
        RefreshColor();
    }

    private void Update()
    {
        bool newUnderCoverValue = FieldOfViewManager.Instance.IsVisible && enemy.UnderCover;
        if (currentUnderCover != newUnderCoverValue)
        {
            currentUnderCover = newUnderCoverValue;
            RefreshHighlighted();
        }
        if (currentFovColor != FieldOfViewManager.Instance.FovMaterial.color)
        {
            currentFovColor = FieldOfViewManager.Instance.FovMaterial.color;
            RefreshColor();
        }
    }

    /// <summary>
    /// Обновить толщину линии обводки
    /// </summary>
    private void RefreshHighlighted()
    {
        highlightEffect.SetHighlighted(currentUnderCover);
    }
    
    /// <summary>
    /// Обновить цвет линии обводки
    /// </summary>
    private void RefreshColor()
    {       
        Color outlineColor = currentFovColor;

        // Убираем интенсивность из цвета подсветки поля зрения, иначе враг будет светиться очень ярко
        outlineColor /= Mathf.Max(outlineColor.a, outlineColor.g, outlineColor.b);

        highlightEffect.overlayColor = outlineColor;
    }
}
