﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

/// <summary>
/// Абстрактный класс, представляющий некоторый повреждаемый объект. 
/// От него создаются конкретные наследники (например, игрок, враг).
/// Представители этого класса добавляются на объекты, которые должны иметь запас здоровья 
/// и которые можно уничтожить.
/// Если нужно создать принципиально новый повреждаемый объект, его нужно унаследовать
/// от данного класса.
/// </summary>
public abstract class DamageableObject : MonoBehaviour, IDamageable, ISaveable
{
    [SerializeField]
    [Tooltip("Коллайдер объекта")]
    protected Collider damageableCollider = null;

    [SerializeField]
    [Tooltip("Начальное (максимальное) здоровье")]
    protected int startHealth = 100;

    [SerializeField]
    [Tooltip("Включена или выключена регенерация")]
    protected bool regenerationEnabled = false;

    [SerializeField]
    [Tooltip("Время регенерации одного хитпойнта")]
    protected float oneHitRegenerationTime = 1;

    [SerializeField]
    [Tooltip("Длительность кулдауна регенерации после получения урона")]
    protected float regenerationCoolDownDuration = 0;

    [SerializeField]
    [Tooltip("Эффект при попадании")]
    protected VisualEffect hitVisualEffect;

    [SerializeField]
    [Tooltip("Эффект при излечении")]
    protected VisualEffect healVisualEffect;

    [SerializeField]
    [Tooltip("Эффект кулдауна регенерации после получения урона")]
    protected VisualEffect regenerationCoolDownVisualEffect;

    [SerializeField]
    [Tooltip("Является ли объект бессмертным")]
    protected bool immortal = false;

    [SerializeField]
    [Tooltip("Может ли становиться невидимым")]
    protected bool canBeInvisible = false;

    [SerializeField]
    [Tooltip("Время сжигания одного хитпойнта при невидимости")]
    protected float oneHitDamageByInvisibilityTime = 1;

    /// <summary>
    /// Момент времени, когда завершится кулдаун регенерации
    /// </summary>
    protected float regenerationRecoveryTime = 0;

    /// <summary>
    /// Корутина процесса регенерации
    /// </summary>
    protected Coroutine regenerationCoroutine = null;

    /// <summary>
    /// Корутина невидимости
    /// </summary>
    protected Coroutine invisibilityCoroutine = null;

    /// <summary>
    /// Корутина кулдауна регенерации
    /// </summary>
    protected Coroutine regenerationCoolDownCoroutine = null;

    /// <summary>
    /// Текущее значение HP
    /// </summary>
    public int Health { get; private set; }

    /// <summary>
    /// Максимальное значение HP
    /// </summary>
    public int MaxHealth => startHealth;

    /// <summary>
    /// Transform объекта
    /// </summary>
    public Transform Transform => transform;

    /// <summary>
    /// Коллайдер объекта
    /// </summary>
    public Collider DamageableCollider => damageableCollider;

    /// <summary>
    /// Находится ли сейчас регенерация в стадии кулдауна
    /// </summary>
    public bool IsRegenerationCoolDowning => regenerationCoolDownCoroutine != null;

    /// <summary>
    /// Событие, вызываемое при изменении HP
    /// </summary>
    private event Action HealthChanged;

    /// <summary>
    /// Событие смерти, вызываемое при уменьшении HP до нуля
    /// </summary>
    private event Action Killed;

    /// <summary>
    /// Событие воскрешения, вызываемое при возрождении игрока с контрольной точки
    /// </summary>
    private event Action Revived;

    /// <summary>
    /// Событие, вызываемое при получении урона
    /// </summary>
    private event Action Hitted;

    /// <summary>
    /// Событие, вызываемое при восполнении здоровья после убийства врага
    /// </summary>
    private event Action Healed;

    /// <summary>
    /// Событие, вызываемое при изменении невидимости
    /// </summary>
    private event Action InvisibilityChanged;

    /// <summary>
    /// Проверить, жив ли объект
    /// </summary>
    /// <returns></returns>
    public virtual bool IsAlive()
    {
        return Health > 0;
    }

    /// <summary>
    /// Проверить, является ли сейчас объект невидимым
    /// </summary>
    /// <returns></returns>
    public virtual bool IsInvisible() => invisibilityCoroutine != null;

    /// <summary>
    /// Обработать получение урона
    /// </summary>
    /// <param name="damageValue">Количество урона</param>
    public virtual void ReceiveDamage(int damageValue)
    {
        if (immortal)
        {
            return;
        }

        if (Health <= 0)
        {
            return;
        }

        SetHealth(Health - damageValue);

        if (Health <= 0)
        {
            Killed?.Invoke();
            StopRegenerationCoolDown();
            StopAllCoroutines();
        }
    }

    /// <summary>
    /// Добавить HP
    /// </summary>
    /// <param name="healthToAdd">Количество HP для добавления</param>
    public void AddHealth(int healthToAdd)
    {
        if (healthToAdd < 0)
        {
            return;
        }

        SetHealth(Health + healthToAdd);
    }

    /// <summary>
    /// Запуск эффекта при нанесении урона объекту
    /// </summary>
    public virtual void HitEffect()
    {
        Hitted?.Invoke();
        StartRegenerationCoolDown();
        
        if (hitVisualEffect != null)
        {
            hitVisualEffect.Play();
        }
    }

    /// <summary>
    /// Запуск эффекта при лечении объекта
    /// </summary>
    public virtual void HealEffect()
    {
        Healed?.Invoke();
        if (healVisualEffect != null)
        {
            healVisualEffect.Play();
        }
    }

    /// <summary>
    /// Включить/выключить регенерацию
    /// </summary>
    /// <param name="enabled"></param>
    public virtual void SetEnabledRegeneration(bool enabled)
    {
        if (!regenerationEnabled || enabled && !IsAlive())
        {
            return;
        }
        this.StopAndNullCoroutine(ref regenerationCoroutine);
        if (enabled)
        {
            regenerationCoroutine = StartCoroutine(PerformRegeneration());
        }
    }

    /// <summary>
    /// Включить/выключить регенерацию
    /// </summary>
    /// <param name="enabled"></param>
    public virtual void SetEnabledInvisibility(bool enabled)
    {
        if (!canBeInvisible || enabled && Health <= 1)
        {
            return;
        }
        this.StopAndNullCoroutine(ref invisibilityCoroutine);
        if (enabled)
        {
            invisibilityCoroutine = StartCoroutine(PerformInvisibility());
        }
        InvisibilityChanged?.Invoke();
    }

    /// <summary>
    /// Добавить слушателя события изменения HP
    /// </summary>
    /// <param name="listener"></param>
    public virtual void RegisterHealthChangedListener(Action listener)
    {
        HealthChanged += listener;
    }
    
    /// <summary>
    /// Удалить слушателя события изменения HP
    /// </summary>
    /// <param name="listener"></param>
    public virtual void UnregisterHealthChangedListener(Action listener)
    {
        HealthChanged -= listener;
    }

    /// <summary>
    /// Добавить слушателя события смерти объекта
    /// </summary>
    /// <param name="listener"></param>
    public virtual void RegisterKilledListener(Action listener)
    {
        Killed += listener;
    }

    /// <summary>
    /// Удалить слушателя события смерти объекта
    /// </summary>
    /// <param name="listener"></param>
    public virtual void UnregisterKilledListener(Action listener)
    {
        Killed -= listener;
    }

    /// <summary>
    /// Добавить слушателя события воскрешения объекта
    /// </summary>
    /// <param name="listener"></param>
    public virtual void RegisterRevivedListener(Action listener)
    {
        Revived += listener;
    }

    /// <summary>
    /// Удалить слушателя события воскрешения объекта
    /// </summary>
    /// <param name="listener"></param>
    public virtual void UnregisterRevivedListener(Action listener)
    {
        Revived -= listener;
    }

    /// <summary>
    /// Добавить слушателя события получения урона
    /// </summary>
    /// <param name="listener"></param>
    public virtual void RegisterHittedListener(Action listener)
    {
        Hitted += listener;
    }

    /// <summary>
    /// Удалить слушателя события получения урона
    /// </summary>
    /// <param name="listener"></param>
    public virtual void UnregisterHittedListener(Action listener)
    {
        Hitted -= listener;
    }

    /// <summary>
    /// Добавить слушателя события восполнения здоровья после убийства врага
    /// </summary>
    /// <param name="listener"></param>
    public virtual void RegisterHealedListener(Action listener)
    {
        Healed += listener;
    }

    /// <summary>
    /// Удалить слушателя события восполнения здоровья после убийства врага
    /// </summary>
    /// <param name="listener"></param>
    public virtual void UnregisterHealedListener(Action listener)
    {
        Healed -= listener;
    }

    /// <summary>
    /// Добавить слушателя события изменения невидимости
    /// </summary>
    /// <param name="listener"></param>
    public virtual void RegisterInvisibilityChangedListener(Action listener)
    {
        InvisibilityChanged += listener;
    }

    /// <summary>
    /// Удалить слушателя события изменения невидимости
    /// </summary>
    /// <param name="listener"></param>
    public virtual void UnregisterInvisibilityChangedListener(Action listener)
    {
        InvisibilityChanged -= listener;
    }

    protected virtual void Awake()
    {
        SetDefault();
    }

    protected virtual void Destroy()
    {
    }

    protected virtual void Start()
    {
    }

    protected virtual void SetDefault()
    {
        StopAllCoroutines();

        SetStartHealth();

        SetEnabledInvisibility(false);
        SetEnabledRegeneration(true);
        
        StopRegenerationCoolDown();
    }

    /// <summary>
    /// Установить значение HP
    /// </summary>
    /// <param name="healthValue">Значение HP</param>
    protected virtual void SetHealth(int healthValue)
    {
        Health = healthValue;
        HealthChanged?.Invoke();
        if (Health <= 0)
        {
            Health = 0;
        }
        else if (Health > MaxHealth)
        {
            Health = MaxHealth;
        }
    }

    /// <summary>
    /// Полностью восстановить здоровье 
    /// </summary>
    public virtual void SetStartHealth()
    {
        SetHealth(startHealth);
    }

    /// <summary>
    /// Корутина, выполняющее постепенную регенерацию HP
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator PerformRegeneration()
    {
        float timeToNextRegeneration = Time.time + oneHitRegenerationTime;
        while (true)
        {
            if (IsAlive() && !IsInvisible() && !IsRegenerationCoolDowning && Time.time > timeToNextRegeneration)
            {
                if (Health < MaxHealth)
                {
                    AddHealth(1);
                }
                timeToNextRegeneration = Time.time + oneHitRegenerationTime;
            }

            yield return new WaitForFixedUpdate();
        }
    }

    /// <summary>
    /// Запустить корутину кулдауна регенерации
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator PerformRegenerationCoolDown()
    {
        while (Time.time < regenerationRecoveryTime)
        {
            yield return new WaitForSeconds(0.1f);
        }

        StopRegenerationCoolDown();
    }

    /// <summary>
    /// Запустить корутину невидимости, постепенно сжигающую здоровье
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator PerformInvisibility()
    {
        float timeToNextDamage = Time.time + oneHitDamageByInvisibilityTime;
        while (IsAlive() && Health > 1)
        {
            if (Time.time > timeToNextDamage)
            {
                ReceiveDamage(1);
                timeToNextDamage = Time.time + oneHitDamageByInvisibilityTime;
            }

            yield return new WaitForFixedUpdate();
        }

        invisibilityCoroutine = null;
        InvisibilityChanged?.Invoke();
    }

    /// <summary>
    /// Начать кулдаун регенерации
    /// </summary>
    protected virtual void StartRegenerationCoolDown()
    {
        if (regenerationCoolDownDuration <= 0)
        {
            return;
        }

        if (regenerationCoolDownVisualEffect != null)
        {
            regenerationCoolDownVisualEffect.Play();
        }

        regenerationRecoveryTime = Time.time + regenerationCoolDownDuration;
        if (regenerationCoolDownCoroutine == null)
        {
            regenerationCoolDownCoroutine = StartCoroutine(PerformRegenerationCoolDown());
        }
    }

    /// <summary>
    /// Остановить кулдаун регенерации
    /// </summary>
    protected virtual void StopRegenerationCoolDown()
    {
        if (regenerationCoolDownDuration <= 0)
        {
            return;
        }

        if (regenerationCoolDownVisualEffect != null)
        {
            regenerationCoolDownVisualEffect.Stop();
        }

        if (regenerationCoolDownCoroutine != null)
        {
            StopCoroutine(regenerationCoolDownCoroutine);
            regenerationCoolDownCoroutine = null;
        }
        
    }

    /// <summary>
    /// Задать состояние объекта. 
    /// Используется для восстановления состояния объекта при возрождении на контрольной точке.
    /// Потенциально метод может быть использован для загрузки из файла,
    /// но сейчас мы используем его только для того, чтобы при начале игры с контрольной точки
    /// восстановить первоначальное состояние объекта
    /// </summary>
    /// <param name="state"></param>
    public virtual void SetState(object state)
    {
        // При загрузке с контрольной точки персонаж может быть жив или мёртв.
        // Посылаем событие воскрешения только в случае, если персонаж мёртв
        bool wasDead = !IsAlive();

        SetDefault();

        if (wasDead)
        {
            Revived?.Invoke();
        }        
    }

    /// <summary>
    /// Получить состояние объекта.
    /// Потенциально метод может быть использован для сохранения объекта в файл,
    /// сейчас это не требуется, поэтому возвращаем пустую строку в качестве состояния
    /// </summary>
    /// <returns></returns>
    public virtual object GetState()
    {
        return "";
    }
}
