﻿using System;
using UnityEngine;

/// <summary>
/// Триггер, уведомляющий о своём срабатывании подписчиков
/// </summary>
[RequireComponent(typeof(Collider))]
public class TriggerEvent : MonoBehaviour
{
    /// <summary>
    /// Событие, вызываемое при срабатывании триггера
    /// </summary>
    public event Action<Collider> Entered = null;

    private void OnTriggerEnter(Collider other)
    {
        Entered?.Invoke(other);
    }
}
