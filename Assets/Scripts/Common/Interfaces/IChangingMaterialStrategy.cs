﻿using UnityEngine;

/// <summary>
/// Интерфейс стратегии изменения материалов
/// </summary>
public interface IChangingMaterialStrategy
{
    /// <summary>
    /// Соответствует ли материал данной стратегии
    /// </summary>
    /// <param name="material"></param>
    /// <returns></returns>
    bool IsMaterialMatchStrategy(Material material);

    /// <summary>
    /// Аналог Lerp. Установить значение от min до max значения свойства, 
    /// на основе переданного значения в диапазоне [0 ; 1]
    /// </summary>
    /// <param name="material"></param>
    /// <param name="value"></param>
    void SetNormalizedValue(Material material, float value);
}
