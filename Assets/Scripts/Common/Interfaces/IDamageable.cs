﻿using System;
using UnityEngine;

/// <summary>
/// Интерфейс объекта, которому можно нанести повреждения и уничтожить
/// </summary>
public interface IDamageable 
{
    /// <summary>
    /// Трансформ объекта
    /// </summary>
    Transform Transform { get; }

    /// <summary>
    /// Текущее значение HP объекта
    /// </summary>
    int Health { get; }

    /// <summary>
    /// Максимальное значение HP объекта
    /// </summary>
    int MaxHealth { get; }

    /// <summary>
    /// Обработать получение урона
    /// </summary>
    /// <param name="damageValue">Значение урона</param>
    void ReceiveDamage(int damageValue);

    /// <summary>
    /// Добавить HP
    /// </summary>
    /// <param name="healthToAdd">Количество HP для добавления</param>
    void AddHealth(int healthToAdd);

    /// <summary>
    /// Эффект при получении урона
    /// </summary>
    void HitEffect();

    /// <summary>
    /// Проверить, жив ли объект
    /// </summary>
    /// <returns></returns>
    bool IsAlive();

    /// <summary>
    /// Проверить, является ли объект в данный момент невидимым
    /// </summary>
    /// <returns></returns>
    bool IsInvisible();

    /// <summary>
    /// Включить/выключить регенерацию
    /// </summary>
    /// <param name="enabled"></param>
    void SetEnabledRegeneration(bool enabled);

    /// <summary>
    /// Добавить слушателя события смерти объекта
    /// </summary>
    /// <param name="listener"></param>
    void RegisterKilledListener(Action listener);

    /// <summary>
    /// Удалить слушателя события смерти объекта
    /// </summary>
    /// <param name="listener"></param>
    void UnregisterKilledListener(Action listener);
}
