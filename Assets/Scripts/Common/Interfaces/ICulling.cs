﻿using UnityEngine;
/// <summary>
/// Интерфейс объектов, которые деактивируются вне поля зрения игровой камеры
/// </summary>
public interface ICulling
{
    /// <summary>
    /// Объект, реализующий интерфейс
    /// </summary>
    ICulling CullingObject { get; }
    /// <summary>
    /// Размер объекта
    /// </summary>
    float Size { get; }
    /// <summary>
    /// Активен ли объект в данный момент
    /// </summary>
    bool Active { get; }
    /// <summary>
    /// Получить точку объекта, ближайщую к point
    /// </summary>
    /// <param name="point"></param>
    /// <param name="nearestPoint"></param>
    /// <returns></returns>
    bool GetNearestBodyPoint(Vector3 point, out Vector3 nearestPoint);
    /// <summary>
    /// Активировать/деактивировать объект
    /// </summary>
    /// <param name="active"></param>
    void SetActive(bool active);
}
