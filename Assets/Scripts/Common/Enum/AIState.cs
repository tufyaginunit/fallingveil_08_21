﻿/// <summary>
/// Возможные состояния объекта ИИ конкретно в данной игре
/// </summary>
public enum AIState
{
    NotAssigned,
    Idle,
    Patroling,
    Attacking,
    Searching,
    Retreating,
    Chasing,
    Killed
}
