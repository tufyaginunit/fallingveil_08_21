﻿using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Перечислитель, служащий для обхода некоторой коллекции IEnumerable<T> в случайном порядке.
/// </summary>
/// <typeparam name="T"></typeparam>
public class ShuffleEnumerator<T> where T : class
{
    private static readonly System.Random random = new System.Random();
    
    /// <summary>
    /// Перемешиваемый массив элементов исходной коллекции
    /// </summary>
    private T[] shuffleArray = null;

    /// <summary>
    /// Индекс текущего элемента
    /// </summary>
    private int currentIndex = 0;
    
    public ShuffleEnumerator(IEnumerable<T> enumerable)
    {
        shuffleArray = enumerable.ToArray();
        Shuffle();
    }

    /// <summary>
    /// Указатель на текущий элемент
    /// </summary>
    public T Current => (currentIndex >= 0 && currentIndex < shuffleArray.Length) ?
        shuffleArray[currentIndex] : null;

    /// <summary>
    /// Сдвинуть указатель на следующий элемент коллекции
    /// </summary>
    /// <returns></returns>
    public bool MoveNext()
    {
        currentIndex++;
        return currentIndex < shuffleArray.Length;
    }

    /// <summary>
    /// Сдвинуть указатель на следующий элемент коллекции циклически. 
    /// Если текущий элемент является последним, будет осуществлено очередное перемешивание,
    /// и указатель будет установлен на первый элемент (или на второй, если первый элемент равен текущему,
    /// на который ссылается указатель)
    /// </summary>
    public void MoveNextLoop()
    {
        if (currentIndex < shuffleArray.Length - 1)
        {
            currentIndex++;
        }
        else
        {
            T currentItem = shuffleArray[currentIndex];
            Shuffle();
            currentIndex = 0;
            if (shuffleArray[currentIndex] == currentItem && shuffleArray.Length > 1)
            {
                currentIndex = 1;
            }
        }
    }

    /// <summary>
    /// Сбросить указатель на текущий элемент в 0
    /// </summary>
    public void Reset()
    {
        currentIndex = 0;
    }

    /// <summary>
    /// Перемешать элементы
    /// </summary>
    private void Shuffle()
    {
        T tempItem;
        int randomIndex;

        for (int i = 0; i < shuffleArray.Length; i++)
        {
            randomIndex = random.Next(i, shuffleArray.Length);
            tempItem = shuffleArray[i];
            shuffleArray[i] = shuffleArray[randomIndex];
            shuffleArray[randomIndex] = tempItem;
        }
    }
}
