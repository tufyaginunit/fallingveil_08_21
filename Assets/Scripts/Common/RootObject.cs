﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Игровой объект, перемещающийся в корень иерархии после запуска игры.
/// Некоторые объекты удобно настраивать, когда они вложены в другие, однако
/// если родительский объект движется, это может привести к нежелательным изменениям
/// координат вложенного объекта. Например, к таким объектам относится
/// маршрут патрулирования врага, который удобно настраивать в объекте врага, но
/// координаты точек при этом не должны меняться.
/// Если добавить этот компонент к игровому объекту, он будет перемещён в корень иерархии,
/// что исключит влияние на него родительских объектов.
/// </summary>
public class RootObject : MonoBehaviour
{
    [SerializeField, Tooltip("Имя родительского объекта, в который будет помещён данный объект")]
    private string parentObjectName = "Root Object";

    /// <summary>
    /// Словарь уже созданных родительских объектов.
    /// Используется для того, чтобы исключить дублирование родительских объектов
    /// с одинаковыми именами.
    /// </summary>
    private readonly static Dictionary<string, GameObject> parentObjects =
        new Dictionary<string, GameObject>();

    private void Awake()
    {
        if (!parentObjects.TryGetValue(parentObjectName, out GameObject parentObject))
        {
            parentObject = new GameObject(parentObjectName);
            parentObjects[parentObjectName] = parentObject;
        }
        Transform currentParent = gameObject.transform.parent;
        if (gameObject.transform.parent != null)
        {
            gameObject.name = $"{gameObject.name}_{currentParent.name}";
        }

        gameObject.transform.SetParent(parentObject.transform);
    }
}
