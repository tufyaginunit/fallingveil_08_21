﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Контроллер, отвечающий за деактивацию объектов вне поля зрения камеры
/// </summary>
public class CullingController : MonoBehaviour
{
    /// <summary>
    /// Проверяемые объекты
    /// </summary>
    private ICulling[] _cullingObjects = null;

    /// <summary>
    /// Объект главной камеры
    /// </summary>
    private Camera _mainCamera = null;

    /// <summary>
    /// Текущее положение центра экрана в плоскости земли/игрока
    /// </summary>
    private Vector3 _screenCenterOnGround = Vector3.zero;

    float _screenWidth = 0;
    float _screenHeight = 0;

    private void Awake()
    {
        _cullingObjects = SceneManager.GetActiveScene().GetRootGameObjects()
            .SelectMany(go => go.GetComponentsInChildren<ICulling>(false)).ToArray();

        _mainCamera = GameManager.Instance.MainCamera;
    }

    void Update()
    {
        RefreshCullingAll();
    }

    /// <summary>
    /// Обновить состояние скрываемых объектов
    /// </summary>
    private void RefreshCullingAll()
    {
        Vector3 screenCenter = new Vector3(0.5f * Screen.width, 0.5f * Screen.height,
            _mainCamera.transform.position.y);
        _screenCenterOnGround = GameManager.Instance.GetGroundPositionFromScreen(screenCenter);

        _screenWidth = Screen.width;
        _screenHeight = Screen.height;

        foreach (var item in _cullingObjects)
        {
            RefreshCulling(item);
        }
    }

    /// <summary>
    /// Обновить состояние скрываемого объекта
    /// </summary>
    /// <param name="cullingObject"></param>
    private void RefreshCulling(ICulling cullingObject)
    {
        if (!cullingObject.GetNearestBodyPoint(_screenCenterOnGround, out Vector3 cullingObjectPosition))
        {
            cullingObject.SetActive(true);
            return;
        }

        Vector3 vectorToCenter = _screenCenterOnGround - cullingObjectPosition;
        vectorToCenter.y = 0;

        float distToCenter = vectorToCenter.magnitude;

        float size = cullingObject.Size;

        if (distToCenter < size)
        {
            cullingObject.SetActive(true);
            return;
        }

        Vector3 directionToCenter = vectorToCenter / distToCenter;

        Vector3 checkingPoint = cullingObjectPosition + directionToCenter * size;

        Vector3 checkingScreenPoint = _mainCamera.WorldToScreenPoint(checkingPoint);

        cullingObject.SetActive(checkingScreenPoint.x > 0 && checkingScreenPoint.x < _screenWidth
            && checkingScreenPoint.y > 0 && checkingScreenPoint.y < _screenHeight);
    }
}
