﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Игровой объект, уничтожающий при старте свои неактивные дочерние объекты
/// </summary>
public class DestroyDisabledChildren : MonoBehaviour
{
    private void Awake()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            GameObject child = transform.GetChild(i).gameObject;
            if (!child.activeSelf)
            {
                Destroy(child);
            }
        }
    }
}
