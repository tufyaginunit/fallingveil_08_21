﻿using UnityEngine;

/// <summary>
/// Стратегия изменения видимости. Отвечает за то, как материал будет обрабатывать различные значения видимости
/// </summary>
public abstract class OpacityChangingStrategy: MonoBehaviour, IChangingMaterialStrategy
{
    /// <summary>
    /// Установить значение видимости для материала
    /// </summary>
    /// <param name="material"></param>
    /// <param name="value"></param>
    public abstract void SetOpacity(Material material, float value);
    
    /// <summary>
    /// Установить режим прозрачности для материала
    /// </summary>
    /// <param name="material"></param>
    public virtual void SetFadeMode(Material material)
    {
    }
    /// <summary>
    /// Установить режим непрозрачности для материала
    /// </summary>
    /// <param name="material"></param>
    public virtual void SetOpaqueMode(Material material)
    {
    }

    /// <summary>
    /// Проверить, подходит ли материал для изменения по данной стратегии
    /// </summary>
    /// <param name="material"></param>
    /// <returns></returns>
    public abstract bool IsMaterialMatchStrategy(Material material);

    /// <summary>
    /// Проверить, эквивалентна ли данная стратегия указанной
    /// </summary>
    /// <param name="checkingStrategy"></param>
    /// <returns></returns>
    public abstract bool Equal(OpacityChangingStrategy checkingStrategy);

    /// <summary>
    /// Аналог Lerp. Установить значение от min до max значения свойства, 
    /// на основе переданного значения в диапазоне [0 ; 1]
    /// </summary>
    /// <param name="material"></param>
    /// <param name="value"></param>
    public void SetNormalizedValue(Material material, float value)
    {
        SetOpacity(material, value);
    }
}
