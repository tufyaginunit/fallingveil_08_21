﻿using UnityEngine;

/// <summary>
/// Стратегия изменения видимости материала путём применения эффекта Dithering
/// </summary>
public class DitheringOpacityChangingStrategy : OpacityChangingStrategy
{
    /// <summary>
    /// Текущее значение видимости материала
    /// </summary>
    private float currentOpacityValue;

    /// <summary>
    /// Установить значение видимости для материала
    /// </summary>
    /// <param name="material"></param>
    /// <param name="value"></param>
    public override void SetOpacity(Material material, float value)
    {
        currentOpacityValue = value;
        if (currentOpacityValue > 1)
        {
            currentOpacityValue = 1;
        }
        else if (currentOpacityValue < 0)
        {
            currentOpacityValue = 0;
        }

        material.SetOpacity(currentOpacityValue);
    }

    /// <summary>
    /// Проверить, подходит ли материал для изменения по данной стратегии
    /// </summary>
    /// <param name="material"></param>
    /// <returns></returns>
    public override bool IsMaterialMatchStrategy(Material material)
    {
        return material.IsOpacityChangingMaterial();
    }

    public override bool Equal(OpacityChangingStrategy checkingStrategy)
    {
        return checkingStrategy is OpacityChangingStrategy;
    }
}
