﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс, который изменяет материалы указанного игрового объекта по определённым правилам.
/// Правила изменения задаются в объекте, реализующем IChangingMaterialStrategy
/// </summary>
public class MaterialChangingObject
{   
    /// <summary>
    /// Материалы, которые будут изменяться
    /// </summary>
    private readonly List<Material> materials = new List<Material>();

    /// <summary>
    /// Стратегия изменения материалов
    /// </summary>
    private IChangingMaterialStrategy changingStrategy = null;

    public MaterialChangingObject(Transform model, IChangingMaterialStrategy changingStrategy)
    {
        Renderer[] meshRenderers = model.GetComponentsInChildren<Renderer>();

        // Пары материалов <оригинальный материал; изменяемый материал>. Словарь нужен для того, чтобы
        // не дублировать лишний раз одни и те же материалы
        Dictionary<Material, Material> originalAndChangingMaterials = new Dictionary<Material, Material>();

        foreach (var meshRenderer in meshRenderers)
        {
            Material[] newSharedMaterials = new Material[meshRenderer.sharedMaterials.Length];
            for (int i = 0; i < meshRenderer.sharedMaterials.Length; i++)
            {
                Material material = meshRenderer.sharedMaterials[i];

                if (!changingStrategy.IsMaterialMatchStrategy(material))
                {
                    newSharedMaterials[i] = material;
                    continue;
                }

                if (!originalAndChangingMaterials.TryGetValue(material, out Material changingMaterial))
                {
                    changingMaterial = new Material(material);
                    originalAndChangingMaterials[material] = changingMaterial;
                }
               
                newSharedMaterials[i] = changingMaterial;
            }
            meshRenderer.sharedMaterials = newSharedMaterials;
        }

        this.changingStrategy = changingStrategy;
        foreach (var item in originalAndChangingMaterials)
        {
            materials.Add(item.Value);
        }
    }

    /// <summary>
    /// Установить нормализованное значение параметра материала, заданного в changingStrategy
    /// </summary>
    /// <param name="value"></param>
    public void SetNormalizedValue(float value)
    {
        foreach (var item in materials)
        {
            changingStrategy.SetNormalizedValue(item, value);
        }
    }

}
