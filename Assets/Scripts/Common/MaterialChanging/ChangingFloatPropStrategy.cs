﻿using UnityEngine;

/// <summary>
/// Стратегия изменения float-свойства материала
/// </summary>
public class ChangingFloatPropStrategy : IChangingMaterialStrategy
{
    /// <summary>
    /// Название свойства
    /// </summary>
    private string propertyName;

    /// <summary>
    /// Начальное значение
    /// </summary>
    private float startValue;

    /// <summary>
    /// Конечное значение
    /// </summary>
    private float endValue;

    public ChangingFloatPropStrategy(string propertyName, float startValue, float endValue)
    {
        this.propertyName = propertyName;
        this.startValue = startValue;
        this.endValue = endValue;
    }

    /// <summary>
    /// Соответствует ли материал данной стратегии
    /// </summary>
    /// <param name="material"></param>
    /// <returns></returns>   
    public bool IsMaterialMatchStrategy(Material material)
    {
        return material.HasProperty(propertyName);
    }

    /// <summary>
    /// Аналог Lerp. Установить значение от min до max значения свойства, 
    /// на основе переданного значения в диапазоне [0 ; 1]
    public void SetNormalizedValue(Material material, float normalizedValue)
    {
        material.SetFloat(propertyName, startValue + (endValue - startValue) * normalizedValue);
    }
}
