﻿using UnityEngine;

/// <summary>
/// Стратегия изменения видимости материала путём применения эффекта Dissolve
/// </summary>
public class DissolveOpacityChangingStrategy : OpacityChangingStrategy
{
    [Tooltip("Цвет границы эффекта растворения")]
    [SerializeField]
    [ColorUsageAttribute(true, true)]
    private Color dissolveEdgeColor;

    /// <summary>
    /// Текущее значение видимости материала
    /// </summary>
    private float currentOpacityValue;

    /// <summary>
    /// Установить значение видимости для материала
    /// </summary>
    /// <param name="material"></param>
    /// <param name="value"></param>
    public override void SetOpacity(Material material, float value)
    {
        currentOpacityValue = value;
        if (currentOpacityValue > 1)
        {
            currentOpacityValue = 1;
        }
        else if (currentOpacityValue < 0)
        {
            currentOpacityValue = 0;
        }

        material.SetDissolve(1 - currentOpacityValue, dissolveEdgeColor);
    }

    /// <summary>
    /// Проверить, подходит ли материал для изменения по данной стратегии
    /// </summary>
    /// <param name="material"></param>
    /// <returns></returns>
    public override bool IsMaterialMatchStrategy(Material material)
    {
        return material.IsDissolveMaterial();
    }

    public override bool Equal(OpacityChangingStrategy checkingStrategy)
    {
        if (!(checkingStrategy is DissolveOpacityChangingStrategy))
        {
            return false;
        }

        DissolveOpacityChangingStrategy strategy = (DissolveOpacityChangingStrategy)checkingStrategy;

        return checkingStrategy is DissolveOpacityChangingStrategy &&
            dissolveEdgeColor == strategy.dissolveEdgeColor;
    }
}
