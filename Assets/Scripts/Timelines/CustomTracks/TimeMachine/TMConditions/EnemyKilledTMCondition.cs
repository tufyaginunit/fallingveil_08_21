﻿using UnityEngine;
using System.Linq;

/// <summary>
/// Условие TimeMachine трека. Проверяет, были ли убит первый враг / все враги
/// </summary>
public class EnemyKilledTMCondition : TimeMachineCondition
{
    public enum Mode
    {
        First,
        All
    }

    [SerializeField]
    private Mode mode;

    public override bool IsSatisfied()
    {
        if (mode == Mode.First)
        {
            return EnemyManager.Instance.GetDefeatedEnemyCount() == 1;
        }
        else
        {
            return EnemyManager.Instance.GetDefeatedEnemyCount() == EnemyManager.Instance.Enemies.Count();
        }
    }
}
