﻿using UnityEngine;

/// <summary>
/// Условие TimeMachine трека. Проверяет, есть ли враги в указанном радиусе от игрока
/// </summary>
public class EnemyInRangeTMCondition : TMCondPolled
{
	[SerializeField]
	private float range = 5;

    [SerializeField]
    private bool cameraAiming = false;

    [SerializeField]
    private Cinemachine.CinemachineVirtualCamera cameraToAim = null;

    protected override bool EvaluateCondition()
    {
		float rangeSqr = range * range;
        IDamageable nearestEnemy = null;
		foreach (var enemy in EnemyManager.Instance.Enemies)
        {
            if (VectorFunc.GetSqrDistanceXZ(GameManager.Instance.Player.Transform.position,
				enemy.Transform.position) < rangeSqr)
            {
                nearestEnemy = enemy;
                break;
            } 
        }

        if (nearestEnemy != null)
        {
            if (cameraAiming)
            {
                cameraToAim.Follow = nearestEnemy.Transform;
            }
            return true;
        }

        return false;
    }

}
