﻿using UnityEngine;

/// <summary>
/// Условие TimeMachine трека. Проверяет, выполняется ли сейчас игроком атака (процесс удара)
/// </summary>
public class WeaponHitPerformingTMCondition : TimeMachineCondition
{
	public override bool IsSatisfied()
	{
		return GameManager.Instance.PlayerWeapon.IsHitPerforming();
	}
}
