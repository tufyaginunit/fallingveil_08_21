﻿using UnityEngine;

/// <summary>
/// Условие TimeMachine трека. Проверяет, находится ли уровень видимости в заданном значении
/// </summary>
public class OpacityValueTMCondition : TimeMachineCondition
{
	[SerializeField]
	private float targetOpacityValue = 0;

	public override bool IsSatisfied()
	{
		return GameManager.Instance.Controller.OpacityController.OpacityValue == targetOpacityValue;

	}
}
