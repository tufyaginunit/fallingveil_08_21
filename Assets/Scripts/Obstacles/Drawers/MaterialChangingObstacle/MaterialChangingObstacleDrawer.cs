using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����������, ��������������� ��������� ������ ��������� ��������.
/// ��������� ������� ��������� �������� ���� ��������� ���������� ��������� 
/// (��������, ����������� ��� ��������� ������ ������������).
/// ����������� ��� �������� ������ � OpacityController, 
/// ������� ������ ����� ��������� ��������� ������������ MaterialChangingStrategy
/// </summary>
public class MaterialChangingObstacleDrawer : ChangingObstacleDrawer
{   
    /// <summary>
    /// ��������� ��� ��������� ������ ���������
    /// </summary>
    private readonly List<OpacityChangingMaterial> opacityChangingMaterials = new List<OpacityChangingMaterial>();

    /// <summary>
    /// ��������� ��������� ���������
    /// </summary>
    private OpacityChangingStrategy[] materialChangingStrategies;

    /// <summary>
    /// �������� ��������� ������� �� �������� ������ ���������
    /// </summary>
    protected override void Draw(float opacityValue)
    {
        foreach (var item in opacityChangingMaterials)
        {
            item.SetOpacityValueInPercents(opacityValue);
        }
    }

    protected override void Awake()
    {
        base.Awake();

        materialChangingStrategies = GetComponents<OpacityChangingStrategy>();

        if (materialChangingStrategies.Length == 0)
        {
            //Debug.LogWarning($"{name} - Missing obstacle material changing strategy");
            return;
        }
    }

    private void Start()
    {
        InitializeMaterials();
    }

    /// <summary>
    /// ���������������� ���������, ������� ����� ������������ ���������
    /// </summary>
    private void InitializeMaterials()
    {
        // ��� ����������� ������������ �������� sharedMaterials ��������� �����.
        // ��������� ���������� �� sharedMaterials ���� � ��������� ���������� � �������.
        // ����� ����� �� �����������, ����� ��������� ����� ���������� � �������� � ����.

        // ����� ����� ������ ���� ��������� ��������� ����������, ����� �� ���������� ������ ���
        // ��������� � ����������� �����������
        // � ������ ������ � ������ ��� ���������, ����������� � ���������� ����������
        List<OpacityChangingStrategy> strategies = new List<OpacityChangingStrategy>();
        foreach (var item in materialChangingStrategies)
        {
            strategies.Add(item);
        }

        foreach (var obstacle in ObstacleManager.Instance.ChangingObstacles)
        {
            Renderer[] meshRenderers = obstacle.Transform.GetComponentsInChildren<Renderer>();

            foreach (var meshRenderer in meshRenderers)
            {
                OpacityChangingStrategy localStrategy = 
                    meshRenderer.GetComponentInParent<OpacityChangingStrategy>();

                Material[] newSharedMaterials = new Material[meshRenderer.sharedMaterials.Length];
                for (int i = 0; i < meshRenderer.sharedMaterials.Length; i++)
                {
                    Material material = meshRenderer.sharedMaterials[i];

                    if (!TryChooseChangingStrategyForMaterial(material, localStrategy, strategies,
                        out OpacityChangingStrategy strategy))
                    {
                        // �������� �� ����� ��������, ���� �� ������������� ��������� ���������.
                        // �� ����� ��������� ����� �������� � ������ ������������                        
                        newSharedMaterials[i] = material;
                        continue;
                    }
                    
                    if (!strategies.Contains(strategy))
                    {
                        strategies.Add(strategy);
                    }

                    OpacityChangingMaterial opacityChangingMaterial = opacityChangingMaterials
                        .Find(m => m.InitialMaterial == material && m.OpacityChangingStrategy == strategy &&
                        m.InvertedChange == obstacle.InvertedChange);

                    if (opacityChangingMaterial == null)
                    {
                        opacityChangingMaterial = new OpacityChangingMaterial(material, strategy, 
                            obstacle.InvertedChange);
                        opacityChangingMaterials.Add(opacityChangingMaterial);
                    }
                    newSharedMaterials[i] = opacityChangingMaterial.ChangingMaterial;
                }
                meshRenderer.sharedMaterials = newSharedMaterials;
            }
        }

    }

    /// <summary>
    /// ���������� ����� ����� �� ��������� ��������� ��������� � ������
    /// </summary>
    /// <param name="strategy"></param>
    /// <param name="strategies"></param>
    /// <param name="foundStrategy">��������� ���������</param>
    /// <returns></returns>
    private bool TryFindTheSameChangingStrategy(OpacityChangingStrategy strategy, 
        List<OpacityChangingStrategy> strategies, out OpacityChangingStrategy foundStrategy)
    {
        foundStrategy = null;

        foreach (var item in strategies)
        {
            if (strategy.Equal(item))
            {
                foundStrategy = item;
            }
        }

        return foundStrategy != null;
    }

    /// <summary>
    /// ���������� ����� ��������� ��������� ������������, ���������� ���������
    /// </summary>
    /// <param name="material"></param>
    /// <param name="localStrategy"></param>
    /// <param name="strategies"></param>
    /// <param name="chosenStrategy"></param>
    /// <returns></returns>
    private bool TryChooseChangingStrategyForMaterial(Material material, 
        OpacityChangingStrategy localStrategy, List<OpacityChangingStrategy> strategies,
        out OpacityChangingStrategy chosenStrategy)
    {
        chosenStrategy = null;

        if (localStrategy != null && localStrategy.IsMaterialMatchStrategy(material))
        {
            TryFindTheSameChangingStrategy(localStrategy, strategies, out chosenStrategy);           
            if (chosenStrategy == null)
            {
                chosenStrategy = localStrategy;
            }
            return true;
        }

        foreach (var strategy in materialChangingStrategies)
        {
            if (strategy.IsMaterialMatchStrategy(material))
            {
                chosenStrategy = strategy;
                break;
            }
        }

        return chosenStrategy != null;
    }
}
