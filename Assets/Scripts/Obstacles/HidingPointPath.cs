﻿using UnityEngine;
/// <summary>
/// Структура, хранящая параметры маршрута к точке укрытия за каким-либо препятствием
/// </summary>
public struct HidingPointPath
{
    /// <summary>
    /// Точка укрытия
    /// </summary>
    public ObstacleHidingPoint point;
    
    /// <summary>
    /// Длина маршрута до точки
    /// </summary>
    public float pathLength;

    /// <summary>
    /// Маршрут до точки (угловые точки NavMesh)
    /// </summary>
    public Vector3[] pathPoints;

    /// <summary>
    /// Конечная точка маршрута по NavMesh
    /// </summary>
    public Vector3 PathFinishPoint => pathPoints[pathPoints.Length - 1];

    public HidingPointPath(ObstacleHidingPoint point, Vector3[] pathPoints)
    {
        this.point = point;
        this.pathPoints = pathPoints;
        this.pathLength = VectorFunc.GetPathLength(pathPoints);
    }
}
