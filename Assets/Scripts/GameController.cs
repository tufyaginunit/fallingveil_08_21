﻿using System.Collections;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

/// <summary>
/// Контроллер, управляющий главными подсистемами игры
/// Отвечает за реакцию игры на глобальные события (например, показ соответствующего экрана при смерти игрока).
/// </summary>
public class GameController : MonoBehaviour
{
    [SerializeField, Tooltip("Задержка перед авторестартом игры после смерти игрока (десятые доли сек.)")]
    private int delayBeforeRestartInDeciseconds = 5;

    [SerializeField, Tooltip("Объект управления игрока")]
    private PlayerInput playerInput = null;

    [SerializeField, Tooltip("Обработчик нажатия клавиш")]
    private KeyListener keyListener = null;

    [SerializeField, Tooltip("Контроллер курсора")]
    private CursorController cursorController = null;

    [SerializeField, Tooltip("Контроллер игровых экранов")]
    private ScreenController screenController = null;

    [SerializeField, Tooltip("Контроллер чекпоинтов")]
    private CheckPointController checkPointController = null;

    [SerializeField, Tooltip("Контроллер видимости изменяемых объетков")]
    private OpacityController opacityController = null;

    [SerializeField, Tooltip("Контроллер катсцен")]
    private CutSceneController cutSceneController = null;

    [SerializeField, Tooltip("Контроллер обучения")]
    private TutorialController tutorialController = null;
   
    [SerializeField, Tooltip("Контроллер звукового сопровождения")]
    private GameSoundController gameSoundController = null;

    /// <summary>
    /// Таймлиния, находящаяся в данный момент на паузе
    /// </summary>
    private PlayableDirector pausedTimeline = null;

    /// <summary>
    /// Корутина, выполняющая действия рестарта
    /// </summary>
    private Coroutine restartCoroutine = null;

    /// <summary>
    /// Поставлена ли игра на паузу
    /// </summary>
    public bool GamePaused { get; private set; }

    public OpacityController OpacityController => opacityController;

    public CursorController CursorController => cursorController;

    public GameSoundController SoundController => gameSoundController;

    public ScreenController ScreenController => screenController;

    public PlayerInput PlayerInput => playerInput;

    /// <summary>
    /// Включить/выключить отображение курсора
    /// </summary>
    /// <param name="enabled"></param>
    public void SetCursorEnabled(bool enabled)
    {
        cursorController.SetCursorEnabled(enabled);
    }

    /// <summary>
    /// Установить режим игры
    /// </summary>
    /// <param name="enabled">true - геймплей с разрешённым управлением персонажем, false - катсцены, меню</param>
    public void SetGamePlayEnabled(bool enabled)
    {
        if (enabled)
        {
            SetGamePlayControlMode();
        }
        else
        {
            SetCutsceneControlMode();
        }
    }

    /// <summary>
    /// Прервать текущую катсцену
    /// </summary>
    public void InterruptCurrentCutscene()
    {
        cutSceneController.InterruptCurrentCutscene();
    }

    /// <summary>
    /// Поставить указанную таймлинию на паузу
    /// </summary>
    /// <param name="timeline"></param>
    public void PauseTimeline(PlayableDirector timeline)
    {
        pausedTimeline = timeline;
        pausedTimeline.playableGraph.GetRootPlayable(0).SetSpeed(0);
    }

    /// <summary>
    /// Возобновить проигрывание таймлинии, находящейся на паузе
    /// </summary>
    public void ResumeTimeline()
    {
        if (pausedTimeline == null)
        {
            return;
        }
        pausedTimeline.playableGraph.GetRootPlayable(0).SetSpeed(1);
        pausedTimeline = null;
    }

    /// <summary>
    /// Установить разрешённые действия по управлению персонажем
    /// </summary>
    /// <param name="input"></param>
    public void SetAllowedInput(AllowedInput input)
    {
        playerInput.SetAllowedInput(input);
        OnInputChanged();
    }

    /// <summary>
    /// Добавить разрешённые действия персонажа
    /// </summary>
    /// <param name="input"></param>
    public void AddAllowedInput(AllowedInput input)
    {
        playerInput.AddAllowedInput(input);
        OnInputChanged();
    }

    /// <summary>
    /// Выйти из игры
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Запустить/перезапустить главную сцену игры
    /// </summary>
    public void ReloadScene()
    {
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Начать новую игру
    /// </summary>
    public void StartNewGame()
    {
        cutSceneController.PlayNewGameCutscene();
        cutSceneController.CurrentCutscene.RegisterTimeLineStoppedListener((t) => OnNewGameStarted());

        if (!StoredGameDataManager.IntroOptions.GetPlayIntroOption())
        {
            cutSceneController.CurrentCutscene.RequestInterruption();
        }
    }

    /// <summary>
    /// Обработать событие конца игры (после убийства последнего врага)
    /// </summary>
    public void FinishGame()
    {
        gameSoundController.StopGamePlayMusic();
        gameSoundController.StopBackgroundSound();

        cutSceneController.PlayEndGameCutscene();
        cutSceneController.CurrentCutscene.RegisterTimeLineStoppedListener((t) => ReloadScene());
    }

    /// <summary>
    /// Перезапустить игру с последней контрольной точки
    /// </summary>
    /// <param name="delay">Задержка перед затемнением экрана</param>
    public void RunFromLastCheckPoint(float delay = 0)
    {
        if (GamePaused)
        {
            return;
        }
        if (restartCoroutine != null)
        {
            return;
        }

        restartCoroutine = StartCoroutine(PerformRestart(delay));
    }

    /// <summary>
    /// Поставить игру на паузу
    /// </summary>
    public void PauseGame()
    {
        screenController.ShowPauseScreen();
        Time.timeScale = 0;
        GamePaused = true;
    }

    /// <summary>
    /// Снять с паузы
    /// </summary>
    public void ResumeGame()
    {
        screenController.HideCurrentScreen();
        Time.timeScale = 1;
        GamePaused = false;

        OnInputChanged();
    }

    /// <summary>
    /// Выполнить действия проигрыша игрока
    /// </summary>
    public void SetGameOver()
    {
        RunFromLastCheckPoint(0.1f * delayBeforeRestartInDeciseconds);
    }

    private void Start()
    {
        if (cutSceneController.IsEnabled)
        {
            cutSceneController.PlayMainMenuCutscene();
        }
        else
        {
            OnNewGameStarted();
        }      
    }

    /// <summary>
    /// Установить управление игрока в режим катсцены
    /// </summary>
    private void SetCutsceneControlMode()
    {
        FieldOfViewManager.Instance.SetEnemiesFovVisible(false);
        playerInput.SetEnabled(false);
        keyListener.SetMode(KeyListener.ListenerMode.Cutscene);
    }

    /// <summary>
    /// Установить управление игрока в обычный режим игры
    /// </summary>
    private void SetGamePlayControlMode()
    {
        FieldOfViewManager.Instance.SetEnemiesFovVisible(true);
        playerInput.SetEnabled(true);
        keyListener.SetMode(KeyListener.ListenerMode.Gameplay);
    }

    /// <summary>
    /// Обработать событие начала игры (после начальной заставки)
    /// </summary>
    private void OnNewGameStarted()
    {
        if (tutorialController.IsEnabled &&
            StoredGameDataManager.IntroOptions.GetPlayTutorialOption())
        {
            tutorialController.Play();
        }

        gameSoundController.PlayGamePlayMusic();
        gameSoundController.PlayBackgroundSound();

        OnInputChanged();
    }

    /// <summary>
    /// Корутина, выполняющая действия рестарта (затемнение экрана, перезагрузка параметров)
    /// </summary>
    /// <param name="delay">Задержка перед затемнением экрана</param>
    /// <returns></returns>
    private IEnumerator PerformRestart(float delay)
    {
        SetGamePlayEnabled(false);
        
        yield return new WaitForSeconds(delay);

        screenController.ShowBlackScreen();
        
        yield return new WaitForSeconds(0.5f);

        opacityController.SetEnabled(false);

        checkPointController.LoadLastCheckPointState();

        opacityController.SetDefaultState();
        opacityController.SetEnabled(true);

        // Ждём дополнительно один кадр, т.к. иначе вначале будет видно старое положение
        // персонажа 
        yield return null;

        screenController.HideCurrentScreen();

        SetGamePlayEnabled(true);

        restartCoroutine = null;
    }

    /// <summary>
    /// Обработчик события изменения разрешённых действий персонажа
    /// </summary>
    private void OnInputChanged()
    {
    }
}
