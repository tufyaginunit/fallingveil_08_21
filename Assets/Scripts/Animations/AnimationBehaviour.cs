﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Базовый абстрактный класс для анимирования объекта
/// </summary>
public abstract class AnimationBehaviour : MonoBehaviour, ICulling
{
    /// <summary>
    /// Время плавного включения/выключения слоёв (Layers) аниматора
    /// </summary>
    protected const float TransitionTimeBetweenLayers = 0.2f;
    
    /// <summary>
    /// Время сглаживания значения параметра при плавном изменении
    /// </summary>
    protected const float ParameterChangeValueDampTime = 0.5f;
    
    /// <summary>
    /// Разница между текущим и целевым значением параметра, меньше которой считаем, что значения равны
    /// </summary>    
    protected const float ParameterChangeValueAccuracy = 0.01f;

    [SerializeField]
    [Tooltip("Аниматор, которым управляем")]
    protected Animator animator;

    [SerializeField]
    [Tooltip("Движущийся объект, на основе которого выполняются анимации движения")]
    protected MovingObject movingObject;

    [SerializeField]
    [Tooltip("Объект здоровья, на основе которого выполняются анимации повреждения и смерти")]
    protected DamageableObject damageableObject;

    [SerializeField]
    [Tooltip("Контроллер оружия, на основе которого выполняются анимации атаки")]
    protected WeaponController weapon;

    [SerializeField]
    [Tooltip("Максимальная скорость движущегося объекта (нужна для синхронизации " +
        "анимации ног с реальной скоростью движения)")]
    protected float MaxVelocityForMovementBlendTree = 20f;

    private Coroutine settingLayerWeightCoroutine = null;

    public ICulling CullingObject => this;

    float ICulling.Size => 2;

    bool ICulling.Active => animator.enabled;

    void ICulling.SetActive(bool active)
    {
        animator.enabled = active;
    }

    bool ICulling.GetNearestBodyPoint(Vector3 point, out Vector3 nearestPoint)
    {
        nearestPoint = movingObject.Position;
        return true;
    }

    protected virtual void Start()
    {
        // Рассчитываем множитель скорости анимации, в зависимости от скорости персонажа
        if (IsParamaterExists(AnimatorConstants.Parameters.VelocityMultiplier))
        {
            animator.SetFloat(AnimatorConstants.Parameters.VelocityMultiplier, 
                movingObject.MaxSpeed / MaxVelocityForMovementBlendTree);
        }

        // Подписываемся на события объектов, на который должен реагировать аниматор:
        // начало атаки, конец атаки, смерть
        if (weapon != null)
        {
            weapon.RegisterAttackStartedListener(AnimateAttack);
            weapon.RegisterAttackEndedListener(AnimateStopAttack);
        }

        if (damageableObject != null)
        {
            damageableObject.RegisterKilledListener(AnimateDeath);
            damageableObject.RegisterRevivedListener(AnimateRevive);
        }

        movingObject.RegisterMovingModeChangedListener(OnMovingModeChanged);
        OnMovingModeChanged();
    }

    protected virtual void OnDestroy()
    {
        if (weapon != null)
        {
            weapon.UnregisterAttackStartedListener(AnimateAttack);
            weapon.UnregisterAttackEndedListener(AnimateStopAttack);
        }
        if (damageableObject != null)
        {
            damageableObject.UnregisterKilledListener(AnimateDeath);
            damageableObject.UnregisterRevivedListener(AnimateRevive);
        }

        if (movingObject != null)
        {
            movingObject.UnregisterMovingModeChangedListener(OnMovingModeChanged);
        }
        //if (patrolStrategy != null)
        //{
        //    patrolStrategy.UnregisterRoutePatrolingStartedListener(AnimateStartPatroling);
        //    patrolStrategy.UnregisterRoutePatrolingInterruptedListener(AnimateInterruptPatroling);
        //}
    }

    /// <summary>
    /// Задать значение параметра float (плавное изменение)
    /// </summary>
    /// <param name="paramName">Имя параметра</param>
    /// <param name="paramValue">Целевое значение параметра</param>
    protected void SetParameterValueSmooth(string paramName, float paramValue)
    {
        if (animator == null || !IsParamaterExists(paramName))
        {
            return;
        }

        float currentValue = animator.GetFloat(paramName);
        int nearestIntValue = Mathf.RoundToInt(currentValue);
        if (Mathf.Abs(currentValue - nearestIntValue) < ParameterChangeValueAccuracy
             && paramValue == nearestIntValue)
        {
            animator.SetFloat(paramName, nearestIntValue);
        }
        else
        {
            animator.SetFloat(paramName, paramValue, ParameterChangeValueDampTime, Time.deltaTime);
        }
    }

    /// <summary>
    /// Задать значение параметра float (резкое изменение)
    /// </summary>
    /// <param name="paramName">Имя параметра</param>
    /// <param name="paramValue">Значение параметра</param>
    protected void SetParameterValue(string paramName, float paramValue)
    {
        if (animator == null || !IsParamaterExists(paramName))
        {
            return;
        }

        animator.SetFloat(paramName, paramValue);
    }

    /// <summary>
    /// Задать значение параметра bool
    /// </summary>
    /// <param name="paramName">Имя параметра</param>
    /// <param name="paramValue">Значение параметра</param>
    protected void SetParameterValue(string paramName, bool paramValue)
    {
        if (animator == null || !IsParamaterExists(paramName))
        {
            return;
        }

        animator.SetBool(paramName, paramValue);
    }

    /// <summary>
    /// Задействовать триггер
    /// </summary>
    /// <param name="triggerName">Имя триггера</param>
    protected void SetTrigger(string triggerName)
    {
        if (animator == null || !IsParamaterExists(triggerName))
        {
            return;
        }

        animator.SetTrigger(triggerName);
    }

    /// <summary>
    /// Включить слой аниматора
    /// </summary>
    /// <param name="layerName">Имя слоя</param>
    /// <param name="enabled">Включить или выключить</param>
    protected void SetEnabledLayer(string layerName, bool enabled)
    {
        StartCoroutine(PerformSetEnabledLayer(layerName, enabled));
    }

    /// <summary>
    /// Корутина, выполняющая плавное увеличение/уменьшение веса слоя аниматора
    /// </summary>
    /// <param name="layerName">Имя слоя</param>
    /// <param name="enabled">Включить или выключить</param>
    /// <returns></returns>
    protected IEnumerator PerformSetEnabledLayer(string layerName, bool enabled)
    {
        if (animator == null || !IsLayerExists(layerName))
        {
            yield break;
        }

        float targetValue = enabled ? 1 : 0;

        int layerIndex = animator.GetLayerIndex(layerName);

        float currentTransitionTime = 0;
        while (currentTransitionTime < TransitionTimeBetweenLayers)
        {
            float currentValue = currentTransitionTime / TransitionTimeBetweenLayers;
            if (!enabled)
            {
                currentValue = 1 - currentValue;
            }
            animator.SetLayerWeight(layerIndex, currentValue);
            yield return null;
            currentTransitionTime += Time.deltaTime;
        }
        animator.SetLayerWeight(layerIndex, targetValue);
    }

    protected void SetLayerWeight(string layerName, float weight)
    {
        if (settingLayerWeightCoroutine != null)
        {
            StopCoroutine(settingLayerWeightCoroutine);
        }
        settingLayerWeightCoroutine = StartCoroutine(PerformSetLayerWeight(layerName, weight));
    }

    /// <summary>
    /// Корутина, выполняющая плавное увеличение/уменьшение веса слоя аниматора
    /// </summary>
    /// <param name="layerName">Имя слоя</param>
    /// <param name="enabled">Включить или выключить</param>
    /// <returns></returns>
    protected IEnumerator PerformSetLayerWeight(string layerName, float targetWeight)
    {
        if (animator == null || !IsLayerExists(layerName))
        {
            settingLayerWeightCoroutine = null;
            yield break;
        }

        int layerIndex = animator.GetLayerIndex(layerName);

        float currentWeight = animator.GetLayerWeight(layerIndex);

        if (targetWeight == currentWeight)
        {
            settingLayerWeightCoroutine = null;
            yield break;
        }

        int direction = targetWeight > currentWeight ? 1 : -1;

        float currentTransitionTime = 0;
        while (currentTransitionTime < TransitionTimeBetweenLayers)
        {
            yield return null;
            currentTransitionTime += Time.deltaTime;
            currentWeight += direction * currentTransitionTime / TransitionTimeBetweenLayers;
            if (direction > 0) 
            {
                if (currentWeight >= targetWeight)
                {
                    break;
                }
            }
            else
            {
                if (currentWeight <= targetWeight)
                {
                    break;
                }
            }
            animator.SetLayerWeight(layerIndex, currentWeight);          
        }
        animator.SetLayerWeight(layerIndex, targetWeight);

        settingLayerWeightCoroutine = null;
    }

    /// <summary>
    /// Проверить, существует ли параметр в аниматоре
    /// </summary>
    /// <param name="paramName">Имя параметра</param>
    /// <returns></returns>
    protected bool IsParamaterExists(string paramName)
    {
        for (int i = 0; i < animator.parameterCount; i++)
        {
            if (animator.parameters[i].name == paramName)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Проверить, существует ли слой в аниматоре
    /// </summary>
    /// <param name="paramName">Имя слоя</param>
    /// <returns></returns>
    protected bool IsLayerExists(string paramName)
    {
        for (int i = 0; i < animator.layerCount; i++)
        {
            if (animator.GetLayerName(i) == paramName)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Анимировать движение 
    /// (определяется в наследниках, которые используют это действие)
    /// </summary>
    protected virtual void AnimateMove()
    {
    }

    /// <summary>
    /// Анимировать атаку 
    /// (определяется в наследниках, которые используют это действие)
    /// </summary>
    protected virtual void AnimateAttack()
    {
    }

    /// <summary>
    /// Анимировать остановку атаки 
    /// (определяется в наследниках, которые используют это действие)
    /// </summary>
    protected virtual void AnimateStopAttack()
    {
    }

    /// <summary>
    /// Анимировать смерть
    /// </summary>
    protected virtual void AnimateDeath()
    {
        SetTrigger(AnimatorConstants.Parameters.DeathTrigger);
    }

    /// <summary>
    /// Анимировать воскрешение
    /// </summary>
    protected virtual void AnimateRevive()
    {
        SetTrigger(AnimatorConstants.Parameters.ReviveTrigger);
    }

    protected void OnMovingModeChanged()
    {
        if (movingObject.MovingMode == MovingMode.Ground)
        {
            SetLayerWeight(AnimatorConstants.Layers.Flying, 0);
            SetParameterValue(AnimatorConstants.Parameters.Flying, false);
        }
        else
        {
            SetLayerWeight(AnimatorConstants.Layers.Flying, 0.6f);
            SetParameterValue(AnimatorConstants.Parameters.Flying, true);
        }
    }

    private void Update()
    {
        // Анимация движения выполняется постоянно (в зависимости от текущей скорости объекта)
        AnimateMove();
    }
}


