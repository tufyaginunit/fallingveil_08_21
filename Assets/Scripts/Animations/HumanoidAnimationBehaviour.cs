using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����� ��� ��������� �������� ������������ ���������
/// </summary>
public class HumanoidAnimationBehaviour : AnimationBehaviour
{
    /// <summary>
    /// ����������� �������� 
    /// </summary>
    protected override void AnimateMove()
    {
        if (animator == null)
        {
            return;
        }

        Vector3 velocityVector = movingObject.CurrentVelocity;
        Vector3 movementDirection = velocityVector.normalized;

        // ����� ���������� ��������, ��������������� ����������� �������� ���������
        int turnDirection = movingObject.GetTurnDirection();
        SetParameterValueSmooth(AnimatorConstants.Parameters.TurnValue, turnDirection);
        
        // ����� �������� �������� � ��������� � ������� [-1; 1] (�������� ��� ����������
        // �������� �����-�����, �����-������), � ����������� �� ����������� ��������
        float velocityZ = 0;
        float velocityX = 0;
        if (movementDirection != Vector3.zero)
        {
            velocityZ = Vector3.Dot(velocityVector / movingObject.MaxSpeed, transform.forward);
            velocityX = Vector3.Dot(velocityVector / movingObject.MaxSpeed, transform.right);
        }

        SetParameterValue(AnimatorConstants.Parameters.ForwardVelocity, velocityZ);
        SetParameterValue(AnimatorConstants.Parameters.SideVelocity, velocityX);

        if (!IsParamaterExists(AnimatorConstants.Parameters.Moving))
        {
            return;
        }

        // ������������� ���� �������� � true, ���� �������� ���������, false - � ��������� ������
        bool moving = animator.GetBool(AnimatorConstants.Parameters.Moving);
        if (moving && movementDirection == Vector3.zero
            || !moving && movementDirection != Vector3.zero)
        {
            animator.SetBool(AnimatorConstants.Parameters.Moving, movementDirection != Vector3.zero);
        }
    }

    /// <summary>
    /// ����������� �����
    /// </summary>
    protected override void AnimateAttack()
    {
        SetAttackModeEnabled(true);
    }

    /// <summary>
    /// ����������� ��������� �����
    /// </summary>
    protected override void AnimateStopAttack()
    {
        SetAttackModeEnabled(false);
    }

    /// <summary>
    /// ���������� ���� ������ ���.
    /// </summary>
    /// <param name="enabled">true - �������� � ������ ���, false - �������� �� � ������ ���</param>
    private void SetAttackModeEnabled(bool enabled)
    {
        SetParameterValue(AnimatorConstants.Parameters.Attacking, enabled);
        SetEnabledLayer(AnimatorConstants.Layers.UpperBody, enabled);
    }
}

