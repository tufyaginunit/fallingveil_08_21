﻿/// <summary>
/// Параметры изменения видимости объектов.
/// Задаются для препятствий, которые способны менять свой уровень видимости
/// </summary>
public class OpacityChangingParameters
{
    /// <summary>
    /// Значение видимости, при которой объект считается полностью непрозрачным
    /// </summary>
    public float FullOpaqueValue { get; private set; }

    /// <summary>
    /// Значение видимости, при которой через объект можно стрелять
    /// </summary>
    public float AttackValue { get; private set; }

    /// <summary>
    /// Конструктор параметров изменения видимости
    /// </summary>
    /// <param name="fullOpaqueValue">Значение видимости, при которой объект считается полностью непрозрачным</param>
    /// <param name="attackValue">Значение видимости, при которой через объект можно стрелять</param>
    public OpacityChangingParameters(float fullOpaqueValue, float attackValue)
    {
        FullOpaqueValue = fullOpaqueValue;
        AttackValue = attackValue;
    }
}
