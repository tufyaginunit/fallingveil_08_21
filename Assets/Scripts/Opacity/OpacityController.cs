﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Контроллер для управления уровнем видимости всех объектов на сцене, способных менять свою прозрачность
/// Добавляется на произвольный игровой объект.
/// Рекомендуется добавляеть на пустой дочерний объект объекта GameManager
/// </summary>
public class OpacityController : MonoBehaviour
{
    /// <summary>
    /// Минимальный уровень видимости объектов
    /// </summary>    
    public const float MinOpacityValue = 0;
    /// <summary>
    /// Максимальный уровень видимости объектов
    /// </summary>
    public const float MaxOpacityValue = 100;
    /// <summary>
    /// Шаг изменения прозрачности
    /// </summary>
    private const float OpacityChangeStep = 0.5f;
    /// <summary>
    /// Минимальное смешение персонажа для того, чтобы начала меняться прозрачность
    /// </summary>
    private const float MinPositionDifferenceToChangeOpacity = 0.1f;

    [Tooltip("Объект, указывающий направление увеличения уровня видимости")]
    [SerializeField]
    private Transform increaseVector = null;

    [Tooltip("Объект-инициатор изменения уровня видимости")]
    [SerializeField]
    private Transform changeInitiator = null;

    [Tooltip("Скорость изменения видимости объектов (в процентах на метр движения)")]
    [SerializeField]
    private float changeSpeed = 45;

    [Tooltip("Значение видимости, при которой объект появляется полностью")]
    [SerializeField]
    [Range(MinOpacityValue, MaxOpacityValue)]
    private float fullFadeInValue = 99;

    [Tooltip("Значение видимости, при которой объект полностью исчезает")]
    [SerializeField]
    [Range(MinOpacityValue, MaxOpacityValue)]
    private float fullFadeOutValue = 1;

    [Tooltip("Значение видимости, выше которого объект считается полностью непрозрачным")]
    [SerializeField]
    [Range(MinOpacityValue, MaxOpacityValue)]
    private float fullOpaqueValue = 99;

    [Tooltip("Значение видимости, ниже которого через препятствие можно стрелять")]
    [SerializeField]
    [Range(MinOpacityValue, MaxOpacityValue)]
    private float attackValue = 50;

    /// <summary>
    /// Текущий уровень видимости
    /// </summary>
    private float currentOpacityValue = 0;

    /// <summary>
    /// Предыдущий уровень видимости
    /// </summary>
    private float previousAppliedOpacityValue = 0;

    /// <summary>
    /// Текущая позиция объекта-инициатора вдоль вектора увеличения уровня прозрачности
    /// </summary>
    private float currentPositionAlongIncreaseVector = 0;
    /// <summary>
    /// Последнее применнённое значение видимости для объектов
    /// </summary>
    private float lastAppliedOpacityValue = 0;
    /// <summary>
    /// Скорость изменения прозрачности, не зависящая от значений opacityValueForFullFadeIn и opacityValueForFullFadeOut
    /// </summary>
    private float opacityFullFadeChangeSpeed = 0;
    /// <summary>
    /// Накопленное значение изменения позиции
    /// </summary>
    private float accumulatedPositionDifference = 0;

    /// <summary>
    /// Вектор увеличения уровня видимости
    /// </summary>
    public Vector3 OpacityIncreaseVector { get; private set; }

    /// <summary>
    /// Текущий уровень видимости
    /// </summary>
    public float OpacityValue => lastAppliedOpacityValue;

    /// <summary>
    /// Предыдущий уровень видимости
    /// </summary>
    public float PreviousOpacityValue => previousAppliedOpacityValue;

    /// <summary>
    /// Событие, вызываемое при изменении значения видимости
    /// </summary>
    private event Action OpacityChanged;

    /// <summary>
    /// Добавить слушателя события изменения значения видимости
    /// </summary>
    /// <param name="listener"></param>
    public void RegisterOpacityChangedListener(Action listener)
    {
        OpacityChanged += listener;
    }

    /// <summary>
    /// Удалить слушателя события изменения значения видимости
    /// </summary>
    /// <param name="listener"></param>
    public void UnregisterOpacityChangedListener(Action listener)
    {
        OpacityChanged -= listener;
    }

    /// <summary>
    /// Включить/выключить изменение видимости объектов
    /// </summary>
    /// <param name="enabled"></param>
    public void SetEnabled(bool enabled)
    {
        this.enabled = enabled;
    }

    /// <summary>
    /// Установить состояние видимости объектов по умолчанию
    /// </summary>
    public void SetDefaultState()
    {
        SetOpacity(MaxOpacityValue);
    }

    /// <summary>
    /// Установить уровень видимости объектов
    /// </summary>
    /// <param name="opacityValue"></param>
    public void SetOpacity(float opacityValue)
    {
        currentPositionAlongIncreaseVector = GetCurrentPositionAlongIncreaseVector();
        currentOpacityValue = opacityValue;
        previousAppliedOpacityValue = currentOpacityValue;
        lastAppliedOpacityValue = currentOpacityValue;
        accumulatedPositionDifference = 0;
        RefreshVisibilityValueForObjects();
    }

    private void Awake()
    {
        OpacityIncreaseVector = increaseVector.forward;
        // Выключаем объект, указывающий вектор увеличения видимости объектов, т.к. в игре он не нужен
        increaseVector.gameObject.SetActive(false);

        // Преобразуем скорость изменения видимости так, чтобы она не зависела от значений opacityValueForFullFadeIn
        // и opacityValueForFullFadeOut
        opacityFullFadeChangeSpeed = changeSpeed *
            (fullFadeInValue - fullFadeOutValue) / (MaxOpacityValue - MinOpacityValue);
    }

    private void Start()
    {
        // Задаём всем изменяющимся препятствиям одни и те же параметры изменения прозрачности
        OpacityChangingParameters opacityChangingParameters =
            new OpacityChangingParameters(fullOpaqueValue, attackValue);
        foreach (var item in ObstacleManager.Instance.ChangingObstacles)
        {
            item.Initialize(opacityChangingParameters);
        }

        SetDefaultState();
    }

    private void Update()
    {
        float newPositionAlongIncreaseVector = GetCurrentPositionAlongIncreaseVector();
        float positionDifference = newPositionAlongIncreaseVector - currentPositionAlongIncreaseVector;
        currentPositionAlongIncreaseVector = newPositionAlongIncreaseVector;

        accumulatedPositionDifference += positionDifference;

        // Если достигли максимального значения видимости и продолжаем двигаться в направлении увеличения видимости
        // (и наоборот), то сбрасываем накопленное значение разницы позиций, чтобы начать менять
        // значение видимости именно в момент начала движения в противоположном направлении
        if (accumulatedPositionDifference > 0 && lastAppliedOpacityValue >= MaxOpacityValue
            || accumulatedPositionDifference < 0 && lastAppliedOpacityValue <= MinOpacityValue)
        {
            accumulatedPositionDifference = 0;
            return;
        }

        // Если текущее значение видимости = минимальному или максимальному,
        // видимость начинает меняться не сразу, а при определенном накопленном количестве движения
        if ((lastAppliedOpacityValue >= MaxOpacityValue
            || lastAppliedOpacityValue <= MinOpacityValue)
            && Mathf.Abs(accumulatedPositionDifference) < MinPositionDifferenceToChangeOpacity)
        {
            return;
        }

        accumulatedPositionDifference = 0;

        // При возрастании координаты, прозрачность увеличивается, и наоборот.
        float newOpacityValue = currentOpacityValue + positionDifference * opacityFullFadeChangeSpeed;

        // Уровень видимости будет меняться в интервале [opacityValueForFullFadeIn, opacityValueForFullFadeOut]
        // При достижении границ интервала будем устанавливать полную видимость или полную невидимость 
        // Если начинаем движение в противоположном направлении, сразу начинаем менять прозрачность в этом интервале
        if (newOpacityValue > currentOpacityValue)
        {
            if (newOpacityValue < fullFadeOutValue)
            {
                newOpacityValue = fullFadeOutValue;
            }
            else if (newOpacityValue > fullFadeInValue)
            {
                newOpacityValue = MaxOpacityValue;
            }
        }
        else if (newOpacityValue < currentOpacityValue)
        {
            if (newOpacityValue > fullFadeInValue)
            {
                newOpacityValue = fullFadeInValue;
            }
            else if (newOpacityValue < fullFadeOutValue)
            {
                newOpacityValue = MinOpacityValue;
            }
        }

        currentOpacityValue = newOpacityValue;
        // Для изменения прозрачности разница между текущим и новым уровнем прозрачности 
        // должна быть больше установленного шага
        if (Mathf.Abs(lastAppliedOpacityValue - currentOpacityValue) >= OpacityChangeStep)
        {
            previousAppliedOpacityValue = lastAppliedOpacityValue;

            currentOpacityValue = Mathf.Round(currentOpacityValue / OpacityChangeStep) * OpacityChangeStep;

            lastAppliedOpacityValue = currentOpacityValue;
            RefreshVisibilityValueForObjects();
        }

        currentPositionAlongIncreaseVector = newPositionAlongIncreaseVector;
    }

    /// <summary>
    /// Обновить уровень видимости игровых объектов
    /// </summary>
    private void RefreshVisibilityValueForObjects()
    {
        OpacityChanged?.Invoke();
        foreach (var item in ObstacleManager.Instance.ChangingObstacles)
        {
            item.SetOpacityValue(currentOpacityValue);
        }
    }

    /// <summary>
    /// Получить текущую позицию объекта-инициатора вдоль вектора увеличения уровня видимости
    /// </summary>
    /// <returns></returns>
    private float GetCurrentPositionAlongIncreaseVector()
    {
        return Vector3.Dot(changeInitiator.transform.position, OpacityIncreaseVector);
    }
}
