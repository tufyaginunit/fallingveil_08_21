﻿/// <summary>
/// Калькулятор урона атакующего луча игрока, использующий множители
/// </summary>
public class HealthDependentPlayerWeaponDamageCalculator
{
    private float damageMultiplier = 1.6f;
    private int minHealthForDamageIncrease = 20;

    public HealthDependentPlayerWeaponDamageCalculator(float damageMultiplier, 
        int minHealthForDamageIncrease)
    {
        this.damageMultiplier = damageMultiplier;
        this.minHealthForDamageIncrease = minHealthForDamageIncrease;
    }

    /// <summary>
    /// Получить минимальное значение HP, необходимое для победы над указанным врагом
    /// (используется множитель урона, зависящий от здоровья игрока)
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public int GetEnoughHealthToKill(IDamageable target)
    {
        // Алгоритм: урон по врагу зависит от количества HP игрока. Поэтому необходимое количество HP для убийства
        // рассчитываем в цикле, суммируя урон, начиная с урона при минимальном значения HP, и так до тех пор,
        // пока не исчерпаем здоровье врага. Значение HP, при котором HP врага стало нулевым и будет тем значением
        // HP, которое необходимо для убийства.
        // Начинаем со значения HP = 2, т.к. это минимальное значение HP, при котором можно стрелять.
        int selfHealth = 2;
        int targetHealth = target.Health;
        while (targetHealth > 0)
        {
            targetHealth -= CalculateTargetDamage(1, selfHealth);
            selfHealth += CalculateSelfDamage(1);
        }

        return selfHealth;
    }

    /// <summary>
    /// Рассчитать урон с учётом базового значения урона, текущего здоровья игрока
    /// и множителя
    /// </summary>
    /// <param name="damageValue">Базовое значение урона</param>
    /// <returns></returns>
    public int CalculateTargetDamage(int damageValue, int selfHealth)
    {
        int healthMultiplier = selfHealth >= minHealthForDamageIncrease ?
            selfHealth : minHealthForDamageIncrease;

        int resultDamage = (int)(damageValue * 100 / healthMultiplier
            * (damageMultiplier - (100 - healthMultiplier) / 200));

        if (resultDamage <= 0)
        {
            resultDamage = 1;
        }

        return resultDamage;
    }

    /// <summary>
    /// Рассчитать, сколько урона будет нанесено самому игроку при указанном значении базового урона
    /// </summary>
    /// <param name="damageValue"></param>
    /// <returns></returns>
    public int CalculateSelfDamage(int damageValue)
    {
        return damageValue;
    }
}
