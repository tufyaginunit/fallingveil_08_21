﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Атакующий луч, повреждающий владельца
/// </summary>
public class SelfDamageAttackBeam : AttackBeam
{
    [SerializeField,Tooltip("Повреждаемый объект, которому будет наноситься урон " +
        "одновременно с нанесением урона врагу ")]
    private DamageableObject healthObject;

    [SerializeField, Tooltip("Значение урона в секунду себе")]
    private float selfDamagePerSecond = 10;

    [SerializeField, Tooltip("Восстанавливать здоровье полностью после убийства")]
    private bool restoreAllHPAfterKill = false;

    [SerializeField, Tooltip("Прерывать луч после успешной атаки врага")]
    private bool interruptWhenHitted = false;

    /// <summary>
    /// Накопленное значение урона себе за текущую атаку
    /// </summary>
    private float accumulatedSelfDamage = 0;

    /// <summary>
    /// Применённое значение урона себе за текущую атаку
    /// </summary>
    private int appliedSelfDamage = 0;

    private float relativeDpsKoef = 0;

    /// <summary>
    /// Словарь, содержащий пары <враг, потраченное_здоровье>. 
    /// Служит для восстановления ровно такого количество HP игрока при убийстве врага, которое было 
    /// на него потрачено
    /// </summary>
    private readonly Dictionary<IDamageable, int> spentHealthDictionary = new Dictionary<IDamageable, int>();

    /// <summary>
    /// Начать атаку
    /// </summary>
    public override void StartAttack()
    {
        base.StartAttack();
        healthObject.SetEnabledRegeneration(false);
    }

    /// <summary>
    /// Завершить атаку
    /// </summary>
    public override void StopAttack()
    {
        base.StopAttack();
        healthObject.SetEnabledRegeneration(true);
    }

    /// <summary>
    /// Получить минимальное значение HP, необходимое для победы над указанным врагом
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public int GetEnoughHealthToKill(IDamageable target)
    {
        return Mathf.CeilToInt(target.Health * relativeDpsKoef);
    }

    protected override void Awake()
    {
        base.Awake();

        healthObject.RegisterHittedListener(OnHitted);
        healthObject.RegisterRevivedListener(OnRevived);

        relativeDpsKoef = selfDamagePerSecond / damagePerSecond;
    }

    /// <summary>
    /// Проверить, можно ли продолжать атаку
    /// </summary>
    /// <returns></returns>
    protected override bool AreHitConditionsSatisfied()
    {
        return healthObject.Health > 1;
    }

    private void OnDestroy()
    {
        healthObject.UnregisterRevivedListener(OnRevived);
    }

    /// <summary>
    /// Пополнить накопленное значение урона за указанный промежуток времени 
    /// </summary>
    /// <param name="deltaTime"></param>
    protected override void AccumulateDamage(float deltaTime)
    {
        base.AccumulateDamage(deltaTime);

        accumulatedSelfDamage += selfDamagePerSecond * deltaTime;
    }

    /// <summary>
    /// Сделать попытку применить накопленное значение урона
    /// </summary>
    protected override void TryApplyAccumulatedDamage()
    {
        if (!AreHitConditionsSatisfied())
        {
            return;
        }

        base.TryApplyAccumulatedDamage();

        float notAppliedDamage = accumulatedSelfDamage - appliedSelfDamage;
        int selfDamage = 0;
        if (notAppliedDamage >= 1)
        {
            selfDamage = (int)(notAppliedDamage);
            if (selfDamage >= healthObject.Health)
            {
                selfDamage = healthObject.Health - 1;
            }

            if (selfDamage > 0)
            {
                healthObject.ReceiveDamage(selfDamage);
                appliedSelfDamage += selfDamage;
            }
        }

        // Если цели нет, то наносим урон только себе
        if (target == null)
        {
            return;
        }

        // Если есть урон, который только что нанесли себе, атакуя врага, добавляем его в словарь
        // для последующего восстановления HP после убийства данного врага
        if (selfDamage > 0)
        {
            int spentHealthSum = selfDamage;
            if (spentHealthDictionary.TryGetValue(target, out int previousSpentHealthSum))
            {
                spentHealthSum += previousSpentHealthSum;
            }

            spentHealthDictionary[target] = spentHealthSum;
        }      

        // Восстаналиваем HP, если только что убили врага
        if (!target.IsAlive())
        {
            if (restoreAllHPAfterKill)
            {
                healthObject.SetStartHealth();
            }
            else
            {
                healthObject.AddHealth(spentHealthDictionary[target]);
            }

            spentHealthDictionary.Remove(target);

            healthObject.HealEffect();
        }
    }

    /// <summary>
    /// Сбросить значение переменных, отвечающих за накопленный урон
    /// </summary>
    protected override void ResetDamage()
    {
        base.ResetDamage();

        accumulatedSelfDamage = 0;
        appliedSelfDamage = 0;
    }

    /// <summary>
    /// Обработчик события возрождения с контрольной точки.
    /// Служит для того, чтобы при перезапуске уровня очистить словарь 
    /// потраченного на врагов значения HP
    /// </summary>
    private void OnRevived()
    {
        spentHealthDictionary.Clear();
    }

    /// <summary>
    /// Обработчик события получения урона
    /// </summary>
    private void OnHitted()
    {
        if (!interruptWhenHitted)
        {
            return;
        }
        
        // Прерываем атаку, если только что получили урон
        StopAttack();
    }
}
