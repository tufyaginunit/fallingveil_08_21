﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Препятствие, которое создаёт луч для врагов.
/// Требует наличия капсульного коллайдера, которым будут отталкиваться враги.
/// </summary>
[RequireComponent(typeof(CapsuleCollider))]
public class BeamObstacle : MonoBehaviour
{
    [SerializeField, Tooltip("Включён или выключен коллайдер отталкивания врагов")]
    protected bool isEnabled = true;

    [SerializeField, Tooltip("Коллайдер, отталкивающий врагов")]
    private CapsuleCollider beamCollider = null;

    /// <summary>
    /// Включить/выключить препятствие луча
    /// </summary>
    /// <param name="active"></param>
    public void SetActive(bool active)
    {
        if (!isEnabled && active)
        {
            return;
        }

        if (gameObject.activeSelf != active)
        {
            gameObject.SetActive(active);
        }       
    }

    /// <summary>
    /// Установить начальную и конечную точки луча
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="endPoint"></param>
    public void SetPosition(Vector3 startPoint, Vector3 endPoint)
    {
        if (!isEnabled)
        {
            return;
        }
      
        startPoint.y = endPoint.y;
        
        float dist = Vector3.Distance(startPoint, endPoint);

        // Если враг слишком близко, не включаем коллайдер, чтобы избежать его дёрганий
        if (dist < 2 * beamCollider.radius)
        {
            beamCollider.enabled = false;
            return;
        }

        Vector3 direction = (endPoint - startPoint) / dist;

        // Делаем сдвиг коллайдера ближе к персонажу, чтобы конец коллайдера не упирался во врага,
        // иначе он всё время будет отскакивать от луча.
        startPoint -= 2 * beamCollider.radius * direction;
        transform.position = startPoint + direction * dist / 2;
        transform.forward = direction;

        beamCollider.height = dist;

        beamCollider.enabled = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.TryGetComponent(out AIMovingObject movingObject))
        {
            Vector3 objPosition = movingObject.Position;
            objPosition.y = transform.position.y;

            // Выталкиваем врага из луча туда, куда ближе
            Vector3 vectorToObject = objPosition - transform.position;
            vectorToObject.y = 0;
            Vector3 pushDirection = VectorFunc.GetRotatedUnitVector(transform.forward, 90);
            
            if (Vector3.Dot(vectorToObject, pushDirection) < 0)
            {
                pushDirection = -pushDirection;
            }

            float checkDistance = beamCollider.radius * 2;
            if (other.GetType() == typeof(CapsuleCollider))
            {
                checkDistance = (other as CapsuleCollider).radius + beamCollider.radius;
            }

            Vector3 pointOnBeam = VectorFunc.GetProjectionOnDirection(vectorToObject, transform.forward);

            Vector3 pushVector = pushDirection * (checkDistance - Vector3.Distance(pointOnBeam, vectorToObject));
            // Если сдвинуть врага в намеченную точку невозможно, в любом случае сдвигаем его
            // в противоположную сторону
            if (!movingObject.CalculatePathToPoint(objPosition + pushVector, out _))
            {
                pushVector = -pushVector;
            }

            movingObject.JumpToPoint(objPosition + pushVector);
        }
    }
}
