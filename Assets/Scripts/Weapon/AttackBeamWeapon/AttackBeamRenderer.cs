﻿using UnityEngine;

/// <summary>
/// Отрисовщик, отвечающий за визуальное отображение атакующего луча
/// </summary>
public abstract class AttackBeamRenderer : MonoBehaviour
{
    /// <summary>
    /// Задаем луч из AttackBeam
    /// </summary>
    protected AttackBeam beam;  

    /// <summary>
    /// Инициализация поля beam
    /// </summary>
    /// <param name="beam"></param>
    public virtual void Initialize(AttackBeam beam)
    {
        this.beam = beam;
    }
    /// <summary>
    /// Включить, выключить отрисовку луча
    /// </summary>
    /// <param name="active"></param>
    public virtual void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }

    /// <summary>
    /// Установить начальную и конечную точки луча
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="endPoint"></param>
    public abstract void SetPosition(Vector3 startPoint, Vector3 endPoint);

    /// <summary>
    /// Установить цвет луча
    /// </summary>
    /// <param name="color"></param>
    public virtual void SetColor(Color color)
    {
    }
}
