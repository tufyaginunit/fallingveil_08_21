﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Контроллер оружия, стреляющего лучом
/// </summary>
public class BeamWeaponController : WeaponController
{
    [SerializeField]
    [Tooltip("Атакующий луч")]
    public AttackBeam attackBeam = null;

    /// <summary>
    /// Установить цель атаки лучом
    /// </summary>
    /// <param name="target"></param>
    public override void SetTarget(IDamageable target)
    {
        attackBeam.SetTarget(target);
    }

    /// <summary>
    /// Получить цель
    /// </summary>
    /// <returns></returns>
    public override IDamageable GetTarget()
    {
        return attackBeam.GetTarget();
    }

    /// <summary>
    /// Установить состояние контроллера оружия по умолчанию
    /// </summary>
    public override void SetDefaultState()
    {
        base.SetDefaultState();
        attackBeam.StopAttack();
    }

    /// <summary>
    /// Выполнить удар. В данном случае удар будет растянут во времени
    /// пока луч не будет прерван
    /// </summary>
    protected override void Hit()
    {
        isHitPerforming = true;
        StartCoroutine(PerformHit());
    }

    protected override void Awake()
    {
        base.Awake();
        attackBeam.Initialize(attackPoint, attackDistance);   
    }  

    /// <summary>
    /// Корутина выполнения удара
    /// </summary>
    /// <returns></returns>
    private IEnumerator PerformHit()
    {
        attackBeam.StartAttack();
        while (true)
        {
            yield return null;
            if (IsInterruptionRequested || !attackBeam.IsAttacking())
            {
                attackBeam.StopAttack();
                break;
            }
        }

        isHitPerforming = false;
    }
}
