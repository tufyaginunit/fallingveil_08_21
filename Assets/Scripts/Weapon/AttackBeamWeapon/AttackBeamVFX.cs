﻿using System.Collections;
using UnityEngine;
using System;
using UnityEngine.VFX;
using System.Linq;

/// <summary>
/// Класс, использующий визуальный эффект для отображения атакующего луча
/// </summary>
public class AttackBeamVFX : AttackBeamRenderer
{
    [SerializeField]
    [Tooltip("Визуальный эффект VFX Graph")]
    private VisualEffect visualEffect;

    [SerializeField]
    [Tooltip("Дистанция утоньшения луча (через сколько метров после максимальной дистанции атаки луч" +
        "будет иметь минимальную толщину)")]
    private float thinningDistance = 3;

    public override void Initialize(AttackBeam beam)
    {
        base.Initialize(beam);
        if (visualEffect.HasFloat("MaxDistance"))
        {
            visualEffect.SetFloat("MaxDistance", beam.MaxRendererDistance + thinningDistance);
        }

        if (visualEffect.HasFloat("StartInterruptDistance"))
        {
            visualEffect.SetFloat("StartInterruptDistance", thinningDistance);
        }

    }

    /// <summary>
    /// Установить цвет луча
    /// </summary>
    /// <param name="color"></param>
    public override void SetColor(Color color)
    {
        if (visualEffect.HasVector4("Color"))
        {
            visualEffect.SetVector4("Color", color);
        }
    }

    /// <summary>
    /// Установить начальную и конечную точку луча и частоту мигания луча
    /// </summary>
    /// <param name="startPoint"></param>
    /// <param name="endPoint"></param>
    public override void SetPosition(Vector3 startPoint, Vector3 endPoint)
    {
        float currentBeamLength = Vector3.Distance(startPoint, endPoint);
        
        visualEffect.SetFloat("Distance", currentBeamLength);
        visualEffect.SetBool("Colliding", beam.CurrentRaycastHit.collider != null);
        transform.position = startPoint;
        transform.LookAt(endPoint);
    }
}
    
