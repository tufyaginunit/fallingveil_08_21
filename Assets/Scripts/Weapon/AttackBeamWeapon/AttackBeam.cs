﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Абстрактный класс оружия, представляющего собой атакующий луч
/// Используется в BeamWeaponController, который управляет процессом атаки этим оружием
/// </summary>
public abstract class AttackBeam : MonoBehaviour
{
    /// <summary>
    /// Максимально возможная длина луча
    /// </summary>
    private const float MaxBeamLength = 100;

    [SerializeField]
    [Tooltip("Значение урона в секунду")]
    protected float damagePerSecond = 20;

    [SerializeField]
    [Tooltip("Отрисовщик, отвечающий за визуальное отображение атакующего луча")]
    protected AttackBeamRenderer beamRenderer = null;

    [SerializeField]
    [Tooltip("Визуальная точка атаки, в которой будет начинаться луч " +
        "(может не совпадать с фактической точкой атаки)")]
    protected Transform visualAttackPoint = null;

    [SerializeField]
    [Tooltip("Препятствие, которое создаёт луч для врагов")]
    protected BeamObstacle beamObstacle = null;

    /// <summary>
    /// Текущая цель, по которой наносится урон
    /// </summary>
    protected IDamageable target = null;

    /// <summary>
    /// Максимальная длина луча (фактическая). Отсчитывается от фактической точки атаки и
    /// используется для определения попадания по цели
    /// </summary>
    protected float maxDistance = 0;

    /// <summary>
    /// Текущая длина луча
    /// </summary>
    protected float currentDistance = 0;

    /// <summary>
    /// Максимальная длина луча для отрисовщика. Отсчитывается от визуальной точки атаки и
    /// используется для корректной отлисовки луча
    /// </summary>
    protected float maxRendererDistance = 0;

    /// <summary>
    /// Фактическая точка атаки
    /// </summary>
    protected Transform attackPoint = null;

    /// <summary>
    /// Параметры последнего рэйкаста
    /// </summary>
    protected RaycastHit currentRaycastHit;
    
    /// <summary>
    /// Выполняется ли атака по цели
    /// </summary>
    protected bool isAttacking = false;

    /// <summary>
    /// Накопленное значение урона за текущую атаку
    /// </summary>
    protected float accumulatedDamage = 0;

    /// <summary>
    /// Применённое значение урона за текущую атаку
    /// </summary>
    protected int appliedDamage = 0;

    /// <summary>
    /// Максимальная длина луча (фактическая)
    /// </summary>
    public float MaxDistance => maxDistance;

    /// <summary>
    /// Максимальная длина луча для отрисовщика
    /// </summary>
    public float MaxRendererDistance => maxRendererDistance;

    /// <summary>
    /// Текущая длина луча
    /// </summary>
    public float CurrentDistance => currentDistance;

    /// <summary>
    /// Параметры последнего рэйкаста
    /// </summary>
    public RaycastHit CurrentRaycastHit => currentRaycastHit;

    /// <summary>
    /// Событие, возникающее в момент, когда враг оборвал луч о препятствие
    /// </summary>
    private event Action HasInterrupted = null;

    /// <summary>
    /// Добавить слушателя события обрыва луча о препятствие
    /// </summary>
    /// <param name="listener"></param>
    public void RegisterHasInterruptedListener(Action listener)
    {
        HasInterrupted += listener;
    }

    /// <summary>
    /// Удалить слушателя события обрыва луча о препятствие
    /// </summary>
    /// <param name="listener"></param>
    public void UnregisterHasInterruptedListener(Action listener)
    {
        HasInterrupted -= listener;
    }

    /// <summary>
    /// Инициализировать атакующий луч.
    /// Необходимо вызывать перед StartAttack()
    /// </summary>
    /// <param name="attackPoint">Фактическая точка атаки</param>
    /// <param name="maxDistance">Максимальная дистанция атаки</param>
    public virtual void Initialize(Transform attackPoint, float maxDistance)
    {
        this.attackPoint = attackPoint;
        this.maxDistance = maxDistance;
        maxRendererDistance = maxDistance - VectorFunc.GetDistanceXZ(attackPoint.position, visualAttackPoint.position);
        beamRenderer.Initialize(this);
    }

    /// <summary>
    /// Установить цель для атаки
    /// </summary>
    /// <param name="target"></param>
    public virtual void SetTarget(IDamageable target)
    {
        this.target = target;
    }

    /// <summary>
    /// Получить текущую цель атаки
    /// </summary>
    /// <returns></returns>
    public virtual IDamageable GetTarget()
    {
        return target;
    }

    /// <summary>
    /// Проверить, выполняется ли атака
    /// </summary>
    /// <returns></returns>
    public virtual bool IsAttacking()
    {
        return isAttacking;
    }

    /// <summary>
    /// Начать атаку
    /// </summary>
    public virtual void StartAttack()
    {
        isAttacking = true;
        StartCoroutine(Hitting());
    }

    /// <summary>
    /// Остановить атаку
    /// </summary>
    public virtual void StopAttack()
    {
        StopAllCoroutines();
        isAttacking = false;
        SetActiveBeam(false);
        target = null;
    }

    protected virtual void Awake()
    {
        SetActiveBeam(false);
    }

    /// <summary>
    /// Проверить, можно ли продолжать атаку
    /// </summary>
    /// <returns></returns>
    protected abstract bool AreHitConditionsSatisfied();

    /// <summary>
    /// Пополнить накопленное значение урона за указанный промежуток времени
    /// (Поскольку здоровье измеряется в целых единицах, не будем наносить урон каждый кадр
    /// т.к. при значении меньше 1, будет наносится 0 урона.
    /// Будем копить урон каждый кадр и наносить только тогда, когда значение будет больше 1.)
    /// </summary>
    /// <param name="deltaTime"></param>
    protected virtual void AccumulateDamage(float deltaTime)
    {
        accumulatedDamage += damagePerSecond * deltaTime;
    }

    /// <summary>
    /// Сделать попытку применить накопленное значение урона
    /// </summary>
    protected virtual void TryApplyAccumulatedDamage()
    {
        if (target == null || !AreHitConditionsSatisfied())
        {
            return;
        }

        float notAppliedDamage = accumulatedDamage - appliedDamage;
        if (notAppliedDamage < 1)
        {
            return;
        }

        int damageToApply = (int)notAppliedDamage;
        target.ReceiveDamage(damageToApply);
        appliedDamage += damageToApply;
    }

    /// <summary>
    /// Сбросить значение переменных, отвечающих за накопленный урон
    /// </summary>
    protected virtual void ResetDamage()
    {
        accumulatedDamage = 0;
        appliedDamage = 0;
    }

    /// <summary>
    /// Корутина, выполяющая длительное повреждение врага лучом
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator Hitting()
    {
        ResetDamage();

        Coroutine raycastCoroutine = StartCoroutine(RaycastRefreshing());

        // Делаем пропуск кадра, чтобы успели обновиться все объекты, анализирующие
        // состояние атаки
        yield return new WaitForFixedUpdate();

        bool hittingTarget = false;
        while (AreHitConditionsSatisfied())
        {
            RefreshBeamPosition();

            yield return new WaitForFixedUpdate();

            AccumulateDamage(Time.fixedDeltaTime);
            TryApplyAccumulatedDamage();

            if (target != null)
            {
                // Принудительно прерываем атаку, если только что уничтожили атакуемую цель
                if (!target.IsAlive())
                {
                    break;
                }

                hittingTarget = true;
            }
            else if (hittingTarget)
            {
                // Принудительно прерываем атаку, если цель, по которой вели атаку, скрылась
                HasInterrupted?.Invoke();
                break;
            }
        }
        StopCoroutine(raycastCoroutine);

        StopAttack();
    }

    /// <summary>
    /// Активировать/деактивировать луч
    /// </summary>
    /// <param name="active"></param>
    protected void SetActiveBeam(bool active)
    {
        beamRenderer.SetActive(active);
        beamObstacle.SetActive(active);
    }

    /// <summary>
    /// Обновить позицию луча
    /// </summary>
    protected void RefreshBeamPosition()
    {
        Vector3 direction = attackPoint.forward;

        Vector3 startPoint = attackPoint.position;
        Vector3 endPoint = GetEndPoint(startPoint, direction, maxDistance);
        currentDistance = Vector3.Distance(startPoint, endPoint);

        startPoint = visualAttackPoint.position;
        beamRenderer.SetPosition(startPoint, endPoint);
        beamObstacle.SetPosition(startPoint, endPoint);
        SetActiveBeam(true);
    }

    /// <summary>
    /// Обновить параметры рэйкаста луча, пытаясь обнаружить цель
    /// </summary>
    protected virtual void RefreshTargetRaycast()
    {
        currentRaycastHit = GetAttackRaycast();
        IDamageable newTarget = null;
        if (currentRaycastHit.collider != null)
        {
            if (target != null || currentRaycastHit.distance < maxDistance)
            {
                newTarget = currentRaycastHit.collider.GetComponent<IDamageable>();
            }         
        }
        target = newTarget;
    }

    /// <summary>
    /// Корутина, выполняющая обновление рэйкаста луча
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator RaycastRefreshing()
    {       
        while (true)
        {
            RefreshTargetRaycast();
            yield return new WaitForFixedUpdate();
        }
    }

    /// <summary>
    /// Получить конечную точку луча
    /// </summary>
    /// <returns></returns>
    protected virtual Vector3 GetEndPoint(Vector3 startPoint, Vector3 direction, float maxLength)
    {
        if (currentRaycastHit.collider == null)
        {
            return startPoint + direction * maxLength;
        }
        else
        {
            return currentRaycastHit.point;
        }
    }

    /// <summary>
    /// Получить рэйкаст атакующего луча в текущем направлении атаки
    /// </summary>
    /// <returns></returns>
    protected virtual RaycastHit GetAttackRaycast()
    {
        Vector3 direction = attackPoint.forward;
        float distance = maxDistance;

        // Если уже ведётся огонь по цели, доворачиваем луч к ней.
        // Также в этом случае не ограничиваем длину луча (его можно будет прервать только убив врага
        // или зайдя за угол, но не разорвав дистанцию с ним)
        if (target != null)
        {
            direction = target.Transform.position - attackPoint.position;
            direction.y = 0;
            distance = MaxBeamLength;
        }

        // Если на пути луча есть препятствие, то ограничиваем максимальную длину луча расстоянием до
        // препятствия
        if (Physics.Raycast(attackPoint.position, direction, out RaycastHit obstacleHitInfo, distance,
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore))
        {
            distance = obstacleHitInfo.distance;
        }

        // Пытаемся продолжать атаку по текущей цели, обновляем расстояние до неё
        Ray ray = new Ray(attackPoint.position, direction);
        if (target != null &&
            currentRaycastHit.collider != null && 
            currentRaycastHit.collider.transform == target.Transform &&
            currentRaycastHit.collider.Raycast(ray, out RaycastHit hitInfo, distance))
        {
            return hitInfo;
        }

        // Цели нет (или не удалось продолжить атаку по ней). Пытаемся найти цель
        if (Physics.Raycast(attackPoint.position, direction, out hitInfo, distance,
            GameManager.Instance.EnemyLayers, QueryTriggerInteraction.Ignore))
        {
            return hitInfo;
        }

        // Цель не нашли, возвращаем информацию о найденном препятствии на пути луча
        return obstacleHitInfo;
    }
}
