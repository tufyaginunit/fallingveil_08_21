﻿using UnityEngine;

/// <summary>
/// Данные полёта пули
/// </summary>
public struct BulletEmitData
{
    public BulletEmitData(Vector3 startPosition, Quaternion startRotation, float startSpeed,
        float damage, IDamageable target, float maxFlightRange)
    {
        StartPosition = startPosition;
        StartRotation = startRotation;
        Damage = damage;
        StartSpeed = startSpeed;
        Target = target;
        MaxFlightRange = maxFlightRange;
    }

    public Vector3 StartPosition { get; private set; }
    public Quaternion StartRotation { get; private set; }
    public float StartSpeed { get; private set; }
    public float Damage { get; private set; }
    public IDamageable Target { get; private set; }
    public float MaxFlightRange { get; private set; }
}
