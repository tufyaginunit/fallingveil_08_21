﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Управление поведением пули
/// </summary>
public class Bullet : MonoBehaviour
{
    [SerializeField, Tooltip("Тело пули")]
    private BulletBody bulletBody;

    [SerializeField, Tooltip("Начальная скорость движения пули")]
    private float bulletStartSpeed;

    [SerializeField, Tooltip("Текущее здоровье врага делится на это число")]
    private int defaultDamageDevisor = 5;

    [SerializeField, Tooltip("Минимальный урон")]
    private int minDamageValue = 10;

    [SerializeField, Tooltip("Максимальная дальность полёта")]
    private int maxFlightRange = 1000;

    /// <summary>
    /// Значение урона текущей выпущенной пули
    /// </summary>
    private int currentDamageValue;

    /// <summary>
    /// Текущий владелец пули
    /// </summary>
    private GameObject owner;

    /// <summary>
    /// Корутина, отсчитывающая время, когда пуля будет готова к использованию
    /// </summary>
    private Coroutine busyCoroutine = null;

    /// <summary>
    /// Значение глобального времени, до которого невозможно повторно использовать пулю
    /// </summary>
    private float canUseTime = 0;

    /// <summary>
    /// Значение урона, приведённое к диапазону [0;1]
    /// </summary>
    public float NormalizedDamageValue { get; private set; }

    #region События
    /// <summary>
    /// Событие, генерируемое непосредственно перед тем, как пуля будет выпущена
    /// </summary>
    private event Action BeforeEmit;

    /// <summary>
    /// Событие, генерируемое перед уничтожением пули, которая ни во что не врезалась
    /// </summary>
    private event Action BeforeDestroyWithoutHit;

    /// <summary>
    /// Событие, генерируемое в момент попадания пули по цели
    /// </summary>
    private event Action HittedTarget;

    /// <summary>
    /// Событие, генерируемое в момент попадания пули в стену
    /// </summary>
    private event Action HittedWall;

    public void RegisterOnBeforeEmitListener(Action listener)
    {
        BeforeEmit += listener;
    }

    public void UnregisterOnBeforeEmitListener(Action listener)
    {
        BeforeEmit -= listener;
    }

    public void RegisterBeforeDestroyWithoutHitListener(Action listener)
    {
        BeforeDestroyWithoutHit += listener;
    }

    public void UnregisterBeforeDestroyWithoutHitListener(Action listener)
    {
        BeforeDestroyWithoutHit -= listener;
    }

    public void RegisterHittedTargetListener(Action listener)
    {
        HittedTarget += listener;
    }

    public void UnregisterHittedTargetListener(Action listener)
    {
        HittedTarget -= listener;
    }

    public void RegisterHittedWallListener(Action listener)
    {
        HittedWall += listener;
    }

    public void UnregisterHittedWallListener(Action listener)
    {
        HittedWall -= listener;
    }
    #endregion

    /// <summary>
    /// Проверить, можно ли использовать пулю для стрельбы
    /// </summary>
    /// <returns></returns>
    public bool IsReadyToEmit()
    {
        return !bulletBody.IsEnabled() && busyCoroutine == null;
    }

    /// <summary>
    /// Получить коллайдер пули
    /// </summary>
    /// <returns></returns>
    public Collider GetBulletBodyCollider()
    {
        return bulletBody.BulletCollider;
    }

    /// <summary>
    /// Выпустить пулю
    /// </summary>
    /// <param name="owner"></param>
    /// <param name="startPosition"></param>
    /// <param name="rotation"></param>
    /// <param name="velocity"></param>
    /// <param name="targetBulletPoint"></param>
    /// <param name="damageMultiplier"></param>
    public void Emit(GameObject owner, Vector3 startPosition, Quaternion rotation,
        Vector3 velocity, IDamageable target, float damageMultiplier)
    {
        //владелец пули
        this.owner = owner;

        //определяем значение текущего здоровья врага, на основе которого рассчитываем урон пули
        int currentHealth = ConsiderOwnersHealth(owner, out int maxHealth);

        currentDamageValue = (int)(currentHealth / (float)defaultDamageDevisor);
        if (currentDamageValue < minDamageValue)
        {
            currentDamageValue = minDamageValue;
        }

        BulletEmitData bulletBodyData = new BulletEmitData(startPosition, rotation,
            bulletStartSpeed, currentDamageValue, target, maxFlightRange);

        float maxDamageValue = maxHealth / defaultDamageDevisor;
        NormalizedDamageValue = currentDamageValue / maxDamageValue;

        BeforeEmit?.Invoke();
        bulletBody.Emit(bulletBodyData);
    }

    /// <summary>
    /// Установить время, которое пуля будет недоступна для повторного использования
    /// </summary>
    /// <param name="time"></param>
    public void SetBusyTime(float time)
    {
        canUseTime = Time.time + time;
        if (busyCoroutine == null)
        {
            busyCoroutine = StartCoroutine(BusyTimer());
        }
    }

    public void SetDefaultState()
    {
        DisableBullet();
        canUseTime = 0;
    }

    private void Awake()
    {
        bulletBody.Collided += OnBulletBodyCollided;
        bulletBody.MaxDistanceReached += OnBulletBodyMaxDistanceReached;
    }

    private void OnDestroy()
    {
        if (bulletBody != null)
        {
            bulletBody.Collided -= OnBulletBodyCollided;
            bulletBody.MaxDistanceReached -= OnBulletBodyMaxDistanceReached;
        }
    }

    /// <summary>
    /// если в GameObject содержится компонент DamagableObject, то метод получает значение Health этого объекта
    /// </summary>
    /// <param name="owner"></param>
    /// <returns></returns>
    private int ConsiderOwnersHealth(GameObject owner, out int maxHealth)
    {
        if (owner != null)
        {
            if (owner.TryGetComponent(out DamageableObject damageableOwner))
            {
                maxHealth = damageableOwner.MaxHealth;
                return damageableOwner.Health;
            }                
            else
            {
                maxHealth = 1;
                return 1;
            }
        }        
        else
        {
            maxHealth = 1;
            return 1;
        }
    }

    /// <summary>
    /// Обработчик события столкновения тела пули с каким-либо объектом
    /// </summary>
    /// <param name="other"></param>
    private void OnBulletBodyCollided(Collider other)
    {
        // Проверим, нужно ли реагировать на столкновение с коллайдером
        if (!IsReactionOnCollisionNeeded(other))
        {
            return;
        }

        if (other.TryGetComponent(out IDamageable damageableObject))
        {
            damageableObject.ReceiveDamage(currentDamageValue);
            damageableObject.HitEffect();
            HittedTarget?.Invoke();
        }
        else
        {
            HittedWall?.Invoke();
        }

        DisableBullet();
    }

    /// <summary>
    /// Обработчик события достижения телом пули максимальной дистанции полёта
    /// </summary>
    private void OnBulletBodyMaxDistanceReached()
    {
        BeforeDestroyWithoutHit?.Invoke();
        DisableBullet();
    }

    /// <summary>
    /// Проверить, нужно ли реагировать на столкновение с коллайдером
    /// </summary>
    /// <param name="other">Коллайдер объекта, с которым проверяется необходимость реакции</param>
    /// <returns></returns>
    private bool IsReactionOnCollisionNeeded(Collider other)
    {
        // Не будем реагировать, если произошло попадание в себя, в союзников и в прозрачные стены
        return !other.isTrigger &&
            (owner == null || (other.gameObject != owner && other.gameObject.layer != owner.layer));
    }

    /// <summary>
    /// Метод устранения пули
    /// </summary>
    /// <returns></returns>
    private void DisableBullet()
    {
        bulletBody.Disable();
    }

    /// <summary>
    /// Корутина, выполнение которой сигнализирует о невозможности использовать пулю.
    /// </summary>
    /// <returns></returns>
    private IEnumerator BusyTimer()
    {
        while (Time.time < canUseTime)
        {
            yield return null;
        }
        busyCoroutine = null;
    }
}
