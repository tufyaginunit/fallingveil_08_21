﻿using System;
using System.Collections;
using UnityEngine;

/// <summary>
/// Тело пули, движущееся по направлению к цели
/// </summary>
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class BulletBody : MonoBehaviour
{
    /// <summary>
    /// Период проверки превышения дистанции полёта
    /// </summary>
    const float FlightRangeCheckPeriod = 1;
    
    /// <summary>
    /// Данные выпущенной пули
    /// </summary>
    private BulletEmitData bulletEmitData;

    /// <summary>
    /// Событие столкновения с препятствием
    /// </summary>
    public event Action<Collider> Collided;

    /// <summary>
    /// Событие достижения максимальной дистанции полёта
    /// </summary>
    public event Action MaxDistanceReached;

    /// <summary>
    /// Коллайдер пули
    /// </summary>
    public Collider BulletCollider {get; private set;}

    /// <summary>
    /// Квадрат максимальной дистанции полёта (для оптимизации проверки)
    /// </summary>
    private float sqrMaxFlightRange = 0;

    /// <summary>
    /// Флаг, сигнализирующий о том, что цель была потеряна
    /// </summary>
    private bool targetLost = false;

    /// <summary>
    /// Выпустить пулю
    /// </summary>
    /// <param name="bulletEmitData">Данные полёта пули</param>
    public void Emit(BulletEmitData bulletEmitData)
    {
        this.bulletEmitData = bulletEmitData;

        sqrMaxFlightRange = bulletEmitData.MaxFlightRange * bulletEmitData.MaxFlightRange;

        transform.SetPositionAndRotation(bulletEmitData.StartPosition, bulletEmitData.StartRotation);

        gameObject.SetActive(true);

        targetLost = false;

        StartCoroutine(BulletMoving());
        StartCoroutine(CheckingFlightRange());
    }

    /// <summary>
    /// Выключить тело пули
    /// </summary>
    public void Disable()
    {
        StopAllCoroutines();
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Проверить, включено ли тело пули
    /// </summary>
    /// <returns></returns>
    public bool IsEnabled()
    {
        return gameObject.activeSelf;
    }

    private void Awake()
    {
        BulletCollider = GetComponent<Collider>();
        Disable();
    }

    private void OnTriggerEnter(Collider other)
    {
        Collided?.Invoke(other);
    }

    private void OnTriggerStay(Collider other)
    {
        Collided?.Invoke(other);
    }

    /// <summary>
    /// Корутина, выполняющая смещение пули
    /// </summary>
    /// <returns></returns>
    private IEnumerator BulletMoving()
    {
        while (true)
        {           
            if (!targetLost && (!bulletEmitData.Target.IsAlive() || bulletEmitData.Target.IsInvisible()))
            {
                targetLost = true;
            }

            Vector3 targetPosition;
            if (targetLost)
            {
                targetPosition = transform.position + transform.forward;
            }
            else
            {
                targetPosition = bulletEmitData.Target.Transform.position;
                // Пуля будет всегда лететь горизонтально
                targetPosition.y = transform.position.y;

                transform.LookAt(targetPosition);
            }         

            transform.position = Vector3.MoveTowards(transform.position,
                targetPosition, bulletEmitData.StartSpeed * 0.01f);

            yield return new WaitForFixedUpdate();
        }
    }

    /// <summary>
    /// Корутина, проверяющая достижение максимальной дистанции полёта
    /// </summary>
    /// <returns></returns>
    private IEnumerator CheckingFlightRange()
    {
        while ((transform.position - bulletEmitData.StartPosition).sqrMagnitude < sqrMaxFlightRange)
        {
            yield return new WaitForSeconds(FlightRangeCheckPeriod);
        }

        MaxDistanceReached?.Invoke();
    }
}
