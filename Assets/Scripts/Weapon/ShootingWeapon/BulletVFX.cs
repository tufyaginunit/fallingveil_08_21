using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

/// <summary>
/// ���������� ������� ����
/// </summary>
public class BulletVFX : MonoBehaviour
{
    [SerializeField, Tooltip("������ ����")]
    private Bullet bullet = null;

    [SerializeField, Tooltip("���������� �������, ������������ �� ����� �����")]
    private VisualEffect[] flyingVisualEffects = null;

    [SerializeField, Tooltip("������������ ��� ����������� �����"), Range(0.1f, 0.9f)]
    private float minAlpha = 0.1f;

    private void Awake()
    {
        bullet.RegisterOnBeforeEmitListener(OnBulletBeforeEmit);
    }

    private void OnDestroy()
    {
        if (bullet != null)
        {
            bullet.UnregisterOnBeforeEmitListener(OnBulletBeforeEmit);
        }
    }

    /// <summary>
    /// ���������� �������, ����������� ����� ���, ��� ���� ����� ��������
    /// </summary>
    private void OnBulletBeforeEmit()
    {
        foreach (var vfx in flyingVisualEffects)
        {
            if (vfx.HasFloat("Strength"))
            {
                float strength = bullet.NormalizedDamageValue;
                if (strength < minAlpha)
                {
                    strength = minAlpha;
                }
                vfx.SetFloat("Strength", strength);
            }            
        }
    }
}
