﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Основная задача класса: Создать пулл пуль заданного количестваа по заданному префабу и
/// в методе Shoot передавать положение и поворот источника пули.
/// </summary>
public class Shooting : MonoBehaviour
{  
    [SerializeField] private Bullet bulletPrefab; //через инспектор загружаем тип пули со своим поведением
    [SerializeField] private bool isInheritingSourceVelocity = false;

    public Vector3 BulletSize { get; private set; }

    private void Awake()
    {
        // Получить размер пули из префаба нельзя. Поэтому создаем временный объект, берем размер, и уничтожаем
        Bullet bullet = Instantiate(bulletPrefab, Vector3.zero, Quaternion.identity);

        Collider bulletCollider = bullet.GetBulletBodyCollider();
        if (bulletCollider is BoxCollider boxCollider)
        {
            BulletSize = boxCollider.size;
        }
        else
        {
            BulletSize = bulletCollider.bounds.size;
        }
        //BulletBounds = bullet.GetBulletBodyCollider().bounds;
        Destroy(bullet.gameObject);
    }

    public void Shoot(Vector3 startDirection, Quaternion startRotation, Vector3 startVelocity, 
        IDamageable target, float damageMultiplier)
    {
        if (!isInheritingSourceVelocity) startVelocity = Vector3.zero;

        BulletPoolContainerSingleton.Instance.Pool.EmitBullet(gameObject, startDirection, startRotation, 
            startVelocity, target, damageMultiplier);
    }
}
