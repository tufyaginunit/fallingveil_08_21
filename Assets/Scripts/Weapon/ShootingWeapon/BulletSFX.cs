﻿using UnityEngine;

/// <summary>
/// Звуковые эффекты пули
/// </summary>
public class BulletSFX : MonoBehaviour
{
    [SerializeField, Tooltip("Объект пули")]
    private Bullet bullet = null;

    [SerializeField, Tooltip("Звук при выстреле")]
    private AudioSource emissionSound = null;

    [SerializeField, Tooltip("Звук при попадании пули в стену")]
    private AudioSource wallHitSound = null;

    [SerializeField, Tooltip("Звук при попадании пули по цели")]
    private AudioSource targetHitSound = null;

    [SerializeField, Tooltip("Звук уничтожения пули в воздухе")]
    private AudioSource destroyedInAirSound = null;

    private void OnEnable()
    {
        bullet.RegisterOnBeforeEmitListener(OnBeforeEmit);
        bullet.RegisterBeforeDestroyWithoutHitListener(OnBeforeDestroyWithoutHit);
        bullet.RegisterHittedTargetListener(OnHittedTarget);
        bullet.RegisterHittedWallListener(OnHittedWall);
    }

    private void OnDisable()
    {
        bullet.UnregisterOnBeforeEmitListener(OnBeforeEmit);
        bullet.UnregisterBeforeDestroyWithoutHitListener(OnBeforeDestroyWithoutHit);
        bullet.UnregisterHittedTargetListener(OnHittedTarget);
        bullet.UnregisterHittedWallListener(OnHittedWall);
    }

    private void OnBeforeEmit()
    {
        PlaySound(emissionSound);       
    }

    private void OnBeforeDestroyWithoutHit()
    {
        PlaySound(destroyedInAirSound);
    }

    private void OnHittedTarget()
    {
        PlaySound(targetHitSound);
    }

    private void OnHittedWall()
    {
        PlaySound(wallHitSound);
    }

    private void PlaySound(AudioSource sound)
    {
        if (sound == null)
        {
            return;
        }
        SoundFunc.Stop(sound);
        SoundFunc.Play(sound);
        bullet.SetBusyTime(sound.clip.length);
    }
}
