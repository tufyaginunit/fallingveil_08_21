﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Контроллер оружия, стреляющего пулями
/// </summary>
public class ShootingWeaponController : WeaponController
{
    [SerializeField]
    [Tooltip("Объект, отвечающий за создание пуль")]
    private Shooting shooting = null;

    /// <summary>
    /// Цель, по которой стреляем
    /// </summary>
    private IDamageable target = null;

    /// <summary>
    /// Установить цель
    /// </summary>
    /// <param name="target"></param>
    public override void SetTarget(IDamageable target)
    {
        this.target = target;
    }

    /// <summary>
    /// Получить цель
    /// </summary>
    /// <returns></returns>
    public override IDamageable GetTarget()
    {
        return target;
    }

    /// <summary>
    /// Есть ли препятствия на линии атаки.
    /// Перегружаем метод, т.к. нужно учитывать размер пули
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public override bool ThereAreObstaclesOnAttackWay(IDamageable target)
    {
        // Используем BoxCast, т.к. нужно учитывать размер пули, определяя препятствия на пути.
        // Снаряд может появиться уже в стене, если атакующий стоит прямо перед ней, и BoxCast будет
        // ошибочно полагать, что препятствия нет. Поэтому делаем дополнительный BoxCast сверху, чтобы
        // определить, появится ли пуля в препятствии

        Vector3 topPoint = attackPoint.position;
        topPoint.y = VectorFunc.MaxRaycastDistance;

        Vector3 attackVector = target.Transform.position - attackPoint.position;
        attackVector.y = 0;
        Quaternion bulletBoxRotation = Quaternion.LookRotation(attackVector, Vector3.up);

        Vector3 bulletHalfExtents = (1 + 0.01f) * 0.5f * shooting.BulletSize;

        if (Physics.BoxCast(topPoint, bulletHalfExtents, Vector3.down, bulletBoxRotation,
            VectorFunc.MaxRaycastDistance - attackPoint.position.y,
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore))
        {
            return true;
        }

        Physics.BoxCast(attackPoint.position, bulletHalfExtents, attackVector, out RaycastHit hitInfo, bulletBoxRotation,
            attackVector.magnitude, GameManager.Instance.ObstacleLayers | target.Transform.gameObject.layer, QueryTriggerInteraction.Ignore);
        return hitInfo.collider != null && hitInfo.collider.transform != target.Transform;
    }

    /// <summary>
    /// Выполнить выстрел
    /// </summary>
    protected override void Hit()
    {
        Vector3 attackDirection;
        if (target != null)
        {
            attackDirection = (target.Transform.position - attackPoint.position).normalized;
            attackDirection.y = 0;
        }
        else
        {
            attackDirection = attackPoint.forward;
        }

        Quaternion rotation = Quaternion.LookRotation(attackDirection);

        shooting.Shoot(attackPoint.position,
                        rotation,
                        Vector3.zero,
                        target,
                        1);

        isHitPerforming = false;
    }

}
