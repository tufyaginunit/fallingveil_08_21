﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool
{
    [SerializeField] private Bullet bulletPrefab { get; }
    [SerializeField] private Transform bulletContainer { get; }

    private List<Bullet> pool;

    public BulletPool(Bullet BulletPrefab, int Count, Transform BulletContainer)
    {
        this.bulletPrefab = BulletPrefab;
        this.bulletContainer = BulletContainer;
        this.CreatePool(Count);
    }

    public void DisableAll()
    {
        foreach (var item in pool)
        {
            item.SetDefaultState();
        }
    }

    /// <summary>
    /// Заполняем пулл заданным числом дективированных объектов
    /// </summary>
    /// <param name="count"></param>
    private void CreatePool(int count)
    {
        this.pool = new List<Bullet>();

        for (int i = 0; i < count; i++)
        {
            this.AddNewElement();
        }
    }

    /// <summary>
    /// Первоначально создаем объекты с параметрами по умолчанию. Если незадействованные объекты в пулле закончаться, 
    /// то будем создавать сразу активные объекты с заданными парметрами
    /// </summary>
    /// <param name="owner"></param>
    /// <param name="startDirection"></param>
    /// <param name="startRotation"></param>
    /// <param name="startVelocity"></param>
    /// <param name="isActiveByDefault"></param>
    /// <returns></returns>
    private Bullet AddNewElement()
    {
        Bullet createdObject = Object.Instantiate(bulletPrefab, bulletContainer);
        pool.Add(createdObject);
        return createdObject;
    }

    /// <summary>
    /// Выполнить попытку найти пулю, которую можно переиспользовать
    /// </summary>
    /// <param name="element"></param>
    /// <returns></returns>
    public bool TryGetFreeElement(out Bullet element)
    {
        foreach (var bullet in pool)
        {
            if (bullet.IsReadyToEmit())
            {
                element = bullet;
                return true;
            }
        }
        element = null;
        return false;
    }

    /// <summary>
    /// Если есть незадействованный элемент в пулле, то передаем ему начальные параметры и 
    /// запускаем метод Fire();
    /// Если все объекты в пулле заняты, создаем новый сразу активный объект с заданными параметрами 
    /// и запускаем метод Fire()
    /// </summary>
    /// <param name="startDirection"></param>
    /// <param name="startRotation"></param>
    /// <param name="startVelocity"></param>
    public void EmitBullet(GameObject owner, Vector3 startDirection, Quaternion startRotation, 
        Vector3 startVelocity, IDamageable target, float damageMultiplier)
    {
        if (!TryGetFreeElement(out Bullet element))
        {
            element = AddNewElement();
        }            

        element.Emit(owner, startDirection, startRotation, startVelocity, target, damageMultiplier);
    }
}
