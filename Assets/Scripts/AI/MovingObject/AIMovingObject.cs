using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// �����, ���������� �� �������� �������, ������������ ��� ����������� ��
/// ����������� �� ������� ������ �����.
/// </summary>
public class AIMovingObject : MovingObject, ISaveable
{
    /// <summary>
    /// ����������� �������� �������� ��������� �� ������� ����� ���������� �� �������� �����,
    /// ����� ����� ����� �������� ����� ����������
    /// </summary>
    public const float MinSqrDistanceDifference = 0.001f;

    /// <summary>
    /// �������� ��������� �� �������� ����� ����������, ���� �������� ����� ����� ������
    /// �������� ��������
    /// ������� ��������
    /// </summary>
    private const float MinSqrDistanceToRecalculatePath = 1;

    private const float MinDistanceToSetCorner = 2;
    private const float MinSqrDistanceToSetCorner = MinDistanceToSetCorner * MinDistanceToSetCorner;

    [SerializeField]
    [Tooltip("�������� �������� � ������ ���")]
    protected float attackSpeed = 5;

    [SerializeField]
    [Tooltip("�������� �������� � ������ ���")]
    private float attackRotationSpeed = 180;

    [SerializeField]
    [Tooltip("�������� �������� � ��������� �������")]
    private float alertRotationSpeed = 120;

    [SerializeField]
    [Tooltip("�������� �������� ���� ������ (������)")]
    private float fovRotationSpeed = 20;
    
    [SerializeField]
    [Tooltip("��������� NavMesh ������, ������� ��������� �� ��������� �� NavMesh")]
    private NavMeshAgent navMeshAgent = null;

    /// <summary>
    /// ������ ������� ���������� ���������� ����
    /// </summary>
    private static readonly System.Random random = new System.Random();

    /// <summary>
    /// ������ ��������
    /// </summary>
    private readonly CoroutineTask coroutineTask = new CoroutineTask();

    /// <summary>
    /// ��������� ������ ����������� ������� ��, ������������� � ����������
    /// </summary>
    private int defaultAvoidancePriority = 0;

    /// <summary>
    /// ��������, ����������� �������� � �������� �����
    /// </summary>
    private Coroutine movingCoroutine = null;

    /// <summary>
    /// ������� ����� ����������, � ������� ����������� ��������
    /// </summary>
    private Vector3 currentDestinationPoint = Vector3.zero;

    private float collisionCheckRadius = 0;
    private float sqrCollisionCheckRadius = 0;

    /// <summary>
    /// ������� �������� ������� ��
    /// </summary>
    public override Vector3 CurrentVelocity => navMeshAgent.velocity;

    /// <summary>
    /// ������� �������� ��������, ������� �������� ������� ������
    /// </summary>
    public override float CurrentDesiredSpeed => navMeshAgent.speed;

    /// <summary>
    /// �������� �������� � ������ ���
    /// </summary>
    public float AttackRotationSpeed => attackRotationSpeed;

    /// <summary>
    /// �������� �������� � ��������� �������
    /// </summary>
    public float AlertRotationSpeed => alertRotationSpeed;

    /// <summary>
    /// �������� �������� ���� ������
    /// </summary>
    public float FovRotationSpeed => fovRotationSpeed;

    /// <summary>
    /// ���������� ������� �������� ��������
    /// </summary>
    public void SetIdleSpeed()
    {
        navMeshAgent.speed = speed;
    }

    /// <summary>
    /// ���������� �������� �������� � ��������� �����
    /// </summary>
    public void SetAttackSpeed()
    {
        navMeshAgent.speed = attackSpeed;
    }

    /// <summary>
    /// ���������� ������������ ��������� ������ ����������� ��� ������� ������� ��.
    /// ������������ ��� ����, ����� ������ ������ �� ��� ������������ ������ ������� ��
    /// </summary>
    public void SetMaxAvoidancePriority()
    {
        navMeshAgent.avoidancePriority = 0;
    }

    /// <summary>
    /// ���������� ��������� ������ ����������� �� ���������
    /// </summary>
    public void SetDefaultAvoidancePriority()
    {
        navMeshAgent.avoidancePriority = defaultAvoidancePriority;
    }

    /// <summary>
    /// ��������� �������� � �������� �����
    /// </summary>
    /// <param name="point"></param>
    public override void MoveToPoint(Vector3 point)
    {
        // ���� �������� ������ �� �������� � ����� �����, �� ��������, �� ��������� ��
        // ������ ��� � ��������
        if (coroutineTask.IsPerforming)         
        {
            // �� �������� ����� ����, ���� ������� ����� ���������� ������ ���������� � ����� ����
            if (VectorFunc.GetSqrDistanceXZ(currentDestinationPoint, point) <=
                MinSqrDistanceDifference)
            {
                return;
            }
            // ��������� ������� ��������, ����� ������ �����
            Stop();
        }

        // ���������� �������� � ����� �����
        coroutineTask.Start();
        movingCoroutine = StartCoroutine(PerformMovingToPoint(point));       
    }

    /// <summary>
    /// ��������� ������������� � ��������� �����
    /// </summary>
    /// <param name="point"></param>
    public void JumpToPoint(Vector3 point)
    {
        if (!IsAINavigationEnabled())
        {
            return;
        }
        navMeshAgent.Warp(point);
    }

    /// <summary>
    /// ���������� ����������� ��������
    /// </summary>
    public void Stop()
    {
        this.StopAndNullCoroutine(ref movingCoroutine);
        coroutineTask.Stop();
        SetDefaultAvoidancePriority();

        if (!IsAINavigationEnabled())
        {
            return;
        }
        navMeshAgent.ResetPath();
    }

    /// <summary>
    /// �������� ������� ���������� �� ������� ����� ����������
    /// </summary>
    /// <returns></returns>
    public float GetSqrDistanceToDestination()
    {
        return GetSqrDistanceToPoint(currentDestinationPoint);
    }

    /// <summary>
    /// ��������/��������� �������������� ������� ������� �� �� ������� ��������
    /// </summary>
    /// <param name="enabled">true - ��������, false - ���������</param>
    public void SetEnabledAutomaticRotation(bool enabled)
    {
        navMeshAgent.updateRotation = enabled;
    }

    /// <summary>
    /// ���������, ��������� �� ������� ��������
    /// </summary>
    /// <returns></returns>
    public bool IsMoving()
    {
        return coroutineTask.IsPerforming;
    }

    /// <summary>
    /// ��������/��������� ������ ��������.
    /// ����� ������������, ���� ����� ��������� ����������� ��������, � ����� ������ �����
    /// </summary>
    /// <param name="enabled">true - ��������, false - ���������</param>
    public void SetEnabled(bool enabled)
    {
        if (navMeshAgent.enabled != enabled)
        {
            navMeshAgent.enabled = enabled;
        }        
    }

    /// <summary>
    /// �������� �� ��������� ��
    /// </summary>
    /// <returns></returns>
    public bool IsAINavigationEnabled()
    {
        return navMeshAgent.enabled;
    }

    /// <summary>
    /// ���������� ������� �� ����� �� NavMesh
    /// </summary>
    /// <param name="point">�����, �� ������� �������������� �������</param>
    /// <param name="pathPoints">����� ��������</param>
    /// <returns>true - ����� ��������� � ������� ��� ��������, false - ����� �� ���������</returns>
    public bool CalculatePathToPoint(Vector3 point, out Vector3[] pathPoints)
    {
        pathPoints = null;
        if (!IsAINavigationEnabled())
        {
            return false;
        }
        
        NavMeshPath path = new NavMeshPath();       
        if (navMeshAgent.CalculatePath(point, path) && path.status == NavMeshPathStatus.PathComplete &&
            path.corners.Length > 0)
        {
            pathPoints = path.corners;
            return true;
        }

        return false;
    }

    /// <summary>
    /// ������ ��������� �������. 
    /// ������������ ��� �������������� ��������� ������� ��� ����������� �� ����������� �����.
    /// ������������ ����� ����� ���� ����������� ��� �������� �� �����,
    /// �� ������ �� ���������� ��� ������ ��� ����, ����� ��� ������ ���� � ����������� �����
    /// ������������ �������������� ��������� �������
    /// </summary>
    /// <param name="state"></param>
    public override void SetState(object state)
    {
        SetEnabled(false);
        base.SetState(state);
        SetEnabled(true);
    }

    protected override void Awake()
    {
        base.Awake();
        MaxSpeed = attackSpeed;

        navMeshAgent.speed = speed;
        navMeshAgent.angularSpeed = rotationSpeed;

        defaultAvoidancePriority = navMeshAgent.avoidancePriority;
        currentDestinationPoint = Position;
        collisionCheckRadius = navMeshAgent.radius * transform.lossyScale.x + 0.1f;
        sqrCollisionCheckRadius = collisionCheckRadius * collisionCheckRadius;
    }

    /// <summary>
    /// ���������, ���� �� ���������� ������� ����� ����������
    /// </summary>
    /// <returns></returns>
    private bool IsDestinationReached()
    {
        return GetSqrDistanceToDestination() <= MinSqrDistanceDifference;
    }

    /// <summary>
    /// ���������, ���� �� ���������� ��������� ����� ����������, 
    /// �������� � ������ ������ � navMeshAgent.destination
    /// </summary>
    /// <returns></returns>
    private bool IsNavMeshDestinationReached()
    {
        if (!IsAINavigationEnabled())
        {
            return false;
        }

        if (!navMeshAgent.pathPending)
        {
            if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            {
                if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// ��������, ����������� �������� � �������� �����
    /// </summary>
    /// <param name="finishTargetPoint">�������� �����</param>
    /// <returns></returns>
    private IEnumerator PerformMovingToPoint(Vector3 finishTargetPoint)
    {
        NavMeshPath path = new NavMeshPath();

        finishTargetPoint.y = GameManager.Instance.GroundY;
        currentDestinationPoint = finishTargetPoint;
        Vector3 nextPosition = currentDestinationPoint;

        float nextAllyPushTime = Time.time + 0.1f;
        
        float nextStuckCheckTime = Time.time + 1;
        Vector3 previousStuckCheckPosition = Position;
        Vector3 previousStuckCheckDestinationPoint = currentDestinationPoint;

        float lastSqrDistanceToDestination = GetSqrDistanceToDestination();
        while (!IsDestinationReached())
        {
            // ���� ��������� �� ���������, �������� � �������� ����� ����� �������� �� ��� ���, ����
            // ��� ����� �� ����� ��������           
            if (!IsAINavigationEnabled())
            {
                yield return new WaitForFixedUpdate();
                continue;
            }

            Vector3 positionXZ = Position;
            positionXZ.y = navMeshAgent.destination.y;
            nextPosition.y = navMeshAgent.destination.y;

            // ��������, �������� �� ������������� ��������� ��������
            if (IsNavMeshDestinationReached() ||
                (GetSqrDistanceToPoint(currentDestinationPoint) > sqrCollisionCheckRadius) &&
                (GetSqrDistanceToPoint(navMeshAgent.destination) <= sqrCollisionCheckRadius ||
                Vector3.Dot(navMeshAgent.destination - positionXZ, nextPosition - positionXZ) < 0))
            {
                navMeshAgent.CalculatePath(currentDestinationPoint, path);

                // ������ ��������� ����� ������ ��������, ������� ��������� �� ����������, �����������
                // ��� ������ ��������
                int cornerIndexToMove = -1;
                float sqrDistanceToNearestCorner = 0;
                for (int i = 1; i < path.corners.Length; i++)
                {
                    sqrDistanceToNearestCorner = VectorFunc.GetSqrDistanceXZ(Position, path.corners[i]);
                    if (sqrDistanceToNearestCorner > MinSqrDistanceToRecalculatePath)
                    {
                        cornerIndexToMove = i;
                        break;
                    }
                }

                if (cornerIndexToMove >= 0)
                {
                    // ����� ���� �������� ����� ���������� ����� ������ �� �������� ��������� ������� ��.
                    // ����� ��������� �������� ������� ������� �� �� ������������ ������������,
                    // ����� ���� �� � ����� ����� ���� ��������, � � � ����������� � �����, ������������ ��
                    // ��������� ����������.
                    Vector3 nearestCorner = path.corners[cornerIndexToMove];
                    nextPosition = nearestCorner;
                    if (sqrDistanceToNearestCorner > MinSqrDistanceToSetCorner)
                    {
                        positionXZ = Position;
                        positionXZ.y = path.corners[cornerIndexToMove].y;
                        nearestCorner = positionXZ + 
                            (nearestCorner - positionXZ).normalized * MinDistanceToSetCorner;
                        nextPosition = nearestCorner;
                    }
                    //else if (cornerIndexToMove < path.corners.Length - 1)
                    //{
                    //    nextPosition = path.corners[cornerIndexToMove + 1];
                    //}
                    //else
                    //{
                    //    nextPosition = path.corners[cornerIndexToMove];
                    //}
                    navMeshAgent.SetDestination(nearestCorner);
                }
                else
                {
                    // ���������� ����� �������� ����� �� �������, ������ ��� ��������� ����� ������
                    // � ������� ��������� ������� ��. � ���� ������ ������ ��� � ���.
                    navMeshAgent.SetPath(path);
                    nextPosition = currentDestinationPoint;
                }
            }

            yield return new WaitForFixedUpdate();
         
            // ������������ ���������, �� ����������� �� � ������� ��������� ��
            if (Time.time > nextAllyPushTime)
            {
                float currentSqrDistanceToDestination = GetSqrDistanceToDestination();
                if (Mathf.Abs(lastSqrDistanceToDestination - currentSqrDistanceToDestination) < collisionCheckRadius)
                {
                    // ���� ������ �� �� ��������� ����� ��������� �������������, ��������, ���
                    // �� ����������� � ������� ������. �������� ��������� �������������.                   
                    PushAllys();
                }
                lastSqrDistanceToDestination = currentSqrDistanceToDestination;
                nextAllyPushTime = Time.time + 0.1f + (float)random.NextDouble() * 0.5f;
            }

            if (Time.time > nextStuckCheckTime)
            {
                if (VectorFunc.GetSqrDistanceXZ(previousStuckCheckPosition, Position) < MinSqrDistanceDifference &&
                    VectorFunc.GetSqrDistanceXZ(previousStuckCheckDestinationPoint, currentDestinationPoint) < MinSqrDistanceDifference)
                {
                    //Debug.Log($"{this}: was stucked");
                    break;
                }

                previousStuckCheckPosition = Position;
                previousStuckCheckDestinationPoint = currentDestinationPoint;
                nextStuckCheckTime = Time.time + 1;
            }

        }

        Stop();
    }

    /// <summary>
    /// ����� ���������, �������� ������, � �������� ��
    /// </summary>
    private bool PushAllys()
    {
        bool allysPushed = false;

        Vector3 topPoint = Position;
        topPoint.y = VectorFunc.MaxRaycastDistance;

        RaycastHit[] hits = Physics.SphereCastAll(topPoint, collisionCheckRadius, Vector3.down,
            VectorFunc.MaxRaycastDistance - GameManager.Instance.GroundY, GameManager.Instance.EnemyLayers);

        foreach (var hitInfo in hits)
        {
            if (hitInfo.transform == transform)
            {
                continue;
            }
            AIMovingObject ally = hitInfo.collider.GetComponent<AIMovingObject>();
            if (ally != null && ally.IsAINavigationEnabled())
            {
                PushAlly(ally);
                allysPushed = true;
            }
        }

        return allysPushed;
    }


    /// <summary>
    /// �������� �������� ��� ������������ ���� �� ��������
    /// </summary>
    private void PushAlly(AIMovingObject allyAgent)
    {
        bool canAskForMove = false;
        if (allyAgent.IsMoving())
        {
            if (allyAgent.navMeshAgent.avoidancePriority < navMeshAgent.avoidancePriority)
            {
                return;
            }
            else if (allyAgent.GetSqrDistanceToDestination() <= MinSqrDistanceToRecalculatePath)
            {
                canAskForMove = true;
            }
        }
        else
        {
            canAskForMove = true;
        }

        if (canAskForMove)
        {
            // ������� �������� � ��������� ����������� � ��������� ����� [-90; 90]
            Vector3 pushDirection = allyAgent.transform.position - Position;
            pushDirection = VectorFunc.GetRotatedUnitVector(pushDirection, ((float)random.NextDouble() * 2 - 1) * 90);

            allyAgent.Stop();
            allyAgent.navMeshAgent.velocity = pushDirection;// * attackSpeed;
            allyAgent.navMeshAgent.avoidancePriority = navMeshAgent.avoidancePriority + 1;
        }
        else if (allyAgent.navMeshAgent.avoidancePriority == navMeshAgent.avoidancePriority)
        {
            allyAgent.navMeshAgent.avoidancePriority++;
        }
    }
}
