using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-��������� �����.
/// ���������, � ������� ��������� ����, ����� ����� ������ � ����� ��� ���������.
/// �������, ����� �� ������� ������������� ��������� AIAimingStrategy (��� ����������
/// ��������� �� ������) � ��������� AIAttackingStrategy (��� ���������� ����� ������ ������)
/// ��� ��������� ������ ���������� � �������� �������� �������-�����, 
/// �.�. �� ��������� ����������� ���������� �� ���
/// </summary>
[RequireComponent(typeof(AIAimingStrategy))]
[RequireComponent(typeof(AIAttackingStrategy))]
public class AttackingState : FsmState
{
    /// <summary>
    /// ���������� ������ ��
    /// </summary>
    private AIMovingObject movingObject;

    /// <summary>
    /// ��������� HP ������� ��
    /// </summary>
    private IDamageable healthObject;

    /// <summary>
    /// ���������� ������ ��
    /// </summary>
    private WeaponController weaponController;

    /// <summary>
    /// ��������� ��������� �� �� ����
    /// </summary>
    private AIAimingStrategy aimingStrategy;

    /// <summary>
    /// ��������� ����� ��
    /// </summary>
    private AIAttackingStrategy attackingStrategy;

    public override AIState AiState => AIState.Attacking;

    /// <summary>
    /// �����, ���������� ��� �������� � ���������
    /// </summary>
    public override void OnStateEnter()
    {
        IDamageable target = GameManager.Instance.Player;
        attackingStrategy.Initialize(weaponController, target);
        aimingStrategy.Initialize(movingObject, target);

        movingObject.SetAttackSpeed();
        aimingStrategy.StartMoving();
        attackingStrategy.StartAttacking();
    }

    /// <summary>
    /// �����, ���������� ��� ������ �� ���������
    /// </summary>
    public override void OnStateLeave()
    {
        aimingStrategy.StopMoving();
        attackingStrategy.StopAttacking();
    }

    /// <summary>
    /// ����������� �� �����
    /// </summary>
    /// <returns></returns>
    public override bool IsPerforming()
    {
        return !aimingStrategy.IsStopped();
    }

    private void Awake()
    {
        movingObject = GetComponentInParent<AIMovingObject>();
        healthObject = GetComponentInParent<IDamageable>();
        weaponController = GetComponentInParent<WeaponController>();
        attackingStrategy = GetComponent<AIAttackingStrategy>();
        aimingStrategy = GetComponent<AIAimingStrategy>();
    }
}
