﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine-состояние бегства врага, если он атакован игроком.
/// Требует, чтобы на объекте присутствовал компонент AIRetreatStrategy (для действия
/// по определённому алгоритму бегства).
/// Все состояния должны находиться в дочерних объектах объекта-врага, 
/// т.к. им требуются определённые компоненты на нём
/// </summary>
[RequireComponent(typeof(AIRetreatStrategy))]
public class RetreatState : FsmState
{
    /// <summary>
    /// Стратегия бегства
    /// </summary>
    private AIRetreatStrategy retreatStrategy;

    /// <summary>
    /// Движущийся объект ИИ
    /// </summary>
    private AIMovingObject movingObject;

    /// <summary>
    /// Поле зрения ИИ
    /// </summary>
    private FieldOfView fov;

    /// <summary>
    /// Компонент HP объекта ИИ
    /// </summary>
    private IDamageable healthObject;

    public override AIState AiState => AIState.Retreating;

    /// <summary>
    /// Метод, вызываемый при переходе в состояние
    /// </summary>
    public override void OnStateEnter()
    {
        healthObject.SetEnabledRegeneration(false);

        fov.SetAlertDetectionDelay();
        fov.SetEnabled(false);
        movingObject.SetIdleSpeed();

        retreatStrategy.Initialize(movingObject, fov, GameManager.Instance.Player, 
            GameManager.Instance.PlayerWeapon);
        retreatStrategy.StartMoving();
    }

    /// <summary>
    /// Метод, вызываемый при выходе из состояния
    /// </summary>
    public override void OnStateLeave()
    {
        healthObject.SetEnabledRegeneration(true);

        retreatStrategy.StopMoving();
        fov.SetEnabled(true);
    }

    private void Awake()
    {
        movingObject = GetComponentInParent<AIMovingObject>();
        fov = GetComponentInParent<FieldOfView>();
        healthObject = GetComponentInParent<IDamageable>();

        retreatStrategy = GetComponent<AIRetreatStrategy>();
    }
}
