﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Стратегия движения, выполняемая при поиске объектом ИИ цели (игрока), 
/// который вышел из поля зрения.
/// При использовании данного алгоритма враг ищет игрока не только в точке, где в последний раз 
/// его видел, но и за случайным препятствием
/// </summary>
public partial class AISearchingBehindObstaclesStrategy : AIMovementStrategy
{
    [SerializeField]
    [Tooltip("Угол поворота, который нужно совершить по прибытии на точку, " +
        "на которой была в последний раз замечена цель")]
    private float lastTargetPointRotationAngle = 60;

    [SerializeField]
    [Tooltip("Время поворота на точке, где в последний раз была замечена цель, сек")]
    private float lastTargetPointRotationTime = 4;

    [SerializeField]
    [Tooltip("Угол поворота, который нужно совершить по прибытии на точку за препятствием, " +
        " где мог спрятаться игрок")]
    private float behindObstacleRotationAngle = 60;

    [SerializeField]
    [Tooltip("Время поворота на точке за препятствием")]
    private float behindObstacleRotationTime = 4;

    [SerializeField]
    [Tooltip("Угол поворота, который нужно совершить, если не нашли препятствия, " +
        "за которым мог спрятаться игрок или если при движении к препятствию" +
        "оно стало прозрачным")]
    private float noObstacleRotationAngle = 360;

    [SerializeField]
    [Tooltip("Максимальное время, которое враг будет пытаться добраться до точки, где" +
        "в последний раз видел игрока, сек")]
    private float maxTryingReachLastTargetPointDuration = 60;

    [SerializeField]
    [Tooltip("Максимальное время движения к точке за препятствием, сек")]
    private float maxObstacleSearchDuration = 5;

    [SerializeField, Tooltip("Шанс того, что враг отреагирует на изменение прозрачности препятствий" +
        "при движении к точке, где в последний раз видел игрока"), Range(0, 100)]
    private float changingObstacleReactionChance = 50;

    /// <summary>
    /// Поле обзора врага
    /// </summary>
    private FieldOfView fov = null;

    /// <summary>
    /// Цель поиска (игрок)
    /// </summary>
    private Transform target = null;

    /// <summary>
    /// Текущая найденная точка за препятствием, где враг будет искать игрока
    /// (оформлено как поле класса для отладки)
    /// </summary>
    private ObstacleHidingPoint currentHidingPoint = null;

    /// <summary>
    /// Параметры текущего пути к точке за препятствием, где враг будет искать игрока
    /// (оформлено как поле класса для отладки)
    /// </summary>    
    private List<HidingPointPath> currentRetreatPathList = null;

    /// <summary>
    /// Рандом для поиска случайного препятствия для поиска игрока
    /// </summary>
    private static readonly System.Random random = new System.Random();

    /// <summary>
    /// Инициализировать объект стратегии движения
    /// (данный метод необходимо вызывать перед вызовом StartMoving)
    /// </summary>
    /// <param name="movingObject">Движущийся объект ИИ</param>
    /// <param name="fov">Поле обзора врага</param>
    /// <param name="target">Игрок</param>
    public void Initialize(AIMovingObject movingObject, FieldOfView fov, Transform target)
    {
        BaseInitialize(movingObject);
        this.target = target;
        this.fov = fov;
    }

    /// <summary>
    /// Корутина, выполняющая движение к точке, где в последний раз был замечен игрок,
    /// и её обследование
    /// </summary>
    /// <param name="coroutineTask"></param>
    /// <returns></returns>
    private IEnumerator PerformCheckingLastTargetPoint(CoroutineTask coroutineTask)
    {
        Vector3 lastTargetPoint = target.position;

        // Враг будет постоянно смотреть на точку, где в последний раз был замечен игрок.
        movingObject.SetEnabledAutomaticRotation(false);

        float angleToLastTargetPoint = VectorFunc.GetSignedAngleXZ(movingObject.transform.forward,
           lastTargetPoint - movingObject.Position);

        Coroutine rotatingCoroutine = StartCoroutine(PerformInfiniteLookAt(lastTargetPoint,
            movingObject.AlertRotationSpeed));

        // Движемся к точке, где в последний раз был замечен игрок.
        // Даже если находимся в замкнутом пространстве, будем некоторое время делать попытки
        // дойти до точки, т.к. окружающая обстановка может измениться.

        CoroutineTask movingTask = new CoroutineTask();

        float searchEndTime = Time.time + maxTryingReachLastTargetPointDuration;
        while (true)
        {
            movingTask.Start();
            StartCoroutine(PerformMoveToLastTargetPoint(lastTargetPoint, movingTask));
            while (movingTask.IsPerforming)
            {
                yield return new WaitForFixedUpdate();
            }

            if (movingObject.GetSqrDistanceToPoint(lastTargetPoint) < MinSqrDistanceToSmoothCorner 
                || Time.time > searchEndTime)
            {
                break;
            }

            yield return new WaitForFixedUpdate();
        }

        this.StopAndNullCoroutine(ref rotatingCoroutine);

        // Сбрасываем скорость, т.к. враг поначалу двигался к известной точке, где был игрок,
        // а сейчас не понятно, где искать игрока.
        movingObject.SetIdleSpeed();

        // После того, как дошел до точки, враг выполняет поворот, чтобы убедиться,
        // что цели нет в данной точке. Поворот выполняет в направлении, в котором скрылся игрок
        float rotationAngle = angleToLastTargetPoint >= 0 ? lastTargetPointRotationAngle 
            : -lastTargetPointRotationAngle;

        CoroutineTask rotationCoroutineTask = new CoroutineTask();
        rotationCoroutineTask.Start();
        StartCoroutine(PerformRotatingLeftRight(rotationAngle, movingObject.RotationSpeed, 
            lastTargetPointRotationTime, rotationCoroutineTask));
        
        while (rotationCoroutineTask.IsPerforming)
        {
            yield return new WaitForFixedUpdate();
        }
        coroutineTask.Stop();
    }

    /// <summary>
    /// Корутина, выполняющася движение по определённому маршруту
    /// </summary>
    /// <param name="path">Угловые точки маршрута</param>
    /// <param name="coroutineTask">Токен корутины</param>
    /// <returns></returns>
    private IEnumerator PerformMoveTheRoute(Vector3[] path, CoroutineTask coroutineTask)
    {
        if (path == null || path.Length <= 1)
        {
            movingObject.Stop();
            coroutineTask.Stop();
            yield break;
        }

        int pointIndex = 1;
        while (pointIndex < path.Length)
        {
            movingObject.MoveToPoint(path[pointIndex]);

            while (movingObject.IsMoving())
            {
                if (coroutineTask.IsCancellationRequested)
                {
                    movingObject.Stop();
                    coroutineTask.Stop();
                    yield break;
                }

                yield return new WaitForFixedUpdate();

                // Сглаживаем перемещение по маршруту, чтобы не было замедления и ускорения на точках пути.
                if (movingObject.GetSqrDistanceToPoint(path[pointIndex]) < MinSqrDistanceToSmoothCorner)
                {
                    movingObject.Stop();
                    break;
                }
            }

            pointIndex++;
        }

        movingObject.Stop();
        coroutineTask.Stop();
    }

    /// <summary>
    /// Корутина, выполняющая движение к точке, где враг в последний раз видел игрока
    /// </summary>
    /// <param name="lastTargetPoint">Целевая точка</param>
    /// <param name="coroutineTask">Токен корутины</param>
    /// <returns></returns>
    private IEnumerator PerformMoveToLastTargetPoint(Vector3 lastTargetPoint, CoroutineTask coroutineTask)
    {
        OpacityController opacityController = GameManager.Instance.Controller.OpacityController;

        bool needMoveLongWayOnly = false;
        bool longWayInProgress = opacityController.OpacityValue > OpacityController.MinOpacityValue;

        // Движемся к точке, на которой в последний раз был замечен игрок
        movingObject.MoveToPoint(lastTargetPoint);
        
        while (movingObject.IsMoving())
        {
            yield return new WaitForFixedUpdate();

            if (opacityController.OpacityValue <= OpacityController.MinOpacityValue)
            {
                // Коллайдеры стали прозрачными. Значит можно двигаться по более короткому пути.
                // Будем реагировать на это изменение только в том случае, если движемся по длинному пути
                if (longWayInProgress)
                {
                    // Коллайдеры стали прозрачными, проверим, должны ли мы реагировать на это изменение
                    // или будем продолжать движение, не меняя больше маршрут
                    if (random.Next(100) >= changingObstacleReactionChance)
                    {
                        needMoveLongWayOnly = true;
                        break;
                    }

                    // ИИ сам изменит свой путь на короткий
                    longWayInProgress = false;
                }
            }
            else if (!longWayInProgress)
            {
                // Коллайдеры стали непрозрачными, значит движемся по длинному пути
                longWayInProgress = true;
            }            
        }

        if (needMoveLongWayOnly)
        {
            // Продолжаем двигаться, уже не реагируя на изменение обстановки
            movingObject.CalculatePathToPoint(lastTargetPoint, out Vector3[] lastLongWayRoute);
            CoroutineTask movingTheRouteTask = new CoroutineTask();
            movingTheRouteTask.Start();
            StartCoroutine(PerformMoveTheRoute(lastLongWayRoute, movingTheRouteTask));

            while (movingTheRouteTask.IsPerforming)
            {
                yield return new WaitForFixedUpdate();
            }
        }

        coroutineTask.Stop();
    }

    /// <summary>
    /// Корутина, выполняющая движение к точке за случайным препятствием, где потенциально
    /// мог укрыться игрок, и её обследование
    /// </summary>
    /// <param name="coroutineTask"></param>
    /// <returns></returns>
    private IEnumerator PerformCheckingRandomObstacle(CoroutineTask coroutineTask)
    {
        CoroutineTask rotationCoroutineTask = new CoroutineTask();

        movingObject.SetEnabledAutomaticRotation(true);

        // Находим точку за препятствиями, где мог спрятаться игрок
        if (FindPointToSearch(out ObstacleHidingPoint hidingPoint))
        {
            movingObject.MoveToPoint(hidingPoint.Point);
        }

        bool checkOpacityChange = random.NextDouble() > 0.5f;
        bool pointReached = false;

        while (target != null)
        {
            while (hidingPoint != null)
            {
                if (!movingObject.IsMoving())
                {
                    pointReached = true;
                    break;
                }
                else if (checkOpacityChange && !hidingPoint.Obstacle.IsOpaque())
                {
                    // Один раз разрешаем реагировать на изменение прозрачности
                    checkOpacityChange = false;
                    break;
                }
                yield return new WaitForFixedUpdate();
            }

            if (pointReached)
            {
                // Достигли точки назначения за препятствием, выполняем поворот
                rotationCoroutineTask.Start();
                StartCoroutine(PerformRotatingLeftRight(behindObstacleRotationAngle, movingObject.RotationSpeed, 
                    behindObstacleRotationTime, rotationCoroutineTask));
                while (rotationCoroutineTask.IsPerforming)
                {
                    yield return new WaitForFixedUpdate();
                }

                break;
            }

            // Если не нашли точку или препятствие вдруг стало прозрачным, то будем вращаться
            // на текущей точке
            movingObject.Stop();

            rotationCoroutineTask.Start();
            StartCoroutine(PerformRotating(noObstacleRotationAngle, movingObject.RotationSpeed, 
                rotationCoroutineTask));

            bool canContinueMoving = false;
            while (rotationCoroutineTask.IsPerforming)
            {
                yield return null;

                // Прерываем поворот, если препятствие снова стало видимым
                if (hidingPoint != null && hidingPoint.Obstacle.IsOpaque())
                {
                    rotationCoroutineTask.Cancel();
                    canContinueMoving = true;
                    break;
                }
            }

            // Поворот завершился, но препятствие не стало видимым (или точка не была найдена),
            // завершаем действия по поиску
            if (!canContinueMoving)
            {
                break;
            }

            // Препятствие стало видимым, продолжаем движение к точке

            movingObject.MoveToPoint(hidingPoint.Point);

            yield return new WaitForFixedUpdate();
        }

        coroutineTask.Stop();
    }

    /// <summary>
    /// Корутина, которая выполняет поиск игрока
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformMoving()
    {
        CoroutineTask coroutineTask = new CoroutineTask();

        // Сначала идём к точке, где враг в последний раз видел игрока
        coroutineTask.Start();
        StartCoroutine(PerformCheckingLastTargetPoint(coroutineTask));
        while (coroutineTask.IsPerforming)
        {
            yield return null;
        }

        // Продолжаем поиск игрока за произвольным препятствием
        coroutineTask.Start();
        StartCoroutine(PerformCheckingRandomObstacle(coroutineTask));
        while (coroutineTask.IsPerforming)
        {
            yield return null;
        }

        movingCoroutine = null;
    }

    /// <summary>
    /// Получить точки рядом с препятствиями, которые находятся в поле зрения врага
    /// </summary>
    /// <returns></returns>
    private List<ObstacleHidingPoint> GetVisibleHidinigPoints()
    {
        // Получаем все точки вокруг препятствий из менеджера препятствий
        List<ObstacleHidingPoint> hidingPoints = ObstacleManager.Instance.HidingPoints.ToList();

        // Исключаем точки, которые находятся вокруг прозрачных препятствий
        for (int i = hidingPoints.Count - 1; i >= 0; i--)
        {
            if (!hidingPoints[i].Obstacle.IsOpaque())
            {
                hidingPoints.RemoveAt(i);
            }
        }

        List<ObstacleHidingPoint> visiblePoints = new List<ObstacleHidingPoint>();

        // Берём только видимые точки, находящиеся в радиусе поля зрения врага
        foreach (var point in hidingPoints)
        {
            Vector3 vectorToPoint = point.Point - movingObject.Position;
            vectorToPoint.y = 0;
            if (Vector3.Dot(vectorToPoint, movingObject.transform.forward) >= 0 &&
                fov.IsPointInFovRadius(point.Point)
                && CanSeePoint(point.Point))
            {
                visiblePoints.Add(point);
            }
        }

        return visiblePoints;
    }

    /// <summary>
    /// Получить список препятствий из списка точек отхода
    /// </summary>
    /// <param name="hidingPoints"></param>
    /// <returns></returns>
    private List<IObstacle> GetObstaclesFromHidingPoints(List<ObstacleHidingPoint> hidingPoints)
    {        
        List<IObstacle> obstacles = new List<IObstacle>();

        foreach (var point in hidingPoints)
        {
            if (!obstacles.Contains(point.Obstacle))
            {
                obstacles.Add(point.Obstacle);
            }
        }

        return obstacles;
    }

    /// <summary>
    /// Получить точки вокруг препятствий, которые невидимы для врага
    /// </summary>
    /// <param name="obstacles">Список препятствий, вокруг которых ищем точки</param>
    /// <returns></returns>
    private List<ObstacleHidingPoint> GetInvisiblePointsAroundObstacles(List<IObstacle> obstacles)
    {
        // Получаем все точки вокруг препятствий из менеджера препятствий
        List<ObstacleHidingPoint> allHidingPoints = ObstacleManager.Instance.HidingPoints.ToList();

        List<ObstacleHidingPoint> invisiblePoints = new List<ObstacleHidingPoint>();

        foreach (var point in allHidingPoints)
        {
            Vector3 vectorToPoint = point.Point - movingObject.Position;
            vectorToPoint.y = 0;
            if (obstacles.Contains(point.Obstacle) &&
                Vector3.Dot(vectorToPoint, movingObject.transform.forward) >= 0 &&
                Vector3.Dot(point.Normal, movingObject.transform.forward) >= 0 &&
                !CanSeePoint(point.Point))
            {
                invisiblePoints.Add(point);
            }
        }

        return invisiblePoints;
    }

    /// <summary>
    /// Найти точку за произвольным препятствием, где враг будет пытаться найти игрока
    /// </summary>
    /// <param name="pointToSearch"></param>
    /// <returns></returns>
    private bool FindPointToSearch(out ObstacleHidingPoint pointToSearch)
    {
        pointToSearch = null;

        // Найдём препятствия, которые видит враг
        List<ObstacleHidingPoint> visiblePoints = GetVisibleHidinigPoints();
        List<IObstacle> visibleObstacles = GetObstaclesFromHidingPoints(visiblePoints);

        // Найдём точки за препятствиями, которые в данный момент не видны врагу
        List<ObstacleHidingPoint> invisiblePoints = GetInvisiblePointsAroundObstacles(visibleObstacles);

        // Рассчитываем маршруты до точек. Отсеиваем недостижимые точки,
        // а также точки, передвижение до которых займёт много времени
        List<HidingPointPath> hidingPointPathList = GetPathToPointsList(invisiblePoints);

        if (TryChoosePath(hidingPointPathList, out HidingPointPath chosenPath))
        {
            pointToSearch = chosenPath.point;
            currentHidingPoint = pointToSearch;
            return true;
        }
        return false;
    }

    /// <summary>
    /// Заполнить список маршрутов к точкам вокруг препятствий.
    /// Здесь также происходит проверка на возможность достижения точки и отсев точек, 
    /// время пути до которых больше допустимого
    /// </summary>
    /// <param name="hidingPoints">Список точек вокруг препятствий</param>
    private List<HidingPointPath> GetPathToPointsList(List<ObstacleHidingPoint> hidingPoints)
    {
        List<HidingPointPath> pathList = new List<HidingPointPath>();

        foreach (var point in hidingPoints)
        {
            if (movingObject.CalculatePathToPoint(point.Point, out Vector3[] pathPoints))
            {
                HidingPointPath path = new HidingPointPath(point, pathPoints);
                // Отсеим точки с нулевой длиной пути и те, время пути до которых больше допустимого
                if (path.pathLength <= 0 || (path.pathLength / movingObject.CurrentDesiredSpeed > maxObstacleSearchDuration))
                {
                    continue;
                }

                pathList.Add(path);            
            }
        }

        return pathList;
    }

    private bool TryChoosePath(List<HidingPointPath> pathList, out HidingPointPath chosenPath)
    {
        if (pathList.Count == 0)
        {
            chosenPath = default;
            return false;
        }

        // Вытащим список препятствий из возможных маршрутов движения
        List<IObstacle> obstacles = new List<IObstacle>();
        foreach (var path in pathList)
        {
            if (!obstacles.Contains(path.point.Obstacle))
            {
                obstacles.Add(path.point.Obstacle);
            }
        }

        // Из всего списка точек выберем только по одной ближайшей к врагу точке
        // каждого препятствия
        List<HidingPointPath> minPathList = new List<HidingPointPath>();

        foreach (var obstacle in obstacles)
        {
            float minLength = float.MaxValue;
            HidingPointPath minPath = default;
            foreach (var path in pathList)
            {
                if (path.point.Obstacle == obstacle && path.pathLength < minLength)
                {
                    minLength = path.pathLength;
                    minPath = path;
                }
            }
            minPathList.Add(minPath);
        }

        // Для отладки. Будем выводить точки различных препятствий с минимальным расстоянием,
        // из которых враг выбирает точку, куда пойти
        currentRetreatPathList = minPathList;

        // Посчитаем сумму весов для распределения вероятностей.
        // Чем длиннее путь до точки, тем меньше будет вероятность, что враг туда пойдет
        float weightSum = 0;
        foreach (var path in minPathList)
        {
            weightSum += 1 / path.pathLength;
        }

        // Берём случайное число в полуинтервале [0,1)
        double randomProbabilityValue = random.NextDouble();
        
        // Находим диапазон соответствующего препятствия, в который попало
        // случайно число
        chosenPath = minPathList[0];
        float probabilityRangeValue = 0;
        foreach (var path in minPathList)
        {
            float weight = 1 / path.pathLength;
            probabilityRangeValue += weight * 1 / weightSum;
            if (randomProbabilityValue < probabilityRangeValue)
            {
                chosenPath = path;
                break;
            }
        }

        return true;
    }

    /// <summary>
    /// Проверить, видна ли точка
    /// </summary>
    /// <param name="point">Проверяемая точка</param>
    /// <returns></returns>
    private bool CanSeePoint(Vector3 point)
    {
        Vector3 vectorToPoint = point - fov.transform.position;
        vectorToPoint.y = 0;
        float distance = vectorToPoint.magnitude;

        return !AreObstaclesOnWay(fov.transform.position, vectorToPoint, distance);
    }

    /// <summary>
    /// Проверить, есть ли препятствия на векторе
    /// </summary>
    /// <param name="startPoint">Начальная точка</param>
    /// <param name="direciton">Направление вектора</param>
    /// <param name="distance">Длина вектора</param>
    /// <returns></returns>
    private bool AreObstaclesOnWay(Vector3 startPoint, Vector3 direciton, float distance)
    {
        return Physics.Raycast(startPoint, direciton, distance,
            GameManager.Instance.ObstacleLayers, QueryTriggerInteraction.Ignore);
    }

    /// <summary>
    /// Отладочная отрисовка на экране сцены
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        if (ObstacleManager.Instance == null || !ObstacleManager.Instance.DrawHidingPoints)
        {
            return;
        }

        // Орисовываем красным текущую точку, выбранную для того, чтобы спрятаться
        if (currentHidingPoint != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawCube(currentHidingPoint.Point, new Vector3(1, 1, 1));
        }

        // Отрисовываем зелёным возможные точки, из которых враг выбирает оптимальную
        if (currentRetreatPathList != null)
        {
            foreach (var item in currentRetreatPathList)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawCube(item.point.Point, new Vector3(0.7f, 0.7f, 0.7f));
            }
        }
    }

}
