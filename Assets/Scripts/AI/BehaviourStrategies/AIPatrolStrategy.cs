﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Стратегия движения, выполняемая при патрулировании ИИ
/// </summary>
public class AIPatrolStrategy : AIMovementStrategy
{    
    /// <summary>
    /// Класс, хранящий параметры текущей точки маршрута и отвечающий за выбор следующей
    /// </summary>
    private class CurrentPathParam
    {
        /// <summary>
        /// Маршрут движения
        /// </summary>
        private NPCPath path;
        /// <summary>
        /// Индекс текущей точки маршрута
        /// </summary>
        private int pointIndex;
        /// <summary>
        /// Направление движения (1 - движение в прямом направлении, -1 - в обратном)
        /// </summary>
        private int directionIndexIncrement;

        /// <summary>
        /// Индекс текущей точки маршрута
        /// </summary>
        public int PointIndex => pointIndex;

        /// <summary>
        /// Конструктор, который принимает маршрут движения и текущую точку ИИ, и рассчитывает
        /// точку маршрута, ближайшую к текущей точке
        /// </summary>
        /// <param name="path">Маршрут движения</param>
        /// <param name="currentPosition">Текущая точка, на которой находится объект ИИ</param>
        public CurrentPathParam(NPCPath path, Vector3 currentPosition)
        {
            this.path = path;
            pointIndex = path.GetNearestPoint(currentPosition);
            directionIndexIncrement = 1;
        }

        /// <summary>
        /// Выбрать следующую точку маршрута для движения
        /// </summary>
        public void SetNextPoint()
        {
            int nextPointIndex = GetNextPointInDirection(directionIndexIncrement);

            if (!path.IsClosed && nextPointIndex > pointIndex && directionIndexIncrement < 0 
                || nextPointIndex < pointIndex && directionIndexIncrement > 0)
            {
                directionIndexIncrement = -directionIndexIncrement;
            }

            pointIndex = nextPointIndex;
        }

        public int GetNextPointInDirection(int direction)
        {
            return direction > 0 ? path.GetNextPoint(pointIndex) 
                : path.GetPreviousPoint(pointIndex);
        }
    }

    [SerializeField]
    [Tooltip("Данные маршрута движения")]
    private PathData pathData = null;

    [SerializeField]
    [Tooltip("Нужно ли сглаживать перемещение по маршруту, " +
        "чтобы не было замедления и ускорения на угловых точках.")]
    private bool smoothPath = false;

    /// <summary>
    /// Положение объекта ИИ перед началом патрулирования 
    /// </summary>
    private Vector3 initialDirection;

    /// <summary>
    /// Поле обнаружения объекта ИИ
    /// </summary>
    private FieldOfView fov = null;

    /// <summary>
    /// Данные маршрута движения
    /// </summary>
    public PathData PathData => pathData;

    /// <summary>
    /// Инициализировать объект стратегии движения
    /// (данный метод необходимо вызывать перед вызовом StartMoving)
    /// </summary>
    /// <param name="movingObject">Движущийся объект ИИ</param>
    public void Initialize(AIMovingObject movingObject, FieldOfView fov)
    {
        BaseInitialize(movingObject);
        this.fov = fov;
        if (pathData.Path.PointsCount <= 1)
        {
            initialDirection = movingObject.transform.forward;
        }
        else
        {
            initialDirection = (pathData.Path[1] - pathData.Path[0]).normalized;
        }
    }

    /// <summary>
    /// Установить состояние по умолчанию
    /// </summary>
    public void SetDefault()
    {
        movingObject.JumpToPoint(pathData.Path[0]);
        movingObject.Rotate(movingObject.Position + initialDirection);
    }

    /// <summary>
    /// Остановить движение
    /// </summary>
    public override void StopMoving()
    {
        base.StopMoving();
      
        float rotation = VectorFunc.GetSignedAngleXZ(fov.VisionFov.PointOfSight.forward, 
            movingObject.transform.forward);
        movingObject.Rotate(rotation);
        fov.VisionFov.PointOfSight.forward = movingObject.transform.forward;

        movingObject.SetMovingMode(MovingMode.Air);
    }

    /// <summary>
    /// Корутина, которая выполняет движение по маршруту патрулирования
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformMoving()
    {
        movingObject.SetEnabledAutomaticRotation(true);

        Coroutine rotatingCoroutine;

        // Если маршрут имеет только одну точку, то это означает, что на ней выполняется только
        // бесконечный поворот, без движения к очередным точкам
        bool infiniteRotation = pathData.Path.PointsCount <= 1;

        CurrentPathParam currentPathParam = new CurrentPathParam(pathData.Path, movingObject.Position);

        int pointIndex = currentPathParam.PointIndex;

        // Вначале выполняем движение объекта ИИ от текущей точки к ближайшей точке маршрута
        movingObject.MoveToPoint(pathData.Path[pointIndex]);

        while (movingObject.IsMoving())
        {
            yield return new WaitForFixedUpdate();
        }

        // Как только вернулись на маршрут, выключаем автоматический поворот объекта.
        // Будем самостоятельно поворачивать объект ИИ в соответствующем направлении.
        movingObject.SetEnabledAutomaticRotation(false);

        // Если маршрут только начался, не будем стоять на начальной точке, а сразу от неё пойдём
        // к следующей
        bool isPatrolJustStarted = true;

        int previousPointIndex = currentPathParam.PointIndex;
        while (true)
        {
            movingObject.SetMovingMode(MovingMode.Air);

            pointIndex = currentPathParam.PointIndex;
            movingObject.MoveToPoint(pathData.Path[pointIndex]);

            // Если текущая точка - один из концов маршрута, то нужно выбрать направление
            // поворота назад.
            PathTurnDirection turnDirection = PathTurnDirection.Auto;
            if (pointIndex != previousPointIndex)
            {
                if (previousPointIndex == 0)
                {
                    turnDirection = pathData.FirstPointTurnDirection;
                }
                else if (previousPointIndex == pathData.Path.PointsCount - 1)
                {
                    turnDirection = pathData.LastPointTurnDirection;
                }
            }

            Vector3 pointToLookAt = pathData.Path[pointIndex] + GetSightDirectionInPoint(pointIndex);
            rotatingCoroutine = StartCoroutine(
                PerformInfiniteLookAt(pointToLookAt, movingObject.RotationSpeed, turnDirection));           

            PathPointData pointData = pathData.GetPointData(currentPathParam.PointIndex);

            while (movingObject.IsMoving())
            {
                yield return new WaitForFixedUpdate();

                // Если до очередной точки маршрута осталось совсем немного, и на ней не нужно стоять,
                // то мы сразу переходим к следующей. Таким образом мы сглаживаем перемещение по маршруту,
                // чтобы не было замедления и ускорения на таких точках.
                if (smoothPath && pointData.StopTime == 0 &&
                    movingObject.GetSqrDistanceToPoint(pathData.Path[pointIndex]) < MinSqrDistanceToSmoothCorner)
                {
                    break;
                }
            }

            this.StopAndNullCoroutine(ref rotatingCoroutine);

            movingObject.SetMovingMode(MovingMode.Ground);
            yield return new WaitForFixedUpdate();

            // Рассчитываем поворот, на который должен быть повернут объект ИИ в данной точке
            if (infiniteRotation || (pointData.StopTime > 0 && !isPatrolJustStarted))
            {
                float angleToSightDirectionInPoint = VectorFunc.GetSignedAngleXZ(movingObject.transform.forward,
                    GetSightDirectionInPoint(pointIndex));

                CoroutineTask rotatingTask = new CoroutineTask();
                rotatingTask.Start();
                StartCoroutine(PerformRotating(angleToSightDirectionInPoint, movingObject.RotationSpeed, rotatingTask));
                while (rotatingTask.IsPerforming)
                {
                    yield return new WaitForFixedUpdate();
                }

                // Если на точке нужно бесконечно стоять, не поворачиваясь, просто выходим из корутины 
                if (infiniteRotation && pointData.RotationAngle == 0)
                {
                    break;
                }

                // Вычисляем время, когда можно будет продолжить движение по маршруту
                if (pointData.RotationAngle > 0)
                {
                    float timeToNextMove = Time.time + pointData.StopTime;

                    // Выполняем повороты влево-вправо отведённое для этого время
                    rotatingCoroutine = StartCoroutine(
                        PerformInfiniteFovRotatingLeftRight(pointData.RotationAngle, movingObject.FovRotationSpeed));
                    while (infiniteRotation || Time.time < timeToNextMove)
                    {
                        yield return new WaitForFixedUpdate();
                    }
                    this.StopAndNullCoroutine(ref rotatingCoroutine);
                }
            }

            previousPointIndex = currentPathParam.PointIndex;

            // Рассчитываем следующую точку движения по маршруту
            currentPathParam.SetNextPoint();

            isPatrolJustStarted = false;
        }

        movingCoroutine = null;
    }

    /// <summary>
    /// Получить направление взгляда в точке патрулирования
    /// </summary>
    /// <param name="pointIndex">Индекс точки патрулирования, 
    /// для которой нужно определить направление взгляда</param>
    /// <returns></returns>
    private Vector3 GetSightDirectionInPoint(int pointIndex)
    {
        // Если точка одна, используем направление объекта ИИ, изначально заданное на сцене
        if (pathData.Path.PointsCount <= 1)
        {
            return initialDirection;
        }

        // Находим два направления: от предыдущей точки маршрута к указанной и от следующей точки маршрута к указанной
        // Вектор, получаемый суммированием этих направлений и будем брать за направление взгляда
        int nextPointIndex = pathData.Path.GetNextPoint(pointIndex);
        int prevPointIndex = pathData.Path.GetPreviousPoint(pointIndex);
        Vector3 direction1 = (pathData.Path[pointIndex] - pathData.Path[nextPointIndex]).normalized;
        Vector3 direction2 = (pathData.Path[pointIndex] - pathData.Path[prevPointIndex]).normalized;

        return (direction1 + direction2).normalized;
    }

    protected IEnumerator PerformInfiniteFovRotatingLeftRight(float fullAngle, float rotationSpeed)
    {
        int direction = fullAngle > 0 ? 1 : -1;
        fullAngle = Mathf.Abs(fullAngle);
        float maxRotationAngle = fullAngle * .5f;
        float fullRotation = 0;
        while (true)
        {
            yield return new WaitForFixedUpdate();

            float rotationDelta = rotationSpeed * Time.fixedDeltaTime;
            fov.VisionFov.Rotate(direction * rotationDelta);

            fullRotation += rotationDelta;
            if (fullRotation >= maxRotationAngle)
            {
                fullRotation = 0;
                direction = -direction;
                maxRotationAngle = fullAngle;
            }
        }
    }
}
