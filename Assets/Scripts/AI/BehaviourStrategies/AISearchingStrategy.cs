using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ��������� ��������, ����������� ��� ������ �������� �� ���� (������), 
/// ������� ����� �� ���� ������
/// </summary>
public class AISearchingStrategy : AIMovementStrategy
{
    /// <summary>
    /// ���� ��������, ������� ����� ��������� �� �������� �� �����, 
    /// �� ������� ���� � ��������� ��� �������� ����
    /// </summary>
    private const float RotationAngle = 360;

    /// <summary>
    /// ����� ����� ��������, ������� ���� ��� � �����, �� ������� ���� � ��������� 
    /// ��� �������� ����, ����� ���, ��� ����� ��������� � ��������������
    /// </summary>
    private const float DelayBeforeReturnToPatrol = 0.5f;

    /// <summary>
    /// ����, ������� ����� ������
    /// </summary>
    private Transform target;

    /// <summary>
    /// ���������������� ������ ��������� ��������
    /// (������ ����� ���������� �������� ����� ������� StartMoving)
    /// </summary>
    /// <param name="movingObject">���������� ������ ��</param>
    /// <param name="target">����</param>
    public void Initialize(AIMovingObject movingObject, Transform target)
    {
        BaseInitialize(movingObject);
        this.target = target;
    }

    /// <summary>
    /// ��������, ������� ��������� ����� ����
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformMoving()
    {
        // ���� ����� ��������� �������� �� �����, ��� � ��������� ��� ��� ������� �����.
        // ���� ��������� �������� �� ����������� �������� � ���� �����, ����� �������� �������� ���� ������,
        // ����� �������� � ��� �����������
        Vector3 pointToLookAt = target.position + (target.position - movingObject.Position).normalized;
        float angleToLastTargetPoint = VectorFunc.GetSignedAngleXZ(movingObject.transform.forward,
            pointToLookAt - movingObject.Position);

        movingObject.SetEnabledAutomaticRotation(false);
        Coroutine rotatingCoroutine = StartCoroutine(PerformInfiniteLookAt(pointToLookAt, 
            movingObject.AlertRotationSpeed));

        // �������� � �����, �� ������� � ��������� ��� ��� ������� �����
        movingObject.MoveToPoint(target.position);

        while (movingObject.IsMoving())
        {
            yield return new WaitForFixedUpdate();
        }
        this.StopAndNullCoroutine(ref rotatingCoroutine);      

        // ����� ����, ��� ����� �� �����, ���� ��������� �������, ����� ���������,
        // ��� ���� ��� ������ ����. ������� ��������� � �����������, � ������� ������� �����
        float rotationAngle = angleToLastTargetPoint >= 0 ? RotationAngle : -RotationAngle;

        CoroutineTask rotatingTask = new CoroutineTask();
        rotatingTask.Start();
        StartCoroutine(PerformRotating(rotationAngle, movingObject.RotationSpeed, rotatingTask));
        while (rotatingTask.IsPerforming)
        {
            yield return new WaitForFixedUpdate();
        }

        this.StopAndNullCoroutine(ref rotatingCoroutine);

        // ����� ��������� �������� ����� ������������ � ��������������
        yield return new WaitForSeconds(DelayBeforeReturnToPatrol);

        movingCoroutine = null;
    }
}
