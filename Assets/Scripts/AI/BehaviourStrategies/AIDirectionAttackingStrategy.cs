﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Стратегия атаки объекта, находящегося на прямой линии атаки 
/// </summary>
public class AIDirectionAttackingStrategy : AIAttackingStrategy
{
    /// <summary>
    /// Периодичность проверки на возможность стрельбы
    /// </summary>
    const float CheckPeriod = 0.1f;

    /// <summary>
    /// Корутина процесса атаки
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformAttacking()
    {
        weaponController.SetTarget(target);
        while (target != null && target.IsAlive())
        {
            // Делаем проверки на возможность стрельбы:
            // оружие не находится в процессе атаки (начало, удар, кулдаун после атаки),
            // цель находится на прямой линии огня,
            // цель находится в радиусе стрельбы оружия
            if (weaponController.IsTargetOnAttackLine(target)
                && weaponController.CheckTargetAttackDistance(target) == AttackCheckResult.Ok)
            {
                if (!weaponController.IsAttacking())
                {
                    weaponController.Attack();
                }              
            }
            else if (weaponController.IsAttacking() && !weaponController.IsCoolDowning && 
                !weaponController.IsInterruptionRequested)
            {
                weaponController.InterruptAttack();
            }

            yield return new WaitForSeconds(CheckPeriod);
        }
    }
}
