﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Стратегия движения, выполняемая при наведении ИИ на цель
/// </summary>
public class AIAimingStrategy : AIMovementStrategy
{
    /// <summary>
    /// Период проверки положения цели (задержка перед выполнением очередной проверки)
    /// </summary>
    const float CheckPeriod = 0.1f;

    [SerializeField, Tooltip("Желаемое расстояние до цели, на которое объект ИИ старается приблизится")]
    private float desiredDistanceToAim = 10;
    
    /// <summary>
    /// Цель для наведения
    /// </summary>
    private IDamageable target;

    private float sqrDesiredDistanceToAim = 0;

    /// <summary>
    /// Инициализировать объект стратегии движения
    /// (данный метод необходимо вызывать перед вызовом StartMoving)
    /// </summary>
    /// <param name="movingObject">Движущийся объект ИИ</param>
    /// <param name="target">Цель наведения</param>
    public void Initialize(AIMovingObject movingObject, IDamageable target)
    {
        BaseInitialize(movingObject);
        this.target = target;
    }

    /// <summary>
    /// Корутина процесса движения, выполняющая наведение на цель
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator PerformMoving()
    {
        if (target == null)
        {
            movingCoroutine = null;
            yield break;
        }

        movingObject.SetEnabledAutomaticRotation(false);
        StartCoroutine(PerformInfiniteLookAt(target.Transform, movingObject.AttackRotationSpeed));

        while (target != null && target.IsAlive())
        {
            if (movingObject.GetSqrDistanceToPoint(target.Transform.position) > sqrDesiredDistanceToAim)
            {
                movingObject.MoveToPoint(target.Transform.position);
            }
            else if (movingObject.IsMoving())
            {
                movingObject.Stop();
            }
            
            yield return new WaitForSeconds(CheckPeriod);
        }

        StopMoving();

        movingCoroutine = null;
    }

    private void Awake()
    {
        sqrDesiredDistanceToAim = desiredDistanceToAim * desiredDistanceToAim;
    }
}
