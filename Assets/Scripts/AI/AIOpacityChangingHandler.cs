﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс для обработки события изменения видимости объектов для объекта ИИ.
/// Служит для перемещения объекта ИИ за пределы препятствия, которое стало видимым 
/// </summary>
public class AIOpacityChangingHandler : OpacityChangingHandler
{
    [SerializeField]
    [Tooltip("Движущийся объект, находящегося под управлением ИИ")]
    private AIMovingObject movingObject;

    [SerializeField]
    [Tooltip("Стратегия движения, при которой враг старается выбраться из препятствия, " +
        "которое стало видимым")]
    private GetOutOfObstacleStrategy getOutOfObstacleStrategy;

    /// <summary>
    /// Обработчик события изменения видимости объектов.
    /// Определяет, находится ли объект ИИ внутри коллайдеров изменяемых препятствий и
    /// передвигает его за пределы этих препятствий
    /// </summary>
    protected override void OnOpacityChanged()
    {
        // Если не было изменения прозрачности с граничного состояния или в граничное состояние,
        // прозрачность коллайдеров препятствий не изменится, поэтому не обрабатываем такие ситуации.
        if (opacityController.OpacityValue > OpacityController.MinOpacityValue &&
            opacityController.OpacityValue < OpacityController.MaxOpacityValue &&
            previousOpacityValue > OpacityController.MinOpacityValue &&
            previousOpacityValue < OpacityController.MaxOpacityValue)
        {
            previousOpacityValue = opacityController.OpacityValue;
            return;
        }

        previousOpacityValue = opacityController.OpacityValue;

        Vector3 topPoint = movingObject.Position;
        topPoint.y = VectorFunc.MaxRaycastDistance;

        RaycastHit[] raycastHits = Physics.RaycastAll(topPoint, Vector3.down,
            VectorFunc.MaxRaycastDistance - GameManager.Instance.GroundY,
            GameManager.Instance.ChangingObstacleLayers);

        if (raycastHits.Length == 0)
        {
            getOutOfObstacleStrategy.StopMoving();
            return;
        }

        List<IObstacle> obstaclesToGetOut = new List<IObstacle>();
        foreach (var hitInfo in raycastHits)
        {
            ChangingObstacle obstacle = hitInfo.collider.GetComponentInParent<ChangingObstacle>();
            if (obstacle != null && !obstacle.CheckPassability(opacityController.OpacityValue) && 
                !obstaclesToGetOut.Contains(obstacle))
            {
                obstaclesToGetOut.Add(obstacle);
            }
        }

        if (obstaclesToGetOut.Count == 0)
        {
            getOutOfObstacleStrategy.StopMoving();
            return;
        }

        getOutOfObstacleStrategy.Initialize(movingObject, obstaclesToGetOut);
        getOutOfObstacleStrategy.StartMoving();
    }
}
