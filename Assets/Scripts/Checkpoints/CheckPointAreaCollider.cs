using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ��������� ���� ����������� �����.
/// � ���� ���� ������������ ����� ������, �� ������� �������� ������ ����������� �����.
/// ������ ���� ������� �� ������ � �����-���� ����������� � �������� ��������� ������� ����������� �����
/// </summary>
[RequireComponent(typeof(Collider))]
public class CheckPointAreaCollider : MonoBehaviour
{
    /// <summary>
    /// ����������� �����-�������� ����������
    /// </summary>
    public CheckPoint Owner { get; private set; }
    
    private void Awake()
    {
        // ������� ������������ ����������� �����
        Owner = GetComponentInParent<CheckPoint>();
    }
}
