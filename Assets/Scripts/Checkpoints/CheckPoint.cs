using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

/// <summary>
/// ����������� �����, �� ������� ������������ �������� ����� ������.
/// ����������� ��� ��������� �������� ������ � ������ ����������� ����������� �����
/// ������ ������ ������, ������� ������ ������������ ��� ��������� ��� ����������� ������,
/// ����� ����������� ������
/// </summary>
public class CheckPoint : MonoBehaviour, ICulling
{   
    [SerializeField]
    [Tooltip("�����, �� ������� ���������� �����")]
    private Transform revivePoint = null;

    /// <summary>
    /// �����, ������� ����� �����, ����� ������������ ����������� �����
    /// </summary>
    private readonly List<Enemy> enemies = new List<Enemy>();

    /// <summary>
    /// ������ ������ ��� �������������� ��� ����������� � ����������� �����
    /// </summary>
    private readonly List<GameObjectSaveableData> enemiesSaveableData = new List<GameObjectSaveableData>();

    private Coroutine _cullingRefreshCoroutine;

    private bool _cullingActive = true;

    /// <summary>
    /// ������ ������ ��� �������������� ��� ����������� � ����������� �����
    /// </summary>
    public List<GameObjectSaveableData> EnemySaveableDataList => enemiesSaveableData;

    public Transform RevivePoint => revivePoint;

    public List<Enemy> Enemies => enemies;

    public ICulling CullingObject => this;

    float ICulling.Size => EnemyManager.GetMaxFovDistance(Enemies, true);

    bool ICulling.Active => _cullingActive;

    /// <summary>
    /// �������, ���������� � ������ ������� ������ ������������ ����������� �����
    /// </summary>
    private event Action<CheckPoint> ActivationRequested;

    void ICulling.SetActive(bool active)
    {
        if (CullingObject.Active == active)
        {
            return;
        }

        _cullingActive = active;

        if (GetAliveEnemiesCount() == 0)
        {
            return;
        }

        if (_cullingRefreshCoroutine != null)
        {
            StopCoroutine(_cullingRefreshCoroutine);
        }
        _cullingRefreshCoroutine = StartCoroutine(SetActiveEnemies(active));
    }

    bool ICulling.GetNearestBodyPoint(Vector3 point, out Vector3 nearestPoint)
    {
        nearestPoint = point;

        if (enemies.Count == 0)
        {
            nearestPoint = point;
            return false;
        }

        Enemy nearestEnemy = null;

        foreach (var enemy in enemies)
        {
            if (enemy.IsAlive())
            {
                nearestEnemy = enemy;
                break;
            }
        }

        if (nearestEnemy == null)
        {
            return false;
        }

        float sqrNearestDistance = VectorFunc.GetSqrDistanceXZ(point, nearestEnemy.Transform.position);

        foreach (var enemy in enemies)
        {
            if (enemy == nearestEnemy || !enemy.IsAlive())
            {
                continue;
            }

            float sqrDistance = VectorFunc.GetSqrDistanceXZ(point, enemy.Transform.position);
            if (sqrDistance < sqrNearestDistance)
            {
                sqrNearestDistance = sqrDistance;
                nearestEnemy = enemy;
            }
        }

        nearestPoint = nearestEnemy.Transform.position;
        return true;
    }

    /// <summary>
    /// �������� ��������� ������� ������� ��������� ����������� �����
    /// </summary>
    /// <param name="listener"></param>
    public void RegisterActivationRequestedListener(Action<CheckPoint> listener)
    {
        ActivationRequested += listener;
    }

    /// <summary>
    /// ������� ��������� ������� ������� ��������� ����������� �����
    /// </summary>
    /// <param name="listener"></param>
    public void UnregisterActivationRequestedListener(Action<CheckPoint> listener)
    {
        ActivationRequested -= listener;
    }

    /// <summary>
    /// ��������� ��������� ����������� �����.
    /// �������� ���� �� �������� �����������, ����� ����� ������������ � ���
    /// </summary>
    public void RequestActivation()
    {
        ActivationRequested?.Invoke(this);
    }

    /// <summary>
    /// �������� ����� � ������ �������� �������������� ����������� �����
    /// </summary>
    /// <param name="enemy"></param>
    public void AddEnemy(Enemy enemy)
    {
        enemiesSaveableData.Add(new GameObjectSaveableData(enemy.Transform.gameObject));
        enemies.Add(enemy);
    }

    /// <summary>
    /// �������� ���������� ������, ������� �������� ����� ��� ��������� ����������� �����
    /// </summary>
    /// <returns></returns>
    public int GetAliveEnemiesCount()
    {
        int aliveEnemiesCount = 0;
        foreach (var enemy in enemies)
        {
            if (enemy.IsAlive())
            {
                aliveEnemiesCount++;
            }
        }
        return aliveEnemiesCount;
    }

    private IEnumerator SetActiveEnemies(bool active)
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            Enemy enemy = enemies[i];
            if (!enemy.IsAlive())
            {
                continue;
            }

            if (EnemySaveableDataList[i].gameObject.activeSelf == active)
            {
                continue;
            }

            EnemySaveableDataList[i].gameObject.SetActive(active);

            if (active)
            {
                EnemySaveableDataList[i].LoadData();
            }

            yield return null;
        }
        _cullingRefreshCoroutine = null;
    }
}
