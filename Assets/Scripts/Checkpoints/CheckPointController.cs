﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Контроллер контрольных точек.
/// Хранит контрольные точки, из которых может возродиться персонаж.
/// Восстанавливает состояние игры при возрождении на котнтрольной точке
/// </summary>
public class CheckPointController : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Слои, на которых располагаются коллайдеры зон врагов для контрольных точек")]
    private LayerMask checkPointAreaLayers;

    [SerializeField]
    [Tooltip("Включить отладочную отрисоку объектов контрольных точек")]
    private bool debugDraw = true;

    /// <summary>
    /// Список контрольных точек
    /// </summary>
    private List<CheckPoint> checkPoints = null;

    /// <summary>
    /// Данные для восстановления игрока
    /// </summary>
    private GameObjectSaveableData playerSaveableObject;

    /// <summary>
    /// Данные для восстановления состояния пуль врагов
    /// </summary>
    private GameObjectSaveableData bulletPoolSaveableData;

    /// <summary>
    /// Индекс текущей активированной контрольной точки
    /// </summary>
    private int currentActiveCheckpointIndex = -1;

    /// <summary>
    /// Отладочные меши объектоы контрольных точек
    /// </summary>
    private MeshRenderer[] debugMeshRenderers = null;

    /// <summary>
    /// Текущее состояние отладочной отрисовки
    /// </summary>
    private bool currentDebugDrawState = false;

    private void Awake()
    {
        // Автоматически находим чекпоинты в дочерних объектах.
        checkPoints = new List<CheckPoint>();
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).TryGetComponent(out CheckPoint checkPoint))
            {
                checkPoints.Add(checkPoint);
            }
        }

        debugMeshRenderers = GetComponentsInChildren<MeshRenderer>();
        SetDebugDrawState(debugDraw);
    }

    private void Start()
    {
        playerSaveableObject = new GameObjectSaveableData(GameManager.Instance.Player.gameObject);
        bulletPoolSaveableData = new GameObjectSaveableData(BulletPoolContainerSingleton.Instance.gameObject);
        foreach (var item in checkPoints)
        {
            item.RegisterActivationRequestedListener(OnCheckPointActivationRequested);
        }

        SetEnemiesToCheckpoints();          
    }

    private void OnDestroy()
    {
        foreach (var item in checkPoints)
        {
            item.UnregisterActivationRequestedListener(OnCheckPointActivationRequested);
        }
    }

    private void Update()
    {
        if (debugDraw != currentDebugDrawState)
        {
            SetDebugDrawState(debugDraw);
        }
    }

    /// <summary>
    /// Включить/выключить отладочную отрисовку
    /// </summary>
    /// <param name="enabled">Отрисовка включена/выключена</param>
    private void SetDebugDrawState(bool enabled)
    {
        foreach (var meshRenderer in debugMeshRenderers)
        {
            meshRenderer.enabled = enabled;
        }
        currentDebugDrawState = enabled;
    }

    /// <summary>
    /// Получить индекс контрольной точки
    /// </summary>
    /// <param name="checkPoint"></param>
    /// <returns></returns>
    private int GetCheckPointIndex(CheckPoint checkPoint)
    {
        return checkPoints.IndexOf(checkPoint);
    }

    /// <summary>
    /// Проверить, активирована ли контрольная точка
    /// </summary>
    /// <param name="checkPointIndex"></param>
    /// <returns></returns>
    private bool IsCheckPointActivated(int checkPointIndex)
    {
        return checkPointIndex <= currentActiveCheckpointIndex;
    }

    /// <summary>
    /// Проверить, является ли контрольная точка следующей точкой, которую можно активировать
    /// </summary>
    /// <param name="checkPointIndex"></param>
    /// <returns></returns>
    private bool IsNextCheckPointToActivate(int checkPointIndex)
    {
        return checkPointIndex == currentActiveCheckpointIndex + 1;
    }

    /// <summary>
    /// Проверить, может ли игрок возродиться с какой-либо контрольной точки.
    /// Возвращает true, если игрок активировал хотя бы одну контрольную точку,
    /// false - в противном случае
    /// </summary>
    /// <returns></returns>
    public bool CanReviveFromCheckPoint()
    {
        return currentActiveCheckpointIndex != -1;
    }

    /// <summary>
    /// Загрузить состояние мира на последней активированной контрольной точке
    /// </summary>
    public void LoadLastCheckPointState()
    {
        // Восстанавливаем состояние пула для пуль врагов, чтобы исключить ситуацию присутствия старых
        // пуль на сцене при загрузке. Это может произойти, когда игрок умер,
        // но кнопку загрузки нажали раньше, чем все пули попали по нему.
        bulletPoolSaveableData.LoadData();
        
        // Восстанавливаем врагов.
        // Игрок мог убить врагов, которые принадлежат ещё не активированным контрольным точкам,
        // Их нужно восстановить, т.к. на момент активации последней контрольной точки они живы.
        for (int i = currentActiveCheckpointIndex + 1; i < checkPoints.Count; i++)
        {
            foreach (var enemy in checkPoints[i].EnemySaveableDataList)
            {
                enemy.gameObject.SetActive(true);
                enemy.LoadData();
            }
        }

        // Восстанавливаем игрока. По умолчанию он будет восстановлен в начале уровня.
        playerSaveableObject.LoadData();
        
        // Если игрок активировал хотя бы одну контрольную точку, игрок будет перенесён туда. 
        if (CanReviveFromCheckPoint())
        {
            Transform playerTransform = playerSaveableObject.gameObject.transform;
            Vector3 revivePosition = 
                checkPoints[currentActiveCheckpointIndex].RevivePoint.position;
            revivePosition.y = playerTransform.position.y;
            playerTransform.position = revivePosition;
        }
    }

    /// <summary>
    /// Обработка события попытки активации контрольной точки игроком 
    /// </summary>
    /// <param name="checkPoint">Контрольная точка, которую игрок пытается активировать</param>
    private void OnCheckPointActivationRequested(CheckPoint checkPoint)
    {
        int aliveEnemiesCount = checkPoint.GetAliveEnemiesCount();
        if (aliveEnemiesCount > 0)
        {
            //Debug.Log($"{checkPoint.name}: {aliveEnemiesCount} enemies left to defeat");
            return;
        }

        int checkPointIndex = GetCheckPointIndex(checkPoint);

        if (IsCheckPointActivated(checkPointIndex))
        {
            //Debug.Log($"{checkPoint.name}: Check point is already activated");
            return;
        }

        if (!IsNextCheckPointToActivate(checkPointIndex))
        {
            //Debug.Log($"{checkPoint.name}: You have to activate previous checkpoint");
            return;
        }

        currentActiveCheckpointIndex = checkPointIndex;

        //Debug.Log($"{checkPoint.name}: Checkpoint activated");
    }

    /// <summary>
    /// Назначить врагов соответствующим контрольным точкам, в зоне которых они находятся
    /// </summary>
    private void SetEnemiesToCheckpoints()
    {
        Enemy[] enemies = FindObjectsOfType<Enemy>();
        foreach (var enemy in enemies)
        {
            Vector3 topPoint = enemy.Transform.position;
            topPoint.y = VectorFunc.MaxRaycastDistance;
            if (!Physics.Raycast(topPoint, Vector3.down, out RaycastHit hitInfo,
                VectorFunc.MaxRaycastDistance * 2, checkPointAreaLayers))
            {
                continue;
            }

            if (!hitInfo.transform.TryGetComponent(out CheckPointAreaCollider areaCollider))
            {
                continue;
            }

            areaCollider.Owner.AddEnemy(enemy);
        }
    }
}
