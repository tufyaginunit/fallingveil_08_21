using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ������� ������, ��������� ��������� � ��������� ��� ���������.
/// ������ ���� �������� �� ������� ������ (Game object), ������� ���������� ���������/���������.
/// ���� ISaveable ���������� ������ � ������ gameobject. ���� ���������� ��������� �������� �������,
/// �� ���������� �������� ���� ��������� SaveableEntity
/// </summary>
public class SaveableEntity : MonoBehaviour
{
    /// <summary>
    /// ������� ����������� ����������� ������� gameobject, �������� �� ��������� ��� ��������������
    /// </summary>
    private Dictionary<ISaveable, object> savingObjects = new Dictionary<ISaveable, object>();

    /// <summary>
    /// ������ ����������� �����������, �������������� �� ������ gameobject
    /// </summary>
    private ISaveable[] saveables = null;

    private void Awake()
    {
        saveables = GetComponents<ISaveable>();
        foreach (var item in saveables)
        {
            savingObjects[item] = null;
        }      
    }

    /// <summary>
    /// ��������� ��������� �����������
    /// </summary>
    public void Save()
    {
        foreach (var item in saveables)
        {
            savingObjects[item] = item.GetState();
        }
    }

    /// <summary>
    /// ��������� ����� ����������� ��������� �����������
    /// </summary>
    public void Load()
    {
        foreach (var item in savingObjects)
        {
            item.Key.SetState(savingObjects[item.Key]);
        }
    }
}
