using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// �������, ������������ ����������� ����� ��� ��������� � ���� ������
/// </summary>
[RequireComponent(typeof(Collider))]
public class CheckPointTrigger : MonoBehaviour
{
    /// <summary>
    /// ����������� �����-�������� ��������
    /// </summary>
    public CheckPoint Owner { get; private set; }

    /// <summary>
    /// ���������������� �� ��������� ������� ������ ������ ���������
    /// </summary>
    private bool enemyListenersRegistered = false;

    /// <summary>
    /// ���������� �� �������� ����������� ���������
    /// </summary>
    private bool needCheckActivation = false;

    private void Awake()
    {
        Owner = GetComponentInParent<CheckPoint>();
    }

    private void OnTriggerEnter(Collider other)
    {
        // ��� ������ ������ ������ � ���� ��������� �������������� ���������� �������
        // ������ ������, ����� ���� �������� ����� ������������ �������� ����� ����������� ���� ������
        if (!enemyListenersRegistered)
        {
            foreach (var item in Owner.Enemies)
            {
                item.RegisterKilledListener(OnEnemyKilled);
            }
            enemyListenersRegistered = true;
        }

        // ��� ��������� ������ � ������� ����������� � ����������� ����� ���������
        if (other.transform == GameManager.Instance.Player.Transform)
        {
            Owner.RequestActivation();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (needCheckActivation && other.transform == GameManager.Instance.Player.Transform)
        {
            Owner.RequestActivation();
            needCheckActivation = false;
        }
    }

    private void OnDisable()
    {
        if (enemyListenersRegistered)
        {
            foreach (var item in Owner.Enemies)
            {
                item.UnregisterKilledListener(OnEnemyKilled);
            }
            enemyListenersRegistered = false;
        }
    }

    /// <summary>
    /// ���������� ������� ������ ������ � ���� ���������.
    /// ������ ��� ���������� ��������� ��������� ��� �������� ������� ���� ������
    /// (��� �������, ��� �� ����� � ���� ���������)
    /// </summary>
    private void OnEnemyKilled()
    {
        needCheckActivation = true;
    }
}
