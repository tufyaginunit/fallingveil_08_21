﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorController : MonoBehaviour
{
    public enum CursorState { CanShoot = 0, NoShooting = 1, Menu = 2}
        
    [SerializeField] private CursorState activeCursorState;
    [SerializeField] private Texture2D[] cursors;

    [SerializeField] private PlayerInput playerInput;
    [SerializeField] private WeaponController weaponController;

    public CursorState ActiveCursorState
    {
        get { return activeCursorState; }
        set
        {
            if (activeCursorState != value)
            {
                activeCursorState = value;
                CursorActiveStatePropertyChanged();
            }
        }
    }

    /// <summary>
    /// Цель под курсором (или в радиусе автоприцеливания)
    /// </summary>
    public IDamageable CursorTarget { get; private set; }

    /// <summary>
    /// Включено ли отображение курсора
    /// </summary>
    public bool CursorEnabled => Cursor.visible;

    /// <summary>
    /// Включить/выключить отображение курсора
    /// </summary>
    /// <param name="enabled"></param>
    public void SetCursorEnabled(bool enabled)
    {
        Cursor.visible = enabled;
    }

    void Start()
    {        
        CursorActiveStatePropertyChanged();
    }

    private void CursorActiveStatePropertyChanged()
    {
        float xPos = cursors[(int)activeCursorState].width / 2;
        float yPos = cursors[(int)activeCursorState].height / 2;

        Cursor.SetCursor(cursors[(int)activeCursorState], new Vector2(xPos, yPos), CursorMode.ForceSoftware);        
    }

    
    private void Update()
    {
        if (!Cursor.visible || GameManager.Instance.Controller.GamePaused 
            || !playerInput.IsInputAllowed(AllowedInput.Attack)
            || !GameManager.Instance.Player.IsAlive())
        {
            ActiveCursorState = CursorState.Menu;
            CursorTarget = null;
            return;
        }

        CursorTarget = playerInput.GetTarget();
        if (CursorTarget == null)
        {           
            ActiveCursorState = CursorState.NoShooting;
        }
        else
        {
            if (weaponController.CheckTargetAttackDistance(CursorTarget) == AttackCheckResult.Ok
                && !weaponController.ThereAreObstaclesOnAttackWay(CursorTarget) && !weaponController.IsCoolDowning)
            {
                ActiveCursorState = CursorState.CanShoot;
            }
            else ActiveCursorState = CursorState.NoShooting;
        }
    }

    
}
