﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Контроллер звуков атакующего луча
/// </summary>
public class AttackBeamSoundController : MonoBehaviour
{
    /// <summary>
    /// Время, за которое громкость звука плавно изменяется от минимального значения до максимального
    /// (или наоборот)
    /// </summary>
    const float VolumeChangeDuration = 0.2f;

    [SerializeField, Tooltip("Атакующий луч")]
    private AttackBeam attackBeam = null;

    [SerializeField, Tooltip("Звук прерывания луча. Проигрывается для индикации того, что " +
        "враг скоро уйдёт на дистанцию, куда луч до него не достанет")]
    private AudioSource interruptionSound = null;

    [SerializeField, Tooltip("Расстояние до максимальной длины луча, на котором начинает воспроизводиться" +
        "звук прерывания луча")]
    private float startInterruptionDistanceFromEnd = 5;

    [SerializeField, Tooltip("Звук, проигрываемый в момент обрыва луча, когда враг скрылся за препятствием")]
    private AudioSource breakSound = null;

    [SerializeField, Tooltip("Звук луча, воспроизводимый на протяжении нанесении урона по цели")]
    private AudioSource hittingTargetSound = null;

    [SerializeField, Tooltip("Звук луча, воспроизводимый на протяжении атаки в пустоту или в препятствие")]
    private AudioSource hittingNoTargetSound = null;

    /// <summary>
    /// Текущий звук атаки луча
    /// </summary>
    private AudioSource currentHittingSound = null;

    /// <summary>
    /// Список звуков атаки луча
    /// </summary>
    private readonly List<AudioSource> hittingSounds = new List<AudioSource>();
   
    /// <summary>
    /// Словарь, задающий соответствие между звуками и их максимальной громкостью
    /// </summary>
    private readonly Dictionary<AudioSource, float> maxSoundsVolume = new Dictionary<AudioSource, float>();

    private void Awake()
    {
        hittingSounds.Add(hittingTargetSound);
        hittingSounds.Add(hittingNoTargetSound);

        maxSoundsVolume.Add(hittingTargetSound, hittingTargetSound.volume);
        maxSoundsVolume.Add(hittingNoTargetSound, hittingNoTargetSound.volume);
        maxSoundsVolume.Add(interruptionSound, interruptionSound.volume);
    }

    private void OnEnable()
    {
        attackBeam.RegisterHasInterruptedListener(OnBeamHasInterrupted);
    }

    private void OnDisable()
    {
        if (attackBeam != null)
        {
            attackBeam.UnregisterHasInterruptedListener(OnBeamHasInterrupted);
        }       
    }

    private void Update()
    {
        RefreshHittingSound();
        RefreshInterruptionSound();
    }

    /// <summary>
    /// Обработчик события, когда враг оборвал луч о препятствие
    /// </summary>
    private void OnBeamHasInterrupted()
    {
        SoundFunc.Play(breakSound);
    }

    /// <summary>
    /// Обновить звук атаки луча
    /// </summary>
    private void RefreshHittingSound()
    {
        if (attackBeam.IsAttacking())
        {
            SetActiveHittingSound(attackBeam.GetTarget() != null
                ? hittingTargetSound : hittingNoTargetSound);
        }
        else
        {
            SetActiveHittingSound(null);
        }
    }

    /// <summary>
    /// Обновить звук прерывания луча
    /// </summary>
    private void RefreshInterruptionSound()
    {
        if (attackBeam.GetTarget() == null)
        {
            SoundFunc.Stop(interruptionSound);
            return;
        }

        float currentDistanceFromEnd = attackBeam.MaxDistance - attackBeam.CurrentDistance;
        if (currentDistanceFromEnd >= startInterruptionDistanceFromEnd)
        {
            SoundFunc.Stop(interruptionSound);
            return;
        }

        SoundFunc.Play(interruptionSound);

        float maxVolume = GetMaxVolume(interruptionSound);
        interruptionSound.volume = maxVolume - (currentDistanceFromEnd / startInterruptionDistanceFromEnd) * maxVolume;
    }

    /// <summary>
    /// Установить активный звук атаки луча
    /// </summary>
    /// <param name="hittingSound"></param>
    private void SetActiveHittingSound(AudioSource hittingSound)
    {
        // Звуки атаки синхронизированы друг с другом.
        // Идея в том, что одновременно будут воспроизводится все звуки атаки (атака по цели и атака в пустоту),
        // но с разной громкостью: у активного громкость плавно изменится с 0 до 1,
        // у остальных - останется нулевой.
        // Это делается для того, чтобы звуки могли плавно перетекать один в другой, в зависимости от
        // текущей ситуации.

        if (hittingSound == currentHittingSound)
        {
            return;
        }
        
        StopAllCoroutines();

        if (hittingSound == null)
        {
            // Указан нулевой звук, останавливаем все звуки атаки луча
            foreach (var sound in hittingSounds)
            {
                StartCoroutine(SmoothVolumeChanging(sound, -1));
            }

            currentHittingSound = hittingSound;
            return;
        }

        if (currentHittingSound == null)
        {
            // Текущего воспроизводимого звука луча нет, воспроизводим все звуки атаки, но
            // с разной громкостью: у активного громкость плавно изменится с 0 до 1,
            // у остальных останется нулевой
            foreach (var sound in hittingSounds)
            {
                sound.volume = 0;
                SoundFunc.Stop(sound);
                SoundFunc.Play(sound);
                if (sound == hittingSound)
                {
                    StartCoroutine(SmoothVolumeChanging(sound, 1));
                }                
            }

            currentHittingSound = hittingSound;
            return;
        }

        // Есть текущий воспроизводимый звук и новый звук, который нужно воспроизвести.
        // Плавно уменьшаем громкость первого и увеличиваем у второго.
        foreach (var sound in hittingSounds)
        {
            if (sound == hittingSound)
            {
                StartCoroutine(SmoothVolumeChanging(sound, 1));
            }
            else
            {
                StartCoroutine(SmoothVolumeChanging(sound, -1));
            }
            SoundFunc.Play(sound);
        }

        currentHittingSound = hittingSound;
    }

    /// <summary>
    /// Корутина плавного изменения громкости указанного звука
    /// </summary>
    /// <param name="hittingSound"></param>
    /// <param name="direction">1 - увеличить громкость, -1 - уменьшить громкость</param>
    /// <returns></returns>
    private IEnumerator SmoothVolumeChanging(AudioSource hittingSound, int direction)
    {
        if (hittingSound == null)
        {
            yield break;
        }

        float maxVolume = GetMaxVolume(hittingSound);

        float changeSpeed = maxVolume / VolumeChangeDuration;

        float currentVolume = hittingSound.volume;
        while (true)
        {
            yield return null;

            currentVolume += direction * changeSpeed * Time.deltaTime;

            if (direction < 0)
            {
                if (currentVolume <= 0)
                {
                    hittingSound.volume = 0;
                    break;
                }              
            }
            else
            {
                if (currentVolume >= maxVolume)
                {
                    hittingSound.volume = maxVolume;
                    break;
                }
            }

            hittingSound.volume = currentVolume;
        }
    }

    /// <summary>
    /// Получить максимальную громкость указанного звука
    /// </summary>
    /// <param name="sound"></param>
    /// <returns></returns>
    private float GetMaxVolume(AudioSource sound)
    {
        if (!maxSoundsVolume.TryGetValue(sound, out float maxVolume))
        {
            maxVolume = 1;
        }
        return maxVolume;
    }
}
