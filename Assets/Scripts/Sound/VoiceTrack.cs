﻿using UnityEngine;

/// <summary>
/// Голосовая звуковая дорожка (звуковая дорожка, сопровождаемая текстом)
/// </summary>
public class VoiceTrack : MonoBehaviour
{
    [field: SerializeField, Tooltip("Голосовая дорожка")]
    public AudioSource SpeechAudioSource { get; private set; } = null;

    [field: SerializeField, Tooltip("Текст голосовой дорожки")]
    public string SpeechText { get; private set; } = "";

    public bool IsPrefab => gameObject != null && gameObject.scene.name == null;

    /// <summary>
    /// Проверить, вопроизводится ли сейчас звуковая дорожка
    /// </summary>
    /// <returns></returns>
    public bool IsPlaying()
    {
        return SpeechAudioSource.isPlaying;
    }

    /// <summary>
    /// Вопроизвести звуковую дорожку
    /// </summary>
    /// <returns></returns>
    public void Play()
    {
        SoundFunc.Play(SpeechAudioSource);
    }
}
