﻿using UnityEngine;

/// <summary>
/// Контроллер звуковых эффектов врагов
/// </summary>
public class EnemySoundController : MonoBehaviour
{
    [SerializeField, Tooltip("Объект врага")]
    private Enemy enemy = null;

    [SerializeField, Tooltip("Звук обнаружения цели")]
    private AudioSource targetDetectedSound = null;

    private void OnEnable()
    {
        if (enemy != null)
        {
            enemy.RegisterStateChangedListener(OnEnemyStateChanged);
        }      
    }

    private void OnDisable()
    {
        if (enemy != null)
        {
            enemy.UnregisterStateChangedListener(OnEnemyStateChanged);
        }
    }

    /// <summary>
    /// Обработчик события изменения состояния врага
    /// </summary>
    private void OnEnemyStateChanged()
    {
        if (enemy.PreviousState == AIState.Patroling &&
            (enemy.CurrentState == AIState.Chasing || enemy.CurrentState == AIState.Attacking ||
            enemy.CurrentState == AIState.Searching))
        {
            SoundFunc.Play(targetDetectedSound);
        }
    }
}
