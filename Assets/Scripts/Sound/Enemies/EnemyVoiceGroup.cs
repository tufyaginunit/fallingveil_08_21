﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Голосовые эффекты, относящиеся к определенной группе врагов
/// </summary>
public class EnemyVoiceGroup : MonoBehaviour
{
    const float DelayToSpeakAfterRetreating = 0.5f;
    
    [SerializeField, Tooltip("Шанс проигрывания голоса"), Range(0, 1)]
    private float chance = 1;

    [SerializeField, Tooltip("Список голосовых эффектов, относящихся к группе врагов")]
    private VoiceTrack[] voiceTracks = null;

    [SerializeField, Tooltip("Минимальное значение параметра pitch (тональности) звука")]
    private float minPitch = 0.9f;

    [SerializeField, Tooltip("Максимальное значение параметра pitch (тональности) звука")]
    private float maxPitch = 1.3f;

    [SerializeField, Tooltip("Голосовые эффекты действуют на всех врагов на карте")]
    private bool forAllEnemies = false;

    [SerializeField, Tooltip("Список врагов группы (используется, только если не поставлен флаг forAllEnemies)")]
    private Enemy[] groupEnemies = null;

    /// <summary>
    /// Контроллер голосовых звуковых эффектов групп врагов
    /// </summary>
    private EnemyVoiceController voiceController = null;

    /// <summary>
    /// Массив перемешенных голосовых эффектов для воспроизведения в случайном порядке
    /// </summary>
    private ShuffleEnumerator<VoiceTrack> voicePlayList = null;

    private static readonly System.Random random = new System.Random();

    private IEnumerable<Enemy> enemies = null;

    private float canSpeakTime = 0;
    /// <summary>
    /// Проверить, воспроизводится ли сейчас какой-либо голосовой эффект у данной группы
    /// </summary>
    /// <returns></returns>
    public bool IsPlaying()
    {
        foreach (var item in voiceTracks)
        {
            if (item.IsPlaying())
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Воспроизвести очередную голосовую дорожку
    /// </summary>
    public void PlayNext()
    {
        // Чтобы немного разнообразить голоса, будем немного менять тональность звука
        voicePlayList.Current.SpeechAudioSource.pitch = 
            minPitch + (float)random.NextDouble() * (maxPitch - minPitch);
        voicePlayList.Current.Play();
        voicePlayList.MoveNextLoop();
    }

    private void Awake()
    {
        for (int i = 0; i < voiceTracks.Length; i++)
        {
            if (voiceTracks[i].IsPrefab)
            {
                voiceTracks[i] = Instantiate(voiceTracks[i], transform.position, Quaternion.identity);
            }           
        }
        voicePlayList = new ShuffleEnumerator<VoiceTrack>(voiceTracks);
        voiceController = GetComponentInParent<EnemyVoiceController>();
        voiceController.AddVoiceGroup(this);
    }

    private void Start()
    {
        if (forAllEnemies)
        {
            enemies = EnemyManager.Instance.Enemies;
        }
        else
        {
            enemies = groupEnemies;
        }

        foreach (var enemy in enemies)
        {
            enemy.RegisterStateChangedListener(() => OnStateChanged(enemy));
        }
    }

    private void OnDestroy()
    {
        voiceController.RemoveVoiceGroup(this);

        foreach (var enemy in enemies)
        {
            enemy.UnregisterStateChangedListener(() => OnStateChanged(enemy));
        }
    }

    private void OnStateChanged(Enemy enemy)
    {
        if (enemy.PreviousState == AIState.Retreating)
        {
            canSpeakTime = Time.time + DelayToSpeakAfterRetreating;
            return;
        }

        if (canSpeakTime > Time.time)
        {
            return;
        }

        if (enemy.CurrentState == AIState.Searching && 
            (enemy.PreviousState == AIState.Attacking || enemy.PreviousState == AIState.Chasing)
            && (chance == 1 || (random.NextDouble() <= chance)))
        {
            voiceController.RequestPlayVoice(enemy, this);
        }
    }
}
