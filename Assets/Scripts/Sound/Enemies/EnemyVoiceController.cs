﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Контроллер, управляющий воспроизведением голосовых треков врагов
/// </summary>
public class EnemyVoiceController : MonoBehaviour
{
    /// <summary>
    /// Список групп врагов, воспроизводящих определенные голоса
    /// </summary>
    private readonly List<EnemyVoiceGroup> enemyVoiceGroups = new List<EnemyVoiceGroup>();

    /// <summary>
    /// Воспроизводится ли сейчас какой-то голос
    /// </summary>
    /// <returns></returns>
    public bool IsEnemySoundPlaying()
    {
        foreach (var item in enemyVoiceGroups)
        {
            if (item.IsPlaying())
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Добавить голосовую группу в список контроля
    /// </summary>
    /// <param name="enemyVoiceGroup"></param>
    public void AddVoiceGroup(EnemyVoiceGroup enemyVoiceGroup)
    {
        enemyVoiceGroups.Add(enemyVoiceGroup);
    }

    /// <summary>
    /// Удалить голосовую группу из списка контроля
    /// </summary>
    /// <param name="enemyVoiceGroup"></param>
    public void RemoveVoiceGroup(EnemyVoiceGroup enemyVoiceGroup)
    {
        enemyVoiceGroups.Remove(enemyVoiceGroup);
    }

    /// <summary>
    /// Запросить воспроизведение голоса.
    /// Если никакой голос сейчас не воспроизводится, то будет выполнено воспроизведение голоса
    /// соответствующего врага. Иначе запрос будет отклонён.
    /// </summary>
    /// <param name="enemy"></param>
    /// <param name="enemyVoiceGroup"></param>
    public void RequestPlayVoice(Enemy enemy, EnemyVoiceGroup enemyVoiceGroup)
    {
        if (!IsEnemySoundPlaying())
        {
            enemyVoiceGroup.PlayNext();
        }
    }
}
