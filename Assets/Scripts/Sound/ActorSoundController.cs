﻿using UnityEngine;

/// <summary>
/// Контроллер звуковых эффектов для персонажей
/// </summary>
public class ActorSoundController : MonoBehaviour
{
    [SerializeField, Tooltip("Объект HP персонажа")]
    private DamageableObject actor = null;

    [SerializeField, Tooltip("Звук получения персонажем повреждения")]
    private AudioSource hitSound = null;

    [SerializeField, Tooltip("Звук смерти персонажа")]
    private AudioSource deathSound = null;

    [SerializeField, Tooltip("Звук регенерации персонажа в результате убийства врага")]
    private AudioSource healedSound = null;

    [SerializeField, Tooltip("Звук низкого количества HP")]
    private AudioSource lowHPSound = null;

    [SerializeField, Tooltip("Значение HP, ниже которого начинает проигрываться звук низкого количества HP")]
    private float lowHPValue = 20;

    /// <summary>
    /// Последнее учтённое значение HP
    /// </summary>
    private int lastAppliedHP = 0;

    private void OnEnable()
    {
        if (actor != null)
        {
            actor.RegisterHittedListener(OnActorHitted);
            actor.RegisterHealedListener(OnActorHealed);
            actor.RegisterRevivedListener(OnActorRevived);
        }
    }

    private void OnDisable()
    {
        if (actor != null)
        {
            actor.UnregisterHittedListener(OnActorHitted);
            actor.UnregisterHealedListener(OnActorHealed);
            actor.UnregisterRevivedListener(OnActorRevived);
        }
    }

    private void Start()
    {
        lastAppliedHP = actor.Health;
    }

    private void Update()
    {
        if (lowHPSound == null)
        {
            return;
        }

        if (actor.Health < lowHPValue && actor.IsAlive())
        {
            SoundFunc.Play(lowHPSound);
        }
        else
        {
            SoundFunc.Stop(lowHPSound);
        }
    }

    /// <summary>
    /// Обработать событие получения урона
    /// </summary>
    private void OnActorHitted()
    {
        if (actor.Health <= 0 && lastAppliedHP <= 0)
        {
            return;
        }

        lastAppliedHP = actor.Health;

        if (actor.IsAlive())
        {
            SoundFunc.Play(hitSound);
        }
        else
        {
            SoundFunc.Play(deathSound);
        }
    }

    /// <summary>
    /// Обработать событие восполнения здоровья в результате убийства врага
    /// </summary>
    private void OnActorHealed()
    {
        SoundFunc.Play(healedSound);
    }

    /// <summary>
    /// Обработать событие воскрешения
    /// </summary>
    private void OnActorRevived()
    {
        lastAppliedHP = actor.Health;
    }
}
