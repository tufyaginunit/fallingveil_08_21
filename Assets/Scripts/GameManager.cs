﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Синглтон-класс менеджера игры. 
/// Предоставляет доступ к некоторым игровым объектам, к которым нужно обращаться 
/// из разных скриптов.
/// Здесь же задаются глобальные параметры игры.
/// </summary>
public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    [Tooltip("Главный контроллер игры")]
    private GameController gameController = null;

    [SerializeField]
    [Tooltip("Имя главной сцены игры")]
    private string gameSceneName = null;

    [SerializeField]
    [Tooltip("Объект HP игрока")]
    private DamageableObject playerHealthObject = null;

    [SerializeField]
    [Tooltip("Контроллер оружия игрока")]
    private WeaponController playerWeaponController = null;

    [SerializeField]
    [Tooltip("Объект движения игрока")]
    private MovingObject playerMovingObject = null;

    [SerializeField]
    [Tooltip("Слои, на которых расположены все возможные препятствия")]
    private LayerMask obstacleLayers;

    [SerializeField]
    [Tooltip("Слои, на которых расположены препятствия, изменяющие свою видимость")]
    private LayerMask changingObstacleLayers;

    [SerializeField]
    [Tooltip("Слои, на которых расположены враги")]
    private LayerMask enemyLayers;

    [SerializeField]
    [Tooltip("Уровень земли")]
    private float groundY = 0;

    [Tooltip("Высота точки атаки игрока и врагов над уровнем земли")]
    [SerializeField]
    private float attackPointHeightFromGround = 1;

    /// <summary>
    /// Плоскость, в которой двигается игрок
    /// </summary>
    private Plane? playerMovingPlane;

    /// <summary>
    /// Ссылка на главную камеру
    /// </summary>
    private Camera mainCamera;

    public GameController Controller => gameController;
    public string GameSceneName => gameSceneName;
    public DamageableObject Player => playerHealthObject;
    public WeaponController PlayerWeapon => playerWeaponController;
    public MovingObject PlayerMovingObject => playerMovingObject;
    public LayerMask ObstacleLayers => obstacleLayers;
    public LayerMask ChangingObstacleLayers => changingObstacleLayers;
    public LayerMask EnemyLayers => enemyLayers;
    public float GroundY => groundY;

    // Абсолютная Y-координата точки атаки игрока и врагов
    public float AttackPointY => groundY + attackPointHeightFromGround;

    public Plane PlayerMovingPlane
    {
        get
        {
            if (playerMovingPlane == null)
            {
                playerMovingPlane = new Plane(Vector3.up, transform.position +
                   new Vector3(0, playerWeaponController.AttackPoint.position.y, 0));
            }
            return playerMovingPlane.Value;
        }
    }

    public Camera MainCamera
    {
        get
        {
            if (mainCamera == null)
            {
                mainCamera = Camera.main;
            }
            return mainCamera;
        }
    }

    /// <summary>
    /// Получить проекцию экранной точки на плоскость XZ, в которой находится игрок
    /// </summary>
    /// <param name="screenPointPosition">Экранные координаты курсора</param>
    /// <returns></returns>
    public Vector3 GetGroundPositionFromScreen(Vector3 screenPointPosition)
    {
        Ray cameraToCursorRay = MainCamera.ScreenPointToRay(screenPointPosition);
        if (PlayerMovingPlane.Raycast(cameraToCursorRay, out float hitDistance))
        {
            return cameraToCursorRay.GetPoint(hitDistance);
        }
        return Vector3.zero;
    }
}
