﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс, служащий для выделения цели на экране.
/// Включает, отключает и переносит объект drawingTransform, в зависимости от того, какая цель сейчас выбрана
/// </summary>
public class PlayerTargetDrawer : MonoBehaviour
{    
    [SerializeField, Tooltip("Трансформ, используемый для отображения выбранной цели, если её можно атаковать")]
    private Transform canAttackDrawingTransform;

    [SerializeField, Tooltip("Трансформ, используемый для отображения выбранной цели, если её нельзя атаковать")]
    private Transform canNotAttackDrawingTransform;

    /// <summary>
    /// Список трансформов отображения цели
    /// </summary>
    private readonly List<Transform> drawingTransforms = new List<Transform>();

    /// <summary>
    /// Контроллер курсора
    /// </summary>
    private CursorController cursorController = null;

    private void Start()
    {
        cursorController = GameManager.Instance.Controller.CursorController;

        drawingTransforms.Add(canAttackDrawingTransform);
        drawingTransforms.Add(canNotAttackDrawingTransform);
    }

    private void Update()
    {
        IDamageable target = cursorController.CursorTarget;
        if (target == null)
        {
            ShowDrawingTransform(null);
            return;
        }

        Transform drawingTransform = 
            cursorController.ActiveCursorState == CursorController.CursorState.CanShoot ?
            canAttackDrawingTransform : canNotAttackDrawingTransform;

        Vector3 drawerPosition = target.Transform.position;
        drawerPosition.y = GameManager.Instance.GroundY + 0.1f;
        drawingTransform.position = drawerPosition;

        ShowDrawingTransform(drawingTransform);
    }

    /// <summary>
    /// Показать указанный трансформ отображения цели. При этом скрываются остальные трансформы
    /// </summary>
    /// <param name="drawingTransform"></param>
    private void ShowDrawingTransform(Transform drawingTransform)
    {
        foreach (var item in drawingTransforms)
        {
            item.gameObject.SetActive(item == drawingTransform);
        }       
    }
}
