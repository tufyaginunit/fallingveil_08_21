using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ������� HP ���� ��� �������� ������
/// </summary>
public class TargetHealthSlider : MonoBehaviour
{
    [SerializeField]
    private Slider sliderValue = null;

    [SerializeField]
    private Text healthValueText = null;

    private CursorController cursorController = null;

    private void Start()
    {
        cursorController = GameManager.Instance.Controller.CursorController;
    }

    private void Update()
    {
        IDamageable target = cursorController.CursorTarget;
        if (target == null)
        {
            if (IsSliderActive())
            {
                SetSliderActive(false);
            }
            return;
        }

        sliderValue.value = target.Health / (float)target.MaxHealth;
        healthValueText.text = $"{target.Health} / {target.MaxHealth}";
        
        if (!IsSliderActive())
        {
            SetSliderActive(true);
        }        
    }

    private bool IsSliderActive()
    {
        return sliderValue.gameObject.activeSelf;
    }

    private void SetSliderActive(bool active)
    {
        sliderValue.gameObject.SetActive(active);
    }
}
