﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Слайдер, отображающий количество HP, необходимое для убийства врага под курсором
/// </summary>
public class EnoughHealthToKillSlider : MonoBehaviour
{
    [SerializeField, Tooltip("Частота миганий (количество в секунду)")]
    private float blinkingFrequencyInSecond = 1;

    [SerializeField, Tooltip("Минимальное значение альфа при мигании")]
    private float minAlpha = 0;

    [SerializeField, Tooltip("Максимальное значение альфа при мигании")]
    private float maxAlpha = 1;

    [SerializeField, Tooltip("Канвас-группа, меняющая значение альфа")]
    private CanvasGroup alphaChangeCanvasGroup = null;

    [SerializeField, Tooltip("Объект слайдера")]
    private Slider slider = null;

    [SerializeField, Tooltip("Атакующий луч игрока")]
    private SelfDamageAttackBeam selfDamageAttackBeam = null;

    /// <summary>
    /// Корутина мигания
    /// </summary>
    private Coroutine blinkingCoroutine = null;

    /// <summary>
    /// Контроллер курсора
    /// </summary>
    private CursorController cursorController;

    /// <summary>
    /// Объект HP игрока
    /// </summary>
    private DamageableObject playerHealthObject = null;

    private void Start()
    {
        cursorController = GameManager.Instance.Controller.CursorController;
        playerHealthObject = GameManager.Instance.Player;
        
        HideSlider();
    }

    private void Update()
    {
        if (cursorController.CursorTarget == null)
        {
            HideSlider();
            return;
        }

        int neededHealth = selfDamageAttackBeam.GetEnoughHealthToKill(cursorController.CursorTarget);
        float newValue = neededHealth / (float)playerHealthObject.MaxHealth;
        if (slider.value != newValue)
        {
            slider.value = newValue;
        }

        if (neededHealth > playerHealthObject.Health)
        {
            ShowSlider();
        }
        else
        {
            HideSlider();
        }

    }

    /// <summary>
    /// Показать слайдер
    /// </summary>
    private void ShowSlider()
    {
        if (blinkingCoroutine == null)
        {
            blinkingCoroutine = StartCoroutine(PerformBlinking());
        }        
    }

    /// <summary>
    /// Скрыть слайдер
    /// </summary>
    private void HideSlider()
    {
        if (blinkingCoroutine != null)
        {
            StopCoroutine(blinkingCoroutine);
            blinkingCoroutine = null;
        }
        if (alphaChangeCanvasGroup.alpha > 0)
        {
            alphaChangeCanvasGroup.alpha = 0;
        }    
    }

    private IEnumerator PerformBlinking()
    {
        float currentAlpha = maxAlpha;
        int currentDirection = -1;

        float alphaChangePerSecond = (maxAlpha - minAlpha) * blinkingFrequencyInSecond;
        while (true)
        {
            alphaChangeCanvasGroup.alpha = currentAlpha;

            yield return null;

            currentAlpha += currentDirection * alphaChangePerSecond * Time.deltaTime;

            if (currentAlpha <= minAlpha)
            {
                currentAlpha = minAlpha;
                currentDirection = 1;
            }
            else if (currentAlpha >= maxAlpha)
            {
                currentAlpha = maxAlpha;
                currentDirection = -1;
            }
        }
    }
}
