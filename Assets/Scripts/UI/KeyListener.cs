using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyListener : MonoBehaviour
{
    public enum ListenerMode
    {
        Gameplay,
        Cutscene
    }

    private ListenerMode currentMode;

    public void SetMode(ListenerMode mode)
    {
        currentMode = mode;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OnEscapeKeyPressed();
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            OnSpaceKeyPressed();
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            OnRKeyPressed();
        }
    }

    private void OnEscapeKeyPressed()
    {
        GameScreen currentScreen = GameManager.Instance.Controller.ScreenController.CurrentScreen;
        if (currentScreen != null && currentScreen.ParentScreen != null)
        {
            GameManager.Instance.Controller.ScreenController.ShowParentScreen();
            return;
        }

        if (currentMode == ListenerMode.Gameplay)
        {
            if (GameManager.Instance.Controller.GamePaused)
            {
                GameManager.Instance.Controller.ResumeGame();
            }
            else
            {
                GameManager.Instance.Controller.PauseGame();
            }
        }
        else
        {
            GameManager.Instance.Controller.InterruptCurrentCutscene();
        }
    }

    private void OnSpaceKeyPressed()
    {
        GameManager.Instance.Controller.ResumeTimeline();
    }

    private void OnRKeyPressed()
    {
        GameManager.Instance.Controller.RunFromLastCheckPoint();
    }
}
