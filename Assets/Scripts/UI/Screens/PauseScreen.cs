﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Экран паузы.
/// Скрипт следует добавлять на UI-панель, которая должна появляться, когда игра ставится на паузу.
/// </summary>
public class PauseScreen : GameScreen
{
    [SerializeField]
    [Tooltip("Кнопка возврата в игру")]
    private Button resumeButton = null;

    [SerializeField]
    [Tooltip("Кнопка выхода из игры")]
    private Button quitButton = null;

    private void Start()
    {
        resumeButton.onClick.AddListener(() => GameManager.Instance.Controller.ResumeGame());
        quitButton.onClick.AddListener(() => GameManager.Instance.Controller.Quit());
    }
}
