﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class BackScreenButton : MonoBehaviour
{   
    private Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        GameManager.Instance.Controller.ScreenController.ShowParentScreen();
    }
}
