﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ChildScreenButton: MonoBehaviour
{
    [SerializeField]
    private GameScreen childScreen;

    private Button button;

    private void Awake()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        GameManager.Instance.Controller.ScreenController.ShowChildScreen(childScreen);
    }
}
