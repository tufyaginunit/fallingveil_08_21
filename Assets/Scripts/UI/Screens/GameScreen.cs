using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ����� ������ ����. 
/// ����� ������������ ����� UI-������, �������������� ������ ���� ���� 
/// ��� ������������� ������-���� �������� �������. 
/// ��� ����� ���� ����� �������� ����, ����� � ����������� � ���������� ������, ����� Game over, � �.�.)
/// ������ ������ ������ ���� �������� �� UI-������ ������.
/// </summary>
public abstract class GameScreen : MonoBehaviour
{
    /// <summary>
    /// ������� �� ����� � ������ ������
    /// </summary>
    public bool IsActive => gameObject.activeSelf;

    /// <summary>
    /// ������������ �����, �� ������� ����� ��������� ������� �����
    /// �������� ������� ����
    /// </summary>
    public GameScreen ParentScreen { get; private set; } = null;

    /// <summary>
    /// ��������/������ �����
    /// </summary>
    /// <param name="active"></param>
    public virtual void SetActive(bool active)
    {
        if (active != IsActive)
        {
            gameObject.SetActive(active);
        }
        if (!active)
        {
            ParentScreen = null;
        }
    }
    
    /// <summary>
    /// ���������� ������������ �����
    /// </summary>
    /// <param name="parentScreen"></param>
    public void SetParent(GameScreen parentScreen)
    {
        ParentScreen = parentScreen;
    }
}
