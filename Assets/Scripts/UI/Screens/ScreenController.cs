﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Контроллер игровых экранов (сринов).
/// Ведёт список UI-экранов игры, отображает и скрывает их.
/// Добавляется отдельным дочерним объектом в GameManager.
/// </summary>
public class ScreenController : MonoBehaviour
{
    [SerializeField, Tooltip("Экран окончания игры")]
    private GameScreen gameOverScreen = null;

    [SerializeField, Tooltip("Экран паузы")]
    private GameScreen pauseScreen = null;

    [SerializeField, Tooltip("Экран настроек")]
    private GameScreen mainMenuScreen = null;

    [SerializeField, Tooltip("Экран настроек")]
    private GameScreen optionsScreen = null;

    [SerializeField, Tooltip("Затемняющий экран")]
    private GameScreen blackoutScreen = null;

    public GameScreen CurrentScreen => GetCurrentScreen();

    /// <summary>
    /// Список всех экранов
    /// </summary>
    private readonly List<GameScreen> screens = new List<GameScreen>();

    /// <summary>
    /// Скрыть текущий экран
    /// </summary>
    public void HideCurrentScreen()
    {
        SetActiveScreen(null);
    }

    /// <summary>
    /// Показать экран окончания игры
    /// </summary>
    public void ShowGameOverScreen()
    {
        SetActiveScreen(gameOverScreen);
    }

    /// <summary>
    /// Показать экран паузы
    /// </summary>
    public void ShowPauseScreen()
    {
        SetActiveScreen(pauseScreen);
    }

    /// <summary>
    /// Показать указанный экран как дочерний
    /// </summary>
    /// <param name="childScreen"></param>
    public void ShowChildScreen(GameScreen childScreen)
    {
        if (childScreen != null)
        {
            childScreen.SetParent(CurrentScreen);
            SetActiveScreen(childScreen);
        }
    }

    /// <summary>
    /// Показать родительский экран текущего экрана
    /// </summary>
    public void ShowParentScreen()
    {
        if (CurrentScreen != null && CurrentScreen.ParentScreen != null)
        {
            SetActiveScreen(CurrentScreen.ParentScreen);
        }
    }

    public void ShowBlackScreen()
    {
        SetActiveScreen(blackoutScreen);
    }

    private void Awake()
    {
        screens.Add(gameOverScreen);
        screens.Add(pauseScreen);
        screens.Add(optionsScreen);
        screens.Add(mainMenuScreen);
        screens.Add(blackoutScreen);

        SetActiveScreen(null);
    }

    /// <summary>
    /// Получить текущий активный экран
    /// </summary>
    /// <returns></returns>
    private GameScreen GetCurrentScreen()
    {
        foreach (var item in screens)
        {
            if (item.IsActive)
            {
                return item;
            }
        }
        return null;
    }
    /// <summary>
    /// Показать указанный экран. Одновременно может быть активен только один экран,
    /// поэтому все остальные будут скрыты.
    /// </summary>
    /// <param name="screen"></param>
    private void SetActiveScreen(GameScreen screen)
    {
        foreach (var item in screens)
        {
            item.SetActive(item == screen);
        }
    }

}
