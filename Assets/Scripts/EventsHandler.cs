﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System;

/// <summary>
/// Обработчик глобальных событий игры
/// </summary>
public class EventsHandler: MonoBehaviour
{
    [SerializeField, Tooltip("Задержка проигрывания финальной заставки после убийства последнего врага")]
    private float delayBeforeEndGame = 2;

    private void OnEnable()
    {
        GameManager.Instance.Player.RegisterKilledListener(OnPlayerKilled);
        EnemyManager.Instance.RegisterEnemyDefeatedListener(OnEnemyDefeated);
    }

    private void OnDisable()
    {
        GameManager.Instance.Player.UnregisterKilledListener(OnPlayerKilled);
        EnemyManager.Instance.UnregisterEnemyDefeatedListener(OnEnemyDefeated);
    }

    /// <summary>
    /// Обработчик события убийства врага
    /// </summary>
    private void OnEnemyDefeated()
    {
        if (EnemyManager.Instance.GetDefeatedEnemyCount() >= EnemyManager.Instance.Enemies.Count())
        {
            StartCoroutine(PerformGameEnd());
        }
    }

    /// <summary>
    /// Обработчик события смерти игрока
    /// </summary>
    private void OnPlayerKilled()
    {
        GameManager.Instance.Controller.SetGameOver();
    }

    /// <summary>
    /// Корутина, делающая задержку перед конйом игры
    /// </summary>
    /// <returns></returns>
    private IEnumerator PerformGameEnd()
    {
        yield return new WaitForSeconds(delayBeforeEndGame);
        GameManager.Instance.Controller.FinishGame();
    }

}
